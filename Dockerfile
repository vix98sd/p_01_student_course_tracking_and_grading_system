FROM openjdk:11
EXPOSE 8080
ADD target/student-tracking-docker.jar student-tracking-docker.jar
ENTRYPOINT ["java", "-jar", "/student-tracking-docker.jar"]