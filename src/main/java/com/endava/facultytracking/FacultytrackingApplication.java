package com.endava.facultytracking;

import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.AdministratorService;
import com.endava.facultytracking.service.AppUserDetailsService;
import com.endava.facultytracking.service.LecturerService;
import com.endava.facultytracking.service.RoleService;
import com.endava.facultytracking.service.StudentService;
import com.endava.facultytracking.utility.PresentationDataProvider;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FacultytrackingApplication {

  public static void main(String[] args) {
    SpringApplication.run(FacultytrackingApplication.class, args);
  }

  @Value("${presentation.load-dummy-data}")
  private boolean isLoadNeeded;

  @Bean
  CommandLineRunner run(PresentationDataProvider presentationDataProvider) {
    return args -> {
      if(isLoadNeeded)
        presentationDataProvider.LoadPresentationData();
    };
  }
}
