package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.service.AdministratorService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/administrators")
@RequiredArgsConstructor
public class AdministratorController {

  private final AdministratorService administratorService;

  @PostMapping
  public Administrator createNewAdministrator(@RequestBody Administrator newAdministrator){
    return administratorService.createNewAdministrator(newAdministrator);
  }

  @GetMapping("{administratorId}")
  public Administrator getAdministrator(@PathVariable("administratorId") Long administratorId){
    return administratorService.getAdministrator(administratorId);
  }

  @GetMapping
  public List<Administrator> getAllAdministrators(){
    return administratorService.getAllAdministrators();
  }

  @PutMapping
  public void updateAdministratorInfo(@RequestBody Administrator newAdministratorInfo){
    administratorService.updateAdministratorInfo(newAdministratorInfo);
  }

  @DeleteMapping("{administratorId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void deleteAdministrator(@PathVariable("administratorId") Long administratorId){
    administratorService.deleteAdministrator(administratorId);
  }
}
