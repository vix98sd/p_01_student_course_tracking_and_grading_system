package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.service.CourseAssignmentService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "v1/assignments")
@RequiredArgsConstructor
public class CourseAssignmentController {

  private final CourseAssignmentService courseAssignmentService;

  @PostMapping
  public CourseAssignment addNewCourseAssignment(@RequestBody CourseAssignment courseAssignment) {
    return courseAssignmentService.addNewCourseAssignment(courseAssignment);
  }

  @GetMapping(path = "{courseAssignmentId}")
  public CourseAssignment getCourseAssignment(
      @PathVariable("courseAssignmentId") Long courseAssignmentId) {
    return courseAssignmentService.getCourseAssignment(courseAssignmentId);
  }

  @GetMapping(path = "courses/{courseId}")
  public List<CourseAssignment> getCourseAssignments(@PathVariable("courseId") Long courseId) {
    return courseAssignmentService.getCourseAssignments(courseId);
  }

  @PutMapping
  public CourseAssignment updateCourseAssignmentInfo(
      @RequestBody CourseAssignment newCourseAssignmentInfo) {
    return courseAssignmentService.updateCourseAssignmentInfo(newCourseAssignmentInfo);
  }

  @DeleteMapping("{courseAssignmentId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteCourseAssignment(@PathVariable("courseAssignmentId") Long courseAssignmentId) {
    courseAssignmentService.deleteCourseAssignment(courseAssignmentId);
  }

  @PostMapping("{courseAssignmentId}/results")
  public void addNewResult(
      @PathVariable("courseAssignmentId") Long courseAssignmentId,
      @RequestBody CourseAssignmentResult courseAssignmentResult
  ) {
    courseAssignmentService.addNewResult(courseAssignmentId, courseAssignmentResult);
  }

  @GetMapping("{courseAssignmentId}/results")
  public List<CourseAssignmentResult> getCourseAssignmentResults(
      @PathVariable("courseAssignmentId") Long courseAssignmentId
  ) {
    return courseAssignmentService.getCourseAssignmentResults(courseAssignmentId);
  }

  @DeleteMapping("results/{courseAssignmentResultId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteCourseAssignmentResult(
      @PathVariable("courseAssignmentResultId") Long courseAssignmentResultId
  ) {
    courseAssignmentService.deleteCourseAssignmentResult(courseAssignmentResultId);
  }
}
