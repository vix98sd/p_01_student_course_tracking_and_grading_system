package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.dto.CourseDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.CourseService;
import com.endava.facultytracking.service.CourseSubmissionService;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/courses")
@RequiredArgsConstructor
public class CourseController {

  private final CourseService courseService;
  private final DtoMapper dtoMapper;

  @PostMapping
  public CourseDto createNewCourse(@RequestBody(required = true) CourseDto course) {
    return dtoMapper.modelToDto(courseService.createNewCourse(dtoMapper.dtoToModel(course)));
  }

  @GetMapping
  public List<CourseDto> getAllCourses() {
    return courseService.getAllCourses().stream().map(dtoMapper::modelToDto)
        .collect(Collectors.toList());
  }

  @GetMapping("{courseId}")
  public CourseDto getCourse(@PathVariable("courseId") Long courseId) {
    return dtoMapper.modelToDto(courseService.getCourse(courseId));
  }

  @PutMapping("{courseId}")
  public CourseDto updateCourse(@PathVariable("courseId") Long courseId,
      @RequestBody(required = true) CourseDto course) {
    return dtoMapper.modelToDto(
        courseService.updateCourseInfo(courseId, dtoMapper.dtoToModel(course)));
  }

  @DeleteMapping("{courseId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void deleteCourse(@PathVariable("courseId") Long courseId) {
    courseService.deleteCourse(courseId);
  }

  @PostMapping("{courseId}/students/{studentId}")
  public void enrollStudent(
      @PathVariable("courseId") Long courseId,
      @PathVariable("studentId") Long studentId
  ) {
    courseService.enrollStudent(courseId, studentId);
  }

  @GetMapping("{courseId}/students")
  public Set<Student> getEnrolledStudents(
      @PathVariable("courseId") Long courseId
  ) {
    return courseService.getEnrolledStudents(courseId);
  }

  @PostMapping("{courseId}/lecturers/{lecturerId}")
  public void assignLecturer(
      @PathVariable("courseId") Long courseId,
      @PathVariable("lecturerId") Long lecturerId
  ) {
    courseService.assignLecturer(courseId, lecturerId);
  }

  @GetMapping("{courseId}/lecturers")
  public Set<Lecturer> getCourseLecturers(
      @PathVariable("courseId") Long courseId
  ) {
    return courseService.getCourseLecturers(courseId);
  }

  @GetMapping("submissions/{courseId}")
  public List<CourseSubmission> getActiveSubmissions(
      @PathVariable("courseId") Long courseId
  ) {
    return courseService.getActiveSubmissions(courseId);
  }

  @GetMapping("submissions/approve/{courseSubmissionId}")
  public void approveCourseSubmission(
      @PathVariable("courseSubmissionId") Long courseSubmissionId
  ) {
    courseService.approveCourseSubmission(courseSubmissionId);
  }

  @GetMapping("submissions/reject/{courseSubmissionId}")
  public void rejectCourseSubmission(
      @PathVariable("courseSubmissionId") Long courseSubmissionId
  ) {
    courseService.rejectCourseSubmission(courseSubmissionId);
  }

  @GetMapping("submissions/revoke/{courseSubmissionId}")
  public void revokeCourseSubmission(
      @PathVariable("courseSubmissionId") Long courseSubmissionId
  ) {
    courseService.revokeCourseSubmission(courseSubmissionId);
  }

  @GetMapping("approving")
  List<CourseDto> getCoursesToBeApproved() {
    return courseService.getCoursesToBeApproved().stream().map(dtoMapper::modelToDto)
        .collect(Collectors.toList());
  }

  @GetMapping("approving/{courseId}")
  void approveCourseCreation(@PathVariable("courseId") Long courseId) {
    courseService.approveCourseCreation(courseId);
  }
}
