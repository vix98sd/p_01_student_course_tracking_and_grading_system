package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.service.CourseExamService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/exams")
@RequiredArgsConstructor
public class CourseExamController {

  private final CourseExamService courseExamService;

  @PostMapping
  public CourseExam addNewCourseExam(@RequestBody CourseExam newCourseExam) {
    return courseExamService.addNewCourseExam(newCourseExam);
  }

  @GetMapping("{courseExamId}")
  public CourseExam getCourseExam(@PathVariable("courseExamId") Long courseExamId) {
    return courseExamService.getCourseExam(courseExamId);
  }

  @GetMapping("courses/{courseId}")
  public List<CourseExam> getCourseExams(@PathVariable("courseId") Long courseId) {
    return courseExamService.getCourseExams(courseId);
  }

  @GetMapping("active/courses/{courseId}")
  public List<CourseExam> getActiveCourseExams(@PathVariable("courseId") Long courseId) {
    return courseExamService.getActiveCourseExams(courseId);
  }

  @PutMapping
  public CourseExam updateCourseExamInfo(@RequestBody CourseExam newCourseExamInfo) {
    return courseExamService.updateCourseExamInfo(newCourseExamInfo);
  }

  @DeleteMapping("{courseExamId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void deleteCourseExam(@PathVariable("courseExamId") Long courseExamId) {
    courseExamService.deleteCourseExam(courseExamId);
  }

  @PostMapping("{courseExamId}/results")
  public void addExamResult(
      @PathVariable("courseExamId") Long courseExamId,
      @RequestBody CourseExamResult newCourseExamResult
  ) {
    courseExamService.addExamResult(courseExamId, newCourseExamResult);
  }

  @GetMapping("{courseExamId}/results")
  public List<CourseExamResult> getCourseExamResults(
      @PathVariable("courseExamId") Long courseExamId) {
    return courseExamService.getCourseExamResults(courseExamId);
  }

  @DeleteMapping("results/{courseExamResultId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void deleteCourseExamResult(@PathVariable("courseExamResultId") Long courseExamResultId) {
    courseExamService.deleteCourseExamResult(courseExamResultId);
  }

  @GetMapping("results/approve/{courseExamResultId}")
  public void approveCourseExamResult(
      @PathVariable Long courseExamResultId
  ) {
    courseExamService.approveCourseExamResult(courseExamResultId);
  }

  @GetMapping("results/reject/{courseExamResultId}")
  public void rejectCourseExamResult(
      @PathVariable Long courseExamResultId
  ) {
    courseExamService.rejectCourseExamResult(courseExamResultId);
  }
}