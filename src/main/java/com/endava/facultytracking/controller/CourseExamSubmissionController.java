package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.service.CourseExamService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/exams/submissions")
@RequiredArgsConstructor
public class CourseExamSubmissionController {

  private final CourseExamService courseExamService;
  private final DtoMapper dtoMapper;

  @PostMapping
  public void createNewFinalExamSubmission(
      @RequestBody CourseExamSubmissionDto courseExamSubmission) {
    courseExamService.createNewFinalExamSubmission(dtoMapper.dtoToModel(courseExamSubmission));
  }

  @GetMapping(path = "approve/{finalExamSubmissionId}")
  public void approveFinalExamSubmission(
      @PathVariable("finalExamSubmissionId") Long finalExamSubmissionId
  ) {
    courseExamService.approveFinalExamSubmission(finalExamSubmissionId);
  }

  @GetMapping(path = "reject/{finalExamSubmissionId}")
  public void rejectFinalExamSubmission(
      @PathVariable("finalExamSubmissionId") Long finalExamSubmissionId
  ) {
    courseExamService.rejectFinalExamSubmission(finalExamSubmissionId);
  }

  @GetMapping(path = "revoke/{finalExamSubmissionId}")
  public void revokeFinalExamSubmission(
      @PathVariable("finalExamSubmissionId") Long finalExamSubmissionId
  ) {
    courseExamService.revokeFinalExamSubmission(finalExamSubmissionId);
  }

  @GetMapping(path = "{finalCourseExamId}")
  public List<CourseExamSubmissionDto> getActiveSubmissions(
      @PathVariable("finalCourseExamId") Long finalCourseExamId
  ) {
    return courseExamService.getActiveSubmissions(finalCourseExamId, dtoMapper);
  }
}
