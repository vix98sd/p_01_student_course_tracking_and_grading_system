package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.CourseScheduleService;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/schedules")
@RequiredArgsConstructor
public class CourseScheduleController {

  private final CourseScheduleService courseScheduleService;

  @PostMapping(path = "{courseId}")
  public CourseSchedule addNewCourseSchedule(
      @PathVariable("courseId") Long courseId,
      @RequestBody CourseSchedule newCourseSchedule
  ) {
    return courseScheduleService.addNewCourseSchedule(courseId, newCourseSchedule);
  }

  @PostMapping(path = "{courseId}/series")
  public List<CourseSchedule> addNewCourseScheduleSeries(
      @PathVariable("courseId") Long courseId,
      @RequestBody CourseSchedule newCourseSchedule
  ) {
    return courseScheduleService.addNewCourseScheduleSeries(courseId, newCourseSchedule);
  }

  @GetMapping(path = "{scheduleDay}/{scheduleMonth}/{scheduleYear}")
  public List<CourseSchedule> getCurrentFullScheduleForWeek(
      @PathVariable("scheduleDay") Integer scheduleDay,
      @PathVariable("scheduleMonth") Integer scheduleMonth,
      @PathVariable("scheduleYear") Integer scheduleYear
  ) {
    return courseScheduleService.getCurrentFullScheduleForWeek(
        LocalDate.of(scheduleYear, scheduleMonth, scheduleDay));
  }

  @GetMapping(path = "{courseScheduleId}")
  public CourseSchedule getCourseScheduleById(@PathVariable("courseScheduleId") Long courseScheduleId) {
    return courseScheduleService.getCourseScheduleById(courseScheduleId);
  }

  @PutMapping(path = "{courseScheduleId}")
  public CourseSchedule updateCourseScheduleInfo(
      @PathVariable("courseScheduleId") Long courseScheduleId,
      @RequestBody CourseSchedule newCourseScheduleInfo
  ) {
    return courseScheduleService.updateCourseScheduleInfo(courseScheduleId, newCourseScheduleInfo);
  }

  @DeleteMapping(path = "{courseScheduleId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void cancelCourseSchedule(
      @PathVariable("courseScheduleId") Long courseScheduleId
  ) {
    courseScheduleService.cancelCourseSchedule(courseScheduleId, false);
  }

  @PutMapping("{courseScheduleId}/attendances/students/{studentId}")
  public void addAttendee(
      @PathVariable("courseScheduleId") Long courseScheduleId,
      @PathVariable("studentId") Long studentId
  ){
    courseScheduleService.addAttendee(courseScheduleId, studentId);
  }

  @PutMapping("{courseScheduleId}/attendances/students")
  public void addAttendees(
      @PathVariable("courseScheduleId") Long courseScheduleId,
      @RequestBody List<Long> studentIds
  ){
    courseScheduleService.addAttendees(courseScheduleId, studentIds);
  }

  @DeleteMapping("{courseScheduleId}/attendances/students/{studentId}")
  public void removeAttendee(
      @PathVariable("courseScheduleId") Long courseScheduleId,
      @PathVariable("studentId") Long studentId
  ){
    courseScheduleService.removeAttendee(courseScheduleId, studentId);
  }

  @GetMapping("{courseScheduleId}/attendances")
  public Set<Student> getAttended(@PathVariable("courseScheduleId") Long courseScheduleId){
    return courseScheduleService.getAttended(courseScheduleId);
  }
}
