package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.service.LecturerService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/lecturers")
@RequiredArgsConstructor
public class LecturerController {

  private final LecturerService lecturerService;

  @PostMapping
  public Lecturer addNewLecturer(@RequestBody(required = true) Lecturer lecturer) {
    return lecturerService.createNewLecturer(lecturer);
  }

  @GetMapping
  public List<Lecturer> getAllLecturers() {
    return lecturerService.getAllLecturers();
  }

  @GetMapping("{lecturerId}")
  public Lecturer getLecturer(@PathVariable("lecturerId") Long lecturerId) {
    return lecturerService.getLecturer(lecturerId);
  }

  @PutMapping("{lecturerId}")
  public Lecturer updateLecturerInfo(
      @PathVariable("lecturerId") Long lecturerId,
      @RequestBody Lecturer newLecturerInfo
  ) {
    return lecturerService.updateLecturerInfo(lecturerId, newLecturerInfo);
  }

  @DeleteMapping("{lecturerId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void deleteLecturer(@PathVariable("lecturerId") Long lecturerId) {
    lecturerService.deleteLecturer(lecturerId);
  }

  @GetMapping("{lecturerId}/courses")
  public Set<Course> getLecturerCourses(@PathVariable("lecturerId") Long lecturerId) {
    return lecturerService.getLecturerCourses(lecturerId);
  }

}
