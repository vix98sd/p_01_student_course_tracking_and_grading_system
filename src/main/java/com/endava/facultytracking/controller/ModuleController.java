package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.dto.ModuleDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.service.ModuleService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/modules")
@RequiredArgsConstructor
public class ModuleController {

  private final ModuleService moduleService;
  private final DtoMapper dtoMapper;

  @PostMapping
  public ModuleDto createNewModule(@RequestBody(required = true) ModuleDto module) {
    return dtoMapper.modelToDto(moduleService.createModule(dtoMapper.dtoToModel(module)));
  }

  @GetMapping
  public List<ModuleDto> getAllModules() {
    return moduleService.getAllModules().stream().map(dtoMapper::modelToDto)
        .collect(Collectors.toList());
  }

  @GetMapping("{moduleId}")
  public ModuleDto getModule(@PathVariable("moduleId") Long moduleId) {
    return dtoMapper.modelToDto(moduleService.getModule(moduleId));
  }

  @PutMapping("{moduleId}")
  public ModuleDto updateModuleInfo(
      @PathVariable("moduleId") Long moduleId,
      @RequestBody ModuleDto module
  ) {
    return dtoMapper.modelToDto(
        moduleService.updateModuleInfo(moduleId, dtoMapper.dtoToModel(module)));
  }

  @DeleteMapping("{moduleId}")
  public String deleteModule(@PathVariable("moduleId") Long moduleId) {
    return moduleService.deleteModule(moduleId);
  }
}
