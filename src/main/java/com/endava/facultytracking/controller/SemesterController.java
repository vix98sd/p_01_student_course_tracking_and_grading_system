package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.dto.SemesterDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.service.SemesterService;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/semesters")
@RequiredArgsConstructor
public class SemesterController {

  private final SemesterService semesterService;
  private final DtoMapper dtoMapper;

  @PostMapping
  public SemesterDto createSemester(@RequestBody SemesterDto semesterDto) {
    return dtoMapper.modelToDto(semesterService.createSemester(dtoMapper.dtoToModel(semesterDto)));
  }

  @GetMapping
  public List<SemesterDto> getAllSemesters() {
    return semesterService.getAllSemesters().stream()
        .map(dtoMapper::modelToDto).collect(Collectors.toList());
  }

  @GetMapping("{semesterId}")
  public SemesterDto getSemester(@PathVariable("semesterId") Long semesterId) {
    return dtoMapper.modelToDto(semesterService.getSemester(semesterId));
  }

  @PutMapping("{semesterId}")
  public SemesterDto updateSemesterInfo(
      @PathVariable("semesterId") Long semesterId, @RequestBody SemesterDto semester) {
    return dtoMapper.modelToDto(
        semesterService.updateSemesterInfo(semesterId, dtoMapper.dtoToModel(semester)));
  }

  @DeleteMapping("{semesterId}")
  public String deleteSemester(@PathVariable("semesterId") Long semesterId) {
    return semesterService.deleteSemester(semesterId);
  }

}
