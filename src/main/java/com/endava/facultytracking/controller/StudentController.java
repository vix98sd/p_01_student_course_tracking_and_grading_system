package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.StudentService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/students")
@RequiredArgsConstructor
public class StudentController {

  private final StudentService studentService;

  @PostMapping
  public Student createNewStudent(@RequestBody(required = true) Student student) {
    return studentService.createNewStudent(student);
  }

  @GetMapping
  public List<Student> getAllStudents() {
    return studentService.getAllStudents();
  }

  @GetMapping("{studentId}")
  public Student getStudent(@PathVariable("studentId") Long studentId) {
    return studentService.getStudent(studentId);
  }

  @PutMapping("{studentId}")
  public Student updateStudent(
      @PathVariable("studentId") Long studentId,
      @RequestBody Student student
  ) {
    return studentService.updateStudent(studentId, student);
  }

  @DeleteMapping("{studentId}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void deleteStudent(@PathVariable("studentId") Long studentId) {
    studentService.deleteStudent(studentId);
  }

  @GetMapping("{studentId}/courses")
  public Set<Course> getEnrolledCourses(@PathVariable("studentId") Long studentId) {
    return studentService.getEnrolledCourses(studentId);
  }

  @PutMapping("{studentId}/modules/{moduleId}")
  public void assignModule(
      @PathVariable("studentId") Long studentId,
      @PathVariable("moduleId") Long moduleId
  ){
    studentService.assignModule(studentId, moduleId);
  }

  @GetMapping("{studentId}/modules")
  public Set<Module> getAssignedModules(@PathVariable("studentId") Long studentId){
    return studentService.getAssignedModules(studentId);
  }
}
