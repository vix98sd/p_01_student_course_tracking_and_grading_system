package com.endava.facultytracking.controller;

import com.endava.facultytracking.domain.model.StudentsGrade;
import com.endava.facultytracking.service.StudentsGradeService;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/grades")
@RequiredArgsConstructor
public class StudentsGradeController {

  private final StudentsGradeService studentsGradeService;

  @PostMapping
  public StudentsGrade addNewStudentsGrade(@RequestBody StudentsGrade newStudentsGrade) {
    return studentsGradeService.addNewStudentsGrade(newStudentsGrade);
  }

  @GetMapping("students/{studentId}")
  public List<Map<String, Number>> getAllStudentsGrades(@PathVariable("studentId") Long studentId) {
    return studentsGradeService.getAllStudentsGrades(studentId);
  }

  @GetMapping("courses/{courseId}")
  public List<Map<String, Number>> getAllGradesByCourse(@PathVariable("courseId") Long courseId) {
    return studentsGradeService.getAllGradesByCourse(courseId);
  }

  @GetMapping("students/{studentId}/courses/{courseId}")
  public StudentsGrade getStudentsGradeByCourse(
      @PathVariable("studentId") Long studentId,
      @PathVariable("courseId") Long courseId
  ) {
    return studentsGradeService.getStudentsGradeByCourse(studentId, courseId);
  }

  @DeleteMapping
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void annulGrade(@RequestBody StudentsGrade studentsGradeToAnnul) {
    studentsGradeService.annulGrade(studentsGradeToAnnul);
  }

}
