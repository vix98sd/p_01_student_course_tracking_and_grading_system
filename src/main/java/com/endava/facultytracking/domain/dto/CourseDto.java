package com.endava.facultytracking.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CourseDto {

  private Long id;
  private String name;
  private String codeName;
  private Integer lecturesPerWeek;
  private Integer classExercisesPerWeek;
  private Integer labExercisesPerWeek;
  private Boolean isOptional;
  private ModuleDto module;
  private SemesterDto semester;
}
