package com.endava.facultytracking.domain.dto;

import com.endava.facultytracking.domain.model.Course;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import java.time.LocalTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CourseExamDto {

  private Long id;
  private LocalDate startingDate;
  private LocalTime startingTime;
  private Boolean isFinal;
  private String description;

  @JsonProperty(access = Access.WRITE_ONLY)
  private CourseDto course;
}
