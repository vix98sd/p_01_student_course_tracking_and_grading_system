package com.endava.facultytracking.domain.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class CourseExamResultDto {

  private LocalDate date;
  private Long percentDone;
  private Boolean isPresent;
}
