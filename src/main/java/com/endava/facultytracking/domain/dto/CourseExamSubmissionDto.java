package com.endava.facultytracking.domain.dto;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CourseExamSubmissionDto {

  private Long id;
  private SubmissionStatus status = SubmissionStatus.WAITING_FOR_APPROVAL;
  private StudentDto student;
  @JsonProperty(access = Access.WRITE_ONLY)
  private CourseExamDto finalExam;
  @JsonProperty(access = Access.READ_ONLY)
  private Integer numberOfClasses;
  @JsonProperty(access = Access.READ_ONLY)
  private Integer attendedOn;
  @JsonProperty(access = Access.READ_ONLY)
  private List<StudentOnClassDto> classesAttended = new ArrayList<>();
  @JsonProperty(access = Access.READ_ONLY)
  private List<CourseExamResultDto> midtermExamResults = new ArrayList<>();
}
