package com.endava.facultytracking.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ModuleDto {

  private Long id;
  private String name;
  private String codeName;
  private Integer grade;
}
