package com.endava.facultytracking.domain.dto;

import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SemesterDto {

  private Long id;
  private LocalDate startingDate;
  private LocalDate endingDate;
  private LocalDate submissionPeriodFrom;
  private LocalDate submissionPeriodTo;
  private LocalDate finalExamPeriodFrom;
  private LocalDate finalExamPeriodTo;
}
