package com.endava.facultytracking.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentDto {

  private Long id;
  private String firstName;
  private String lastName;
  private String indexNumber;
  private Integer grade;
  private String username;

  @JsonProperty(access = Access.WRITE_ONLY)
  private LocalDate dateOfBirth;
  @JsonProperty(access = Access.WRITE_ONLY)
  private String password;
}