package com.endava.facultytracking.domain.dto;

import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentOnClassDto {

  private LocalDate date;
  private boolean attended;
}
