package com.endava.facultytracking.domain.dto.mapper;

import com.endava.facultytracking.domain.dto.CourseDto;
import com.endava.facultytracking.domain.dto.CourseExamDto;
import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.ModuleDto;
import com.endava.facultytracking.domain.dto.SemesterDto;
import com.endava.facultytracking.domain.dto.StudentDto;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import java.util.List;

public interface DtoMapper {

  ModuleDto modelToDto(Module model);

  Module dtoToModel(ModuleDto dto);

  SemesterDto modelToDto(Semester model);

  Semester dtoToModel(SemesterDto dto);

  CourseDto modelToDto(Course model);

  Course dtoToModel(CourseDto dto);

  StudentDto modelToDto(Student model);

  Student dtoToModel(StudentDto dto);

  CourseExamDto modelToDto(CourseExam model);

  CourseExam dtoToModel(CourseExamDto dto);

  CourseExamSubmissionDto modelToDto(CourseExamSubmission model,
      List<CourseSchedule> courseSchedules, List<CourseExamResult> courseExamResults);

  CourseExamSubmission dtoToModel(CourseExamSubmissionDto dto);

//  Dto modelToDto( model);
//  dtoToModel(Dto dto);
}
