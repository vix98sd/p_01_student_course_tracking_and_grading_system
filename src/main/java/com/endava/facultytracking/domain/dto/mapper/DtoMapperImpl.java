package com.endava.facultytracking.domain.dto.mapper;

import com.endava.facultytracking.domain.dto.CourseDto;
import com.endava.facultytracking.domain.dto.CourseExamDto;
import com.endava.facultytracking.domain.dto.CourseExamResultDto;
import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.ModuleDto;
import com.endava.facultytracking.domain.dto.SemesterDto;
import com.endava.facultytracking.domain.dto.StudentDto;
import com.endava.facultytracking.domain.dto.StudentOnClassDto;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class DtoMapperImpl implements DtoMapper {

  @Override
  public ModuleDto modelToDto(Module model) {
    ModuleDto dto = new ModuleDto();
    dto.setId(model.getId());
    dto.setName(model.getName());
    dto.setCodeName(model.getCodeName());
    dto.setGrade(model.getGrade());
    return dto;
  }

  @Override
  public Module dtoToModel(ModuleDto dto) {
    Module model = new Module();
    model.setId(dto.getId());
    model.setName(dto.getName());
    model.setCodeName(dto.getCodeName());
    model.setGrade(dto.getGrade());
    return model;
  }

  @Override
  public SemesterDto modelToDto(Semester model) {
    SemesterDto dto = new SemesterDto();
    dto.setId(model.getId());
    dto.setStartingDate(model.getStartingDate());
    dto.setEndingDate(model.getEndingDate());
    dto.setSubmissionPeriodFrom(model.getSubmissionPeriodFrom());
    dto.setSubmissionPeriodTo(model.getSubmissionPeriodTo());
    dto.setFinalExamPeriodFrom(model.getFinalExamPeriodFrom());
    dto.setFinalExamPeriodTo(model.getFinalExamPeriodTo());
    return dto;
  }

  @Override
  public Semester dtoToModel(SemesterDto dto) {
    Semester model = new Semester();
    model.setId(dto.getId());
    model.setStartingDate(dto.getStartingDate());
    model.setEndingDate(dto.getEndingDate());
    model.setSubmissionPeriodFrom(dto.getSubmissionPeriodFrom());
    model.setSubmissionPeriodTo(dto.getSubmissionPeriodTo());
    model.setFinalExamPeriodFrom(dto.getFinalExamPeriodFrom());
    model.setFinalExamPeriodTo(dto.getFinalExamPeriodTo());
    return model;
  }

  @Override
  public CourseDto modelToDto(Course model) {
    CourseDto dto = new CourseDto();
    dto.setId(model.getId());
    dto.setName(model.getName());
    dto.setCodeName(model.getCodeName());
    dto.setLecturesPerWeek(model.getLecturesPerWeek());
    dto.setClassExercisesPerWeek(model.getClassExercisesPerWeek());
    dto.setLabExercisesPerWeek(model.getLabExercisesPerWeek());
    dto.setIsOptional(model.getIsOptional());
    dto.setModule(this.modelToDto(model.getModule()));
    dto.setSemester(this.modelToDto(model.getSemester()));
    return dto;
  }

  @Override
  public Course dtoToModel(CourseDto dto) {
    Course model = new Course();
    model.setId(dto.getId());
    model.setName(dto.getName());
    model.setCodeName(dto.getCodeName());
    model.setLecturesPerWeek(dto.getLecturesPerWeek());
    model.setClassExercisesPerWeek(dto.getClassExercisesPerWeek());
    model.setLabExercisesPerWeek(dto.getLabExercisesPerWeek());
    model.setIsOptional(dto.getIsOptional());
    model.setModule(this.dtoToModel(dto.getModule()));
    model.setSemester(this.dtoToModel(dto.getSemester()));
    return model;
  }

  @Override
  public StudentDto modelToDto(Student model) {
    StudentDto dto = new StudentDto();
    dto.setId(model.getId());
    dto.setFirstName(model.getFirstName());
    dto.setLastName(model.getLastName());
    dto.setIndexNumber(model.getIndexNumber());
    dto.setGrade(model.getGrade());
    dto.setUsername(model.getUsername());
    dto.setPassword(model.getPassword());
    return dto;
  }

  @Override
  public Student dtoToModel(StudentDto dto) {
    Student model = new Student();
    model.setId(dto.getId());
    model.setFirstName(dto.getFirstName());
    model.setLastName(dto.getLastName());
    model.setIndexNumber(dto.getIndexNumber());
    model.setGrade(dto.getGrade());
    model.setUsername(dto.getUsername());
    model.setPassword(dto.getPassword());
    return model;
  }

  @Override
  public CourseExamDto modelToDto(CourseExam model) {
    CourseExamDto dto = new CourseExamDto();
    dto.setId(model.getId());
    dto.setStartingDate(model.getStartingDate());
    dto.setStartingTime(model.getStartingTime());
    dto.setIsFinal(model.getIsFinal());
    dto.setDescription(model.getDescription());
    return dto;
  }

  @Override
  public CourseExam dtoToModel(CourseExamDto dto) {
    CourseExam model = new CourseExam();
    model.setId(dto.getId());
    model.setStartingDate(dto.getStartingDate());
    model.setStartingTime(dto.getStartingTime());
    model.setIsFinal(dto.getIsFinal());
    model.setDescription(dto.getDescription());
    return model;
  }

  @Override
  public CourseExamSubmissionDto modelToDto(
      CourseExamSubmission model,
      List<CourseSchedule> courseSchedules,
      List<CourseExamResult> courseExamResults
  ) {
    CourseExamSubmissionDto dto = new CourseExamSubmissionDto();
    dto.setId(model.getId());
    dto.setStatus(model.getStatus());
    dto.setStudent(this.modelToDto(model.getStudent()));
    dto.setFinalExam(this.modelToDto(model.getFinalExam()));

    List<StudentOnClassDto> classAttended = courseSchedules.stream()
        .filter(courseSchedule -> courseSchedule.getAttended().contains(model.getStudent()))
        .map(courseSchedule -> {
          StudentOnClassDto studentOnClass = new StudentOnClassDto();
          studentOnClass.setDate(courseSchedule.getStartingDate());
          studentOnClass.setAttended(true);
          return studentOnClass;
        }).collect(Collectors.toList());

    dto.setAttendedOn(classAttended.size());
    dto.setNumberOfClasses(courseSchedules.size());

    dto.setClassesAttended(classAttended);

    List<CourseExamResultDto> midtermExamResultsDto = courseExamResults.stream()
        .map(exam -> {
          CourseExamResultDto courseExamResultDto = new CourseExamResultDto();
          courseExamResultDto.setDate(exam.getCourseExam().getStartingDate());
          courseExamResultDto.setPercentDone(exam.getPercentDone());
          courseExamResultDto.setIsPresent(exam.getIsPresent());
          return courseExamResultDto;
        }).collect(Collectors.toList());

    dto.setMidtermExamResults(midtermExamResultsDto);

    return dto;
  }

  @Override
  public CourseExamSubmission dtoToModel(CourseExamSubmissionDto dto) {
    CourseExamSubmission model = new CourseExamSubmission();
    model.setId(dto.getId());
    model.setStatus(dto.getStatus());
    model.setStudent(this.dtoToModel(dto.getStudent()));
    model.setFinalExam(this.dtoToModel(dto.getFinalExam()));
    return model;
  }
}