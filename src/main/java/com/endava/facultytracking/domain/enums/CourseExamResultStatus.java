package com.endava.facultytracking.domain.enums;

public enum CourseExamResultStatus {
  WAITING_FOR_APPROVAL, REJECTED, APPROVED
}
