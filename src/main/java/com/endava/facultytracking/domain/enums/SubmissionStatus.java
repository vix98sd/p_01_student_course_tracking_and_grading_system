package com.endava.facultytracking.domain.enums;

public enum SubmissionStatus {
  WAITING_FOR_APPROVAL, REVOKED, REJECTED, APPROVED
}
