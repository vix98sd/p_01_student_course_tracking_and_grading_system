package com.endava.facultytracking.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code =  HttpStatus.BAD_REQUEST)
public class InvalidArgumentException extends IllegalStateException{

  public InvalidArgumentException(String s) {
    super(s);
  }
}
