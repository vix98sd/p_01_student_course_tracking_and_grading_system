package com.endava.facultytracking.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class RecordAlreadyExistException extends IllegalStateException{

  public RecordAlreadyExistException(String s) {
    super(s);
  }
}
