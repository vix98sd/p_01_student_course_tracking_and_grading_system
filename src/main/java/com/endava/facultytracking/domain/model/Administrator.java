package com.endava.facultytracking.domain.model;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Administrator")
@Table(
    name = "administrator",
    uniqueConstraints = {
        @UniqueConstraint(name = "administrator_username_unique", columnNames = "username")
    }
)
public class Administrator implements UserDetails {

  @Id
  @SequenceGenerator(
      name = "administrator_sequence",
      sequenceName = "administrator_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "administrator_sequence"
  )
  private Long id;
  private String firstName;
  private String lastName;
  private String username;
  @JsonProperty(access = Access.WRITE_ONLY)
  private String password;
  @JsonIgnore
  private boolean isAccountNonExpired;
  @JsonIgnore
  private boolean isAccountNonLocked;
  @JsonIgnore
  private boolean isCredentialsNonExpired;
  @JsonIgnore
  private boolean isEnabled;
  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "admin_roles",
      joinColumns = @JoinColumn(name = "administrator_id"),
      inverseJoinColumns = @JoinColumn(name = "role_id")
  )
  @JsonIgnore
  private Collection<Role> roles = new ArrayList<>();

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
    this.roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
    return authorities;
  }
}
