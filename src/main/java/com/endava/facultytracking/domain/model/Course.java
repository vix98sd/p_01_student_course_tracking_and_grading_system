package com.endava.facultytracking.domain.model;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Course")
@Table(
    name = "course",
    uniqueConstraints = {
        @UniqueConstraint(name = "course_code_name_unique", columnNames = "codeName")
    }
)
public class Course {

  @Id
  @SequenceGenerator(
      name = "course_sequence",
      sequenceName = "course_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_sequence"
  )
  private Long id;
  private String name;
  private String codeName;
  private Integer lecturesPerWeek;
  private Integer classExercisesPerWeek;
  private Integer labExercisesPerWeek;
  private Boolean isOptional;
  @JsonIgnore
  private Boolean isApproved;

  @ManyToOne
  @JoinColumn(name = "module_id", referencedColumnName = "id")
  @JsonProperty(access = Access.WRITE_ONLY)
  private Module module;

  @ManyToOne
  @JoinColumn(name = "semester_id", referencedColumnName = "id")
  @JsonProperty(access = Access.WRITE_ONLY)
  private Semester semester;

  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "students_course",
      joinColumns = @JoinColumn(name = "course_id"),
      inverseJoinColumns = @JoinColumn(name = "student_id")
  )
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private final Set<Student> enrolledStudents = new HashSet<>();

  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "course_lecturer",
      joinColumns = @JoinColumn(name = "course_id"),
      inverseJoinColumns = @JoinColumn(name = "lecturer_id")
  )
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private final Set<Lecturer> lecturers = new HashSet<>();

  public Course(){
    this.isApproved = false;
  }

  public void enrollStudent(Student student){
    this.enrolledStudents.add(student);
  }

  public void assignLecturer(Lecturer lecturer) {
    this.lecturers.add(lecturer);
  }
}
