package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Course_assignment")
@Table(name = "course_assignment")
public class CourseAssignment {

  @Setter(value = AccessLevel.NONE)
  @Id
  @SequenceGenerator(
      name = "course_assignment_sequence",
      sequenceName = "course_assignment_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_assignment_sequence"
  )
  private Long id;
  private LocalDate startingDate;
  private LocalDate endingDate;
  private LocalTime startingTime;
  private LocalTime endingTime;
  private String description;

  @ManyToOne
  @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
  @JsonProperty(access = Access.WRITE_ONLY)
  private Course course;

  @OneToMany(mappedBy = "courseAssignment")
  @JsonIgnore
  private final Set<CourseAssignmentResult> results = new HashSet<>();
}
