package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Course_assignment_result")
@Table(name = "course_assignment_result")
public class CourseAssignmentResult {

  @Setter(value = AccessLevel.NONE)
  @Id
  @SequenceGenerator(
      name = "course_assignment_result_sequence",
      sequenceName = "course_assignment_result_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_assignment_result_sequence"
  )
  private Long id;
  private Long percentDone;

  @ManyToOne
  @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false)
  @JsonProperty(access = Access.WRITE_ONLY)
  private Student student;

  @ManyToOne
  @JoinColumn(name = "course_assignment_id", referencedColumnName = "id")
  @JsonIgnore
  private CourseAssignment courseAssignment;
}
