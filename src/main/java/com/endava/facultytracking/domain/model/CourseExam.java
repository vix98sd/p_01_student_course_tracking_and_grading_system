package com.endava.facultytracking.domain.model;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Course_exam")
@Table(name = "course_exam")
public class CourseExam {

  @Id
  @SequenceGenerator(
      name = "course_exam_sequence",
      sequenceName = "course_exam_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_exam_sequence"
  )
  private Long id;
  private LocalDate startingDate;
  private LocalTime startingTime;
  private Boolean isFinal;
  private String description;

  @ManyToOne
  @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
  @JsonProperty(access = Access.WRITE_ONLY)
  private Course course;

  @OneToMany(mappedBy = "courseExam", fetch = EAGER)
  @JsonIgnore
  private Set<CourseExamResult> results = new HashSet<>();

  public CourseExam(){
    this.isFinal = false;
  }
}
