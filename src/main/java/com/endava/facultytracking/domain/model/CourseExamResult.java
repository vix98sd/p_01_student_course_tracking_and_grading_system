package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Course_exam_result")
@Table(name = "course_exam_result")
public class CourseExamResult {

  @Setter(value = AccessLevel.NONE)
  @Id
  @SequenceGenerator(
      name = "course_exam_result_sequence",
      sequenceName = "course_exam_result_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_exam_result_sequence"
  )
  private Long id;
  private Long percentDone;

  @ManyToOne
  @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false)
  private Student student;
  private Boolean isPresent;
  private Boolean isAnnulled;
  private CourseExamResultStatus status;

  @ManyToOne
  @JoinColumn(name = "course_exam_id", referencedColumnName = "id", nullable = false)
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private CourseExam courseExam;

  public CourseExamResult(){
    this.percentDone = 0L;
    this.isPresent = false;
    this.isAnnulled = false;
    this.status = CourseExamResultStatus.WAITING_FOR_APPROVAL;
  }
}