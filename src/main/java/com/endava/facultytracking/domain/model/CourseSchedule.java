package com.endava.facultytracking.domain.model;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Course_schedule")
@Table(name = "course_schedule")
public class CourseSchedule {

  @Setter(value = AccessLevel.NONE)
  @Id
  @SequenceGenerator(
      name = "course_schedule_sequence",
      sequenceName = "course_schedule_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_schedule_sequence"
  )
  private Long id;
  private LocalDate startingDate;
  private LocalTime startingTime;
  private Integer numberOfClasses;
  private String type;
  @JsonProperty(access = Access.WRITE_ONLY)
  private Boolean isCanceled;
  @ManyToOne
  @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
  @JsonIgnore
  private Course course;

  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "course_schedule_attendance",
      joinColumns = @JoinColumn(name = "course_schedule_id"),
      inverseJoinColumns = @JoinColumn(name = "student_id")
  )
  @JsonIgnore
  private final Set<Student> attended = new HashSet<>();

  public CourseSchedule() {
    this.isCanceled = false;
  }

  public CourseSchedule(CourseSchedule courseSchedule){

    this.startingDate = courseSchedule.getStartingDate();
    this.startingTime = courseSchedule.getStartingTime();
    this.numberOfClasses = courseSchedule.getNumberOfClasses();
    this.type = courseSchedule.getType();
    this.isCanceled = false;
    this.course = courseSchedule.getCourse();
  }

  public void addAttendee(Student attendee){
    attended.add(attendee);
  }

  public void addAttendees(List<Student> attendees){
    attended.addAll(attendees);
  }

  public void removeAttendee(Student attendee){
    attended.remove(attendee);
  }
}
