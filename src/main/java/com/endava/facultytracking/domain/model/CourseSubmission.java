package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Course_submission")
@Table(name = "course_submission")
public class CourseSubmission {

  @Id
  @SequenceGenerator(
      name = "course_submission_sequence",
      sequenceName = "course_submission_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "course_submission_sequence"
  )
  private Long id;
  private LocalDate dateSubmitted;
  private LocalDate dateResolved;
  private Boolean isResolved;
  @Enumerated(EnumType.ORDINAL)
  private SubmissionStatus status;
  @ManyToOne
  @JoinColumn(name = "student_id", referencedColumnName = "id")
  private Student student;
  @ManyToOne
  @JoinColumn(name = "course_id", referencedColumnName = "id")
  @JsonIgnore
  private Course course;

  public CourseSubmission() {
    this.isResolved = false;
    this.dateSubmitted = LocalDate.now();
    this.dateResolved = LocalDate.now();
    this.status = SubmissionStatus.WAITING_FOR_APPROVAL;
  }
}
