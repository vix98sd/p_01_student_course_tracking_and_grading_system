package com.endava.facultytracking.domain.model;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Lecturer")
@Table(name = "lecturer")
public class Lecturer implements UserDetails {

  @Setter(value = AccessLevel.NONE)
  @Id
  @SequenceGenerator(
      name = "lecturer_sequence",
      sequenceName = "lecturer_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "lecturer_sequence"
  )
  private Long id;
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private LocalDate dateOfBirth;
  private LocalDate dateOfEntry;
  private String degree;
  private String vocation;
  @JsonIgnore
  private LocalDate dateOfPromotion;
  private String username;
  @JsonProperty(access = Access.WRITE_ONLY)
  private String password;
  @JsonIgnore
  private boolean isAccountNonExpired;
  @JsonIgnore
  private boolean isAccountNonLocked;
  @JsonIgnore
  private boolean isCredentialsNonExpired;
  @JsonIgnore
  private boolean isEnabled;

  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "lecturer_roles",
      joinColumns = @JoinColumn(name = "lecturer_id"),
      inverseJoinColumns = @JoinColumn(name = "role_id")
  )
  @JsonIgnore
  private Collection<Role> roles = new ArrayList<>();

  @ManyToMany(mappedBy = "lecturers", fetch = EAGER)
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private final Set<Course> teachesOn = new HashSet<>();

  public Lecturer(String firstName, String lastName, String phoneNumber,
      LocalDate dateOfBirth, LocalDate dateOfEntry, String vocation,
      LocalDate dateOfPromotion, String degree, String username, String password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.dateOfBirth = dateOfBirth;
    this.dateOfEntry = dateOfEntry;
    this.degree = degree;
    this.vocation = vocation;
    this.dateOfPromotion = dateOfPromotion;
    this.username = username;
    this.password = password;
  }

  public void addCourse(Course course) {
    teachesOn.add(course);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
    this.roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
    return authorities;
  }
}