package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity(name = "Module")
@Table(
    name = "module",
    uniqueConstraints = {
        @UniqueConstraint(name = "module_code_name_unique", columnNames = "codeName")
    }
)
public class Module {

  @Id
  @SequenceGenerator(
      name = "module_sequence",
      sequenceName = "module_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "module_sequence"
  )
  private Long id;
  private String name;
  private String codeName;
  private Integer grade;

  @ManyToMany(mappedBy = "studentsModules")
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private final Set<Student> enrolledStudents = new HashSet<>();

  public Module(String name, String codeName, Integer grade) {
    this.name = name;
    this.codeName = codeName;
    this.grade = grade;
  }
}
