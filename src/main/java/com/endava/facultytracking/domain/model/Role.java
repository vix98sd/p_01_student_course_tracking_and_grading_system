package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Role")
@Table(
    name = "role",
    uniqueConstraints = {
        @UniqueConstraint(name = "role_name_unique", columnNames = "name")
    }
)
public class Role {

  @Id
  @SequenceGenerator(
      name = "role_sequence",
      sequenceName = "role_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "role_sequence"
  )
  private Long id;
  private String name;

}
