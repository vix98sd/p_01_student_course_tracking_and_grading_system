package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity(name = "Semester")
@Table(name = "semester")
public class Semester {

  @Id
  @SequenceGenerator(
      name = "semester_sequence",
      sequenceName = "semester_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "semester_sequence"
  )
  private Long id;
  private LocalDate startingDate;
  private LocalDate endingDate;
  private LocalDate submissionPeriodFrom;
  private LocalDate submissionPeriodTo;
  private LocalDate finalExamPeriodFrom;
  private LocalDate finalExamPeriodTo;

  public Semester(LocalDate startingDate) {
    this.startingDate = startingDate;
    this.setDatesByStartingDate();
  }

  public void setDatesByStartingDate() {
    if (this.startingDate != null) {
      this.endingDate = this.startingDate.plusWeeks(16);
      this.submissionPeriodFrom = this.startingDate.minusMonths(1);
      this.submissionPeriodTo = this.startingDate;
      this.finalExamPeriodFrom = this.endingDate;
      this.finalExamPeriodTo = this.finalExamPeriodFrom.plusMonths(1);
    }
  }
}
