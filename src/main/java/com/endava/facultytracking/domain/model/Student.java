package com.endava.facultytracking.domain.model;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "Student")
@Table(
    name = "student",
    uniqueConstraints = {
        @UniqueConstraint(name = "student_username_unique", columnNames = "username")
    }
)
public class Student implements UserDetails {

  @Id
  @SequenceGenerator(
      name = "student_sequence",
      sequenceName = "student_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "student_sequence"
  )
  private Long id;
  private String firstName;
  private String lastName;
  private String indexNumber;
  private LocalDate dateOfBirth;
  @JsonIgnore
  private LocalDate dateOfEntry;
  private Integer grade;
  private String username;
  @JsonProperty(access = Access.WRITE_ONLY)
  private String password;
  @JsonIgnore
  private boolean isAccountNonExpired;
  @JsonIgnore
  private boolean isAccountNonLocked;
  @JsonIgnore
  private boolean isCredentialsNonExpired;
  @JsonIgnore
  private boolean isEnabled;

  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "student_roles",
      joinColumns = @JoinColumn(name = "student_id"),
      inverseJoinColumns = @JoinColumn(name = "role_id")
  )
  @JsonIgnore
  private Collection<Role> roles = new ArrayList<>();

  @ManyToMany(mappedBy = "enrolledStudents", fetch = EAGER)
  @JsonProperty(access = Access.WRITE_ONLY)
  private Set<Course> studentsCourses = new HashSet<>();


  @ManyToMany(fetch = EAGER)
  @JoinTable(
      name = "students_module",
      joinColumns = @JoinColumn(name = "student_id"),
      inverseJoinColumns = @JoinColumn(name = "module_id")
  )
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  @Setter(value = AccessLevel.NONE)
  private final Set<Module> studentsModules = new HashSet<>();

  public Student(String firstName, String lastName, String indexNumber, LocalDate dateOfBirth,
      LocalDate dateOfEntry, Integer grade, String username, String password,
      boolean isAccountNonExpired, boolean isAccountNonLocked, boolean isCredentialsNonExpired,
      boolean isEnabled, Set<Course> studentsCourses) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.indexNumber = indexNumber;
    this.dateOfBirth = dateOfBirth;
    this.dateOfEntry = dateOfEntry;
    this.grade = grade;
    this.username = username;
    this.password = password;
    this.isAccountNonExpired = isAccountNonExpired;
    this.isAccountNonLocked = isAccountNonLocked;
    this.isCredentialsNonExpired = isCredentialsNonExpired;
    this.isEnabled = isEnabled;
    this.studentsCourses = studentsCourses;
  }

  public void assignNewModule(Module newModule){
    studentsModules.add(newModule);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
    this.roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
    return authorities;
  }
}
