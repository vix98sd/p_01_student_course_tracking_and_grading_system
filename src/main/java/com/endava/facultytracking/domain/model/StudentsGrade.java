package com.endava.facultytracking.domain.model;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "Students_grade")
@Table(name = "students_grade")
public class StudentsGrade {

  @Setter(value = AccessLevel.NONE)
  @Id
  @SequenceGenerator(
      name = "students_grade_sequence",
      sequenceName = "students_grade_sequence",
      allocationSize = 1
  )
  @GeneratedValue(
      strategy = SEQUENCE,
      generator = "students_grade_sequence"
  )
  private Long id;
  @ManyToOne
  @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false)
  private Student student;
  @ManyToOne
  @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
  private Course course;
  private Integer points;
  private Integer grade;
  private Boolean isAnnulled;

  public StudentsGrade(
      Student student,
      Course course,
      Integer points,
      Integer grade
  ) {
    this.student = student;
    this.course = course;
    this.points = points;
    this.grade = grade;
    this.isAnnulled = false;
  }
}