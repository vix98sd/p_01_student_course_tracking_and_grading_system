package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Administrator;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {

  Optional<Administrator> findByUsername(String username);
}
