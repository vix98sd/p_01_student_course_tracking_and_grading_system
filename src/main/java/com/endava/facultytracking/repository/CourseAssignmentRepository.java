package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseAssignment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseAssignmentRepository extends JpaRepository<CourseAssignment, Long> {

  List<CourseAssignment> findAllByCourse(Course course);
}
