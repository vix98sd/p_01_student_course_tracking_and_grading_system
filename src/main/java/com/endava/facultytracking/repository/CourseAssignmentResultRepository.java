package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseAssignmentResultRepository extends
    JpaRepository<CourseAssignmentResult, Long> {

  List<CourseAssignmentResult> findAllByCourseAssignment(CourseAssignment courseAssignment);
}
