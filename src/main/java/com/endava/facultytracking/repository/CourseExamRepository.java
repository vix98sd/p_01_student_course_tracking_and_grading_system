package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseExamRepository extends JpaRepository<CourseExam, Long> {

  List<CourseExam> findAllByCourse(Course course);

  List<CourseExam> findAllByCourseAndStartingDateGreaterThanEqual(Course course, LocalDate today);
}
