package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseExamResultRepository extends JpaRepository<CourseExamResult, Long> {

  List<CourseExamResult> findAllByCourseExam(CourseExam courseExam);

  Optional<CourseExamResult> findAllByCourseExamAndStudentAndIsAnnulled(
      CourseExam courseExam,
      Student student,
      Boolean isAnnulled);
}
