package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseExamSubmissionRepository extends JpaRepository<CourseExamSubmission, Long> {

  List<CourseExamSubmission> findAllByFinalExamAndStatus(CourseExam finalExam,
      SubmissionStatus status);
}
