package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Course;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

  boolean existsByCodeName(String codeName);

  List<Course> findAllByIsApproved(Boolean b);
}
