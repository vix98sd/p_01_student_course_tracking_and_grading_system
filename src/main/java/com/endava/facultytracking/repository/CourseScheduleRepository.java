package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSchedule;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseScheduleRepository extends JpaRepository<CourseSchedule, Long> {


  List<CourseSchedule> findByStartingDateAndIsCanceledOrderByStartingDate(LocalDate startingDate,
      Boolean isCanceled);

  @Query(value = "from Course_schedule cs where cs.startingDate BETWEEN :startDate AND :endDate AND cs.isCanceled = :isCanceled ORDER BY cs.startingDate")
  public List<CourseSchedule> findAllBetweenDatesAndIsCanceled(
      @Param("startDate") LocalDate startDate,
      @Param("endDate") LocalDate endDate,
      @Param("isCanceled") Boolean isCanceled);

  List<CourseSchedule> findAllByCourse(Course course);
}
