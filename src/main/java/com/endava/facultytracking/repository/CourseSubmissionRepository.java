package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseSubmissionRepository extends JpaRepository<CourseSubmission, Long> {

  List<CourseSubmission> findAllByCourseAndIsResolved(Course course, Boolean isResolved);
}
