package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Lecturer;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LecturerRepository extends JpaRepository<Lecturer, Long> {

  Optional<Lecturer> findByUsername(String username);
}