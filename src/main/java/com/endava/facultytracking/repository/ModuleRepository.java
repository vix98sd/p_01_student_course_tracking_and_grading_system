package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleRepository extends JpaRepository<Module, Long> {

  boolean existsByCodeName(String codeName);
}
