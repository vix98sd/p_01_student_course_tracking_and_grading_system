package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

  boolean existsByUsername(String username);

  Optional<Student> findByUsername(String username);
}
