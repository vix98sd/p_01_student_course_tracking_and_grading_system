package com.endava.facultytracking.repository;

import com.endava.facultytracking.domain.model.StudentsGrade;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentsGradeRepository extends JpaRepository<StudentsGrade, Long> {

  List<StudentsGrade> findAllByStudentIdAndIsAnnulled(Long studentId, Boolean isAnnulled);

  List<StudentsGrade> findAllByCourseIdAndIsAnnulled(Long courseId, Boolean isAnnulled);

  Optional<StudentsGrade> findAllByStudentIdAndCourseIdAndIsAnnulled(Long studentId, Long courseId,
      Boolean isAnnulled);

}
