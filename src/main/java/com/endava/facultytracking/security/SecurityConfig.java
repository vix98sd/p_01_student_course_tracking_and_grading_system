package com.endava.facultytracking.security;

import static com.endava.facultytracking.utility.PathProvider.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import com.endava.facultytracking.security.filter.CustomAuthenticationFilter;
import com.endava.facultytracking.security.filter.CustomAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final UserDetailsService userDetailsService;
  private final BCryptPasswordEncoder passwordEncoder;

  private final String ROLE_ADMIN = "ROLE_ADMIN";
  private final String ROLE_LECTURER = "ROLE_LECTURER";
  private final String ROLE_STUDENT = "ROLE_STUDENT";

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    CustomAuthenticationFilter customAuthenticationFilter =
        new CustomAuthenticationFilter(authenticationManagerBean());
    customAuthenticationFilter.setFilterProcessesUrl(LOGIN_ENDPOINT);

    http.csrf().disable();

    http
        .sessionManagement().sessionCreationPolicy(STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(LOGIN_ENDPOINT.concat("/**"),
            "/swagger-ui/*", "/swagger-ui.html", "/webjars/**", "/v2/**", "/swagger-resources/**").permitAll()

        .antMatchers(HttpMethod.GET, COURSE_ENDPOINT.concat("{\\d+}"),
            COURSE_ENDPOINT.concat("{\\d+}/students/**"),
            COURSE_ENDPOINT.concat("submissions/revoke/{\\d+}"),
            COURSE_SCHEDULE_ENDPOINT.concat("{\\d+}/{\\d+}/{\\d+}"),
            COURSE_SCHEDULE_ENDPOINT.concat("{\\d+}"),
            COURSE_EXAM_ENDPOINT.concat("{\\d+}"),
            COURSE_EXAM_ENDPOINT.concat("courses/{\\d+}"),
            COURSE_EXAM_ENDPOINT.concat("active/courses/{\\d+}"),
            COURSE_EXAM_ENDPOINT.concat("{\\d+}/results"),
            COURSE_ASSIGNMENT_ENDPOINT.concat("{\\d+}"),
            COURSE_ASSIGNMENT_ENDPOINT.concat("courses/{\\d+}"),
            COURSE_ASSIGNMENT_ENDPOINT.concat("{\\d+}/results")
        ).hasAnyAuthority(ROLE_STUDENT, ROLE_LECTURER, ROLE_ADMIN)

        .antMatchers(COURSE_ENDPOINT.concat("**"), COURSE_ASSIGNMENT_ENDPOINT.concat("**"),
            COURSE_EXAM_ENDPOINT.concat("**"), COURSE_EXAM_SUBMISSION_ENDPOINT.concat("**"),
            STUDENTS_GRADE_ENDPOINT.concat("**")).hasAnyAuthority(ROLE_LECTURER, ROLE_ADMIN)
        .antMatchers(HttpMethod.GET, LECTURER_ENDPOINT.concat("{\\d+}/courses"))
        .hasAnyAuthority(ROLE_LECTURER, ROLE_ADMIN)
        .antMatchers(HttpMethod.PUT,
            COURSE_SCHEDULE_ENDPOINT.concat("{\\d+}/attendances/students/**"))
        .hasAnyAuthority(ROLE_LECTURER, ROLE_ADMIN)

        .antMatchers("/api/v1/**").hasAuthority(ROLE_ADMIN)
        .anyRequest()
        .authenticated()
        .and()
        .addFilter(customAuthenticationFilter)
        .addFilterAfter(new CustomAuthorizationFilter(),
            UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
