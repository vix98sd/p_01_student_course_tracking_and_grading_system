package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Administrator;
import java.util.List;

public interface AdministratorService {

  Administrator createNewAdministrator(Administrator newAdministrator);

  Administrator getAdministrator(Long administratorId);

  Administrator getAdministrator(String username);

  List<Administrator> getAllAdministrators();

  void updateAdministratorInfo(Administrator newAdministratorInfo);

  void deleteAdministrator(Long administratorId);
}
