package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.repository.AdministratorRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AdministratorServiceImpl implements AdministratorService {

  private static final String ADMINISTRATOR_NOT_FOUND = "administrator with id %s was not found";
  private static final String ADMINISTRATOR_USERNAME_NOT_FOUND = "administrator with username %s was not found";
  private final AdministratorRepository administratorRepository;
  private final PasswordEncoder passwordEncoder;

  @Override
  public Administrator createNewAdministrator(Administrator newAdministrator) {
    if (newAdministrator.getFirstName() == null ||
        newAdministrator.getLastName() == null ||
        newAdministrator.getUsername() == null ||
        newAdministrator.getPassword() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }
    newAdministrator.setPassword(passwordEncoder.encode(newAdministrator.getPassword()));
    newAdministrator.setAccountNonExpired(true);
    newAdministrator.setAccountNonLocked(true);
    newAdministrator.setCredentialsNonExpired(true);
    newAdministrator.setEnabled(true);

    return administratorRepository.save(newAdministrator);
  }

  @Override
  public Administrator getAdministrator(Long administratorId) {
    if (administratorId == null) {
      throw new RecordNotFoundException(String.format(ADMINISTRATOR_NOT_FOUND, administratorId));
    }

    return administratorRepository.findById(administratorId).orElseThrow(() ->
        new RecordNotFoundException(String.format(ADMINISTRATOR_NOT_FOUND, administratorId))
    );
  }

  @Override
  public Administrator getAdministrator(String username) {
    if (username == null) {
      throw new RecordNotFoundException(String.format(ADMINISTRATOR_USERNAME_NOT_FOUND, username));
    }

    return administratorRepository.findByUsername(username).orElseThrow(() ->
        new RecordNotFoundException(String.format(ADMINISTRATOR_USERNAME_NOT_FOUND, username))
    );
  }

  @Override
  public List<Administrator> getAllAdministrators() {
    return administratorRepository.findAll();
  }

  @Override
  @Transactional
  public void updateAdministratorInfo(Administrator newAdministratorInfo) {
    Administrator administratorToBeUpdated = this.getAdministrator(newAdministratorInfo.getId());

    if (newAdministratorInfo.getFirstName() != null &&
        !newAdministratorInfo.getFirstName().equals(administratorToBeUpdated.getFirstName())
    ) {
      administratorToBeUpdated.setFirstName(newAdministratorInfo.getFirstName());
    }

    if (newAdministratorInfo.getLastName() != null &&
        !newAdministratorInfo.getLastName().equals(administratorToBeUpdated.getLastName())
    ) {
      administratorToBeUpdated.setLastName(newAdministratorInfo.getLastName());
    }

    if (newAdministratorInfo.getPassword() != null &&
        !newAdministratorInfo.getPassword().equals(administratorToBeUpdated.getPassword())
    ) {
      administratorToBeUpdated.setPassword(newAdministratorInfo.getPassword());
    }
  }

  @Override
  public void deleteAdministrator(Long administratorId) {
    administratorRepository.delete(this.getAdministrator(administratorId));
  }
}
