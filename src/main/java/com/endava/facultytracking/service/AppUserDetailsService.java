package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.domain.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AppUserDetailsService implements UserDetailsService {

  private final AdministratorService administratorService;
  private final LecturerService lecturerService;
  private final StudentService studentService;
  private final RoleService roleService;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException  {

    try {
      return administratorService.getAdministrator(username);
    } catch (RecordNotFoundException exceptionAdmin) {
      try {
        return lecturerService.getLecturer(username);
      } catch (RecordNotFoundException exceptionLecturer) {
        try {
          return studentService.getStudent(username);
        } catch (RecordNotFoundException exceptionStudent) {
          throw new RecordNotFoundException(
              String.format("user with username %s was not found", username));
        }
      }
    }
  }

  @Transactional
  public void addRoleToUser(String username, String roleName) {
    Role role = roleService.getRole(roleName);
    try {
      Administrator administrator = administratorService.getAdministrator(username);
      administrator.getRoles().add(role);
    } catch (RecordNotFoundException exceptionAdmin) {
      try {
        Lecturer lecturer = lecturerService.getLecturer(username);
        lecturer.getRoles().add(role);
      } catch (RecordNotFoundException exceptionLecturer) {
        try {
          Student lecturer = studentService.getStudent(username);
          lecturer.getRoles().add(role);
        } catch (RecordNotFoundException exceptionStudent) {
          throw new RecordNotFoundException(
              String.format("user with username %s was not found", username));
        }
      }
    }
  }

  public void createNewUser(Administrator administrator){
    administratorService.createNewAdministrator(administrator);
  }

  public void createNewUser(Lecturer lecturer){
    lecturerService.createNewLecturer(lecturer);
  }

  public void createNewUser(Student student){
    studentService.createNewStudent(student);
  }

  public void createNewRole(Role role){
    roleService.saveRole(role);
  }
}
