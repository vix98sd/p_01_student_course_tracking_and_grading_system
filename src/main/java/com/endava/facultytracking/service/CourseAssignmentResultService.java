package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import java.util.List;

public interface CourseAssignmentResultService {

  public CourseAssignmentResult addNewCourseAssignmentResult(
      CourseAssignmentResult newCourseAssignmentResult);

  public CourseAssignmentResult getCourseAssignmentResult(Long courseAssignmentResultId);

  public List<CourseAssignmentResult> getCourseAssignmentResults(CourseAssignment courseAssignment);

  public void deleteCourseAssignmentResult(Long courseAssignmentResultId);
}
