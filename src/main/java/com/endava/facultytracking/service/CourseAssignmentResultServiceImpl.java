package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.repository.CourseAssignmentResultRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourseAssignmentResultServiceImpl implements CourseAssignmentResultService {

  private static final String COURSE_ASSIGNMENT_RESULT_NOT_FOUND = "course assignment result with id %s does not exist";
  private final CourseAssignmentResultRepository courseAssignmentResultRepository;
  private final StudentService studentService;

  @Override
  public CourseAssignmentResult addNewCourseAssignmentResult(
      CourseAssignmentResult newCourseAssignmentResult) {

    if (newCourseAssignmentResult.getCourseAssignment() == null ||
        newCourseAssignmentResult.getStudent() == null ||
        newCourseAssignmentResult.getStudent().getId() == null ||
        newCourseAssignmentResult.getPercentDone() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    newCourseAssignmentResult.setStudent(
        studentService.getStudent(newCourseAssignmentResult.getStudent().getId()));

    return courseAssignmentResultRepository.save(newCourseAssignmentResult);
  }

  @Override
  public CourseAssignmentResult getCourseAssignmentResult(Long courseAssignmentResultId) {
    if (courseAssignmentResultId == null) {
      throw new RecordNotFoundException(
          String.format(COURSE_ASSIGNMENT_RESULT_NOT_FOUND, courseAssignmentResultId));
    }

    return courseAssignmentResultRepository.findById(courseAssignmentResultId).orElseThrow(() ->
        new RecordNotFoundException(
            String.format(COURSE_ASSIGNMENT_RESULT_NOT_FOUND, courseAssignmentResultId))
    );
  }

  @Override
  public List<CourseAssignmentResult> getCourseAssignmentResults(
      CourseAssignment courseAssignment) {
    return courseAssignmentResultRepository.findAllByCourseAssignment(courseAssignment);
  }

  @Override
  public void deleteCourseAssignmentResult(Long courseAssignmentResultId) {
    courseAssignmentResultRepository.delete(
        this.getCourseAssignmentResult(courseAssignmentResultId));
  }
}
