package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import java.util.List;

public interface CourseAssignmentService {

  CourseAssignment addNewCourseAssignment(CourseAssignment newCourseAssignment);

  CourseAssignment getCourseAssignment(Long courseAssignmentId);

  List<CourseAssignment> getCourseAssignments(Long courseId);

  CourseAssignment updateCourseAssignmentInfo(CourseAssignment newCourseAssignmentInfo);

  void deleteCourseAssignment(Long courseAssignmentId);

  void addNewResult(Long courseAssignmentId, CourseAssignmentResult courseAssignmentResult);

  List<CourseAssignmentResult> getCourseAssignmentResults(Long courseAssignmentId);

  void deleteCourseAssignmentResult(Long courseAssignmentResultId);
}
