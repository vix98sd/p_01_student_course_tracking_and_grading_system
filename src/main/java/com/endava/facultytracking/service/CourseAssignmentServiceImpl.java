package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.repository.CourseAssignmentRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CourseAssignmentServiceImpl implements CourseAssignmentService {

  private static final String COURSE_ASSIGNMENT_NOT_FOUND = "course assignment with id %s does not exist";

  private final CourseAssignmentRepository courseAssignmentRepository;
  private final CourseService courseService;
  private final CourseAssignmentResultService courseAssignmentResultService;

  @Override
  public CourseAssignment addNewCourseAssignment(CourseAssignment newCourseAssignment) {

    if (newCourseAssignment.getCourse() == null ||
        newCourseAssignment.getCourse().getId() == null ||
        newCourseAssignment.getStartingDate() == null ||
        newCourseAssignment.getEndingDate() == null ||
        newCourseAssignment.getStartingTime() == null ||
        newCourseAssignment.getEndingTime() == null ||
        newCourseAssignment.getDescription() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    Course course = courseService.getCourse(newCourseAssignment.getCourse().getId());

    isAssignmentDuringSemester(course, newCourseAssignment);

    newCourseAssignment.setCourse(course);

    return courseAssignmentRepository.save(newCourseAssignment);
  }

  @Override
  public CourseAssignment getCourseAssignment(Long courseAssignmentId) {
    if (courseAssignmentId == null) {
      throw new RecordNotFoundException(
          String.format(COURSE_ASSIGNMENT_NOT_FOUND, courseAssignmentId));
    }

    return courseAssignmentRepository.findById(courseAssignmentId).orElseThrow(
        () -> new RecordNotFoundException(
            String.format(COURSE_ASSIGNMENT_NOT_FOUND, courseAssignmentId))
    );
  }

  @Override
  public List<CourseAssignment> getCourseAssignments(Long courseId) {
    return courseAssignmentRepository.findAllByCourse(courseService.getCourse(courseId));
  }

  @Override
  @Transactional
  public CourseAssignment updateCourseAssignmentInfo(CourseAssignment newCourseAssignmentInfo) {
    CourseAssignment courseAssignmentToBeUpdated = this.getCourseAssignment(
        newCourseAssignmentInfo.getId());

    if (newCourseAssignmentInfo.getStartingDate() != null &&
        newCourseAssignmentInfo.getEndingDate() != null &&
        newCourseAssignmentInfo.getStartingTime() != null &&
        newCourseAssignmentInfo.getStartingTime() != null
    ) {
      isAssignmentDuringSemester(courseAssignmentToBeUpdated.getCourse(), newCourseAssignmentInfo);
      courseAssignmentToBeUpdated.setStartingDate(newCourseAssignmentInfo.getStartingDate());
      courseAssignmentToBeUpdated.setEndingDate(newCourseAssignmentInfo.getEndingDate());
      courseAssignmentToBeUpdated.setStartingTime(newCourseAssignmentInfo.getStartingTime());
      courseAssignmentToBeUpdated.setEndingTime(newCourseAssignmentInfo.getEndingTime());
    }

    if (newCourseAssignmentInfo.getDescription() != null &&
        !newCourseAssignmentInfo.getDescription()
            .equals(courseAssignmentToBeUpdated.getDescription())
    ) {
      courseAssignmentToBeUpdated.setDescription(newCourseAssignmentInfo.getDescription());
    }

    return courseAssignmentToBeUpdated;
  }

  @Override
  public void deleteCourseAssignment(Long courseAssignmentId) {
    courseAssignmentRepository.delete(this.getCourseAssignment(courseAssignmentId));
  }

  @Override
  public void addNewResult(Long courseAssignmentId, CourseAssignmentResult courseAssignmentResult) {
    CourseAssignment courseAssignment = this.getCourseAssignment(courseAssignmentId);
    courseAssignmentResult.setCourseAssignment(courseAssignment);
    courseAssignmentResultService.addNewCourseAssignmentResult(courseAssignmentResult);
  }

  @Override
  public List<CourseAssignmentResult> getCourseAssignmentResults(Long courseAssignmentId) {
    return courseAssignmentResultService.getCourseAssignmentResults(
        this.getCourseAssignment(courseAssignmentId));
  }

  @Override
  public void deleteCourseAssignmentResult(Long courseAssignmentResultId) {
    courseAssignmentResultService.deleteCourseAssignmentResult(courseAssignmentResultId);
  }

  private void isAssignmentDuringSemester(Course course, CourseAssignment newCourseAssignment) {
    if (newCourseAssignment.getStartingDate().isBefore(course.getSemester().getStartingDate()) ||
        newCourseAssignment.getEndingDate().isAfter(course.getSemester().getEndingDate()) ||
        newCourseAssignment.getStartingDate().isAfter(newCourseAssignment.getEndingDate()) ||
        (newCourseAssignment.getStartingDate().equals(newCourseAssignment.getEndingDate()) &&
            newCourseAssignment.getStartingTime().isAfter(newCourseAssignment.getEndingTime())
        )
    ) {
      throw new InvalidArgumentException("course assignment must be during the semester");
    }
  }
}
