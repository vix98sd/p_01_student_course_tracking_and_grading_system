package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import java.util.List;

public interface CourseExamResultService {

  public CourseExamResult addNewCourseExamResult(
      CourseExamResult newCourseExamResult);

  public CourseExamResult getCourseExamResult(Long courseExamResultId);

  public List<CourseExamResult> getCourseExamResults(CourseExam courseExam);

  void updateCourseExamResult(Long courseExamResultId, CourseExamResult newCourseExamResult);

  public void deleteCourseExamResult(Long courseExamResultId);

  void changeCourseExamResultStatus(Long courseExamResultId, CourseExamResultStatus approved);
}
