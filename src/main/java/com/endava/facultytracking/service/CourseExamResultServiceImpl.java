package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.repository.CourseExamResultRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CourseExamResultServiceImpl implements CourseExamResultService {

  private static final String COURSE_EXAM_RESULT_NOT_FOUND = "course exam result with id %s was not found";
  private static final Boolean IS_ANNULLED = false;
  private final CourseExamResultRepository courseExamResultRepository;
  private final StudentService studentService;

  @Override
  public CourseExamResult addNewCourseExamResult(CourseExamResult newCourseExamResult) {
    if (newCourseExamResult.getCourseExam() == null ||
        newCourseExamResult.getStudent() == null ||
        newCourseExamResult.getStudent().getId() == null ||
        newCourseExamResult.getPercentDone() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    annulIfExists(newCourseExamResult);

    newCourseExamResult.setStudent(
        studentService.getStudent(newCourseExamResult.getStudent().getId()));

    return courseExamResultRepository.save(newCourseExamResult);
  }

  private void annulIfExists(CourseExamResult newCourseExamResult) {
    CourseExamResult courseExamResult = courseExamResultRepository
        .findAllByCourseExamAndStudentAndIsAnnulled(
            newCourseExamResult.getCourseExam(),
            newCourseExamResult.getStudent(),
            IS_ANNULLED
        ).orElse(null);

    if (courseExamResult != null) {
      courseExamResult.setIsAnnulled(true);
      courseExamResultRepository.save(courseExamResult);
    }
  }

  @Override
  public CourseExamResult getCourseExamResult(Long courseExamResultId) {
    if (courseExamResultId == null) {
      throw new RecordNotFoundException(
          String.format(COURSE_EXAM_RESULT_NOT_FOUND, courseExamResultId));
    }

    return courseExamResultRepository.findById(courseExamResultId).orElseThrow(() ->
        new RecordNotFoundException(
            String.format(COURSE_EXAM_RESULT_NOT_FOUND, courseExamResultId)));
  }

  @Override
  public List<CourseExamResult> getCourseExamResults(CourseExam courseExam) {
    return courseExamResultRepository.findAllByCourseExam(courseExam);
  }

  @Override
  @Transactional
  public void updateCourseExamResult(Long courseExamResultId,
      CourseExamResult newCourseExamResult) {
    CourseExamResult resultToBeUpdated = this.getCourseExamResult(courseExamResultId);

    if (newCourseExamResult.getPercentDone() != null &&
        !newCourseExamResult.getPercentDone().equals(resultToBeUpdated.getPercentDone())
    ) {
      resultToBeUpdated.setPercentDone(newCourseExamResult.getPercentDone());
      resultToBeUpdated.setIsPresent(true);
      resultToBeUpdated.setStatus(CourseExamResultStatus.WAITING_FOR_APPROVAL);
    }
  }

  @Override
  public void deleteCourseExamResult(Long courseExamResultId) {
    courseExamResultRepository.delete(this.getCourseExamResult(courseExamResultId));
  }

  @Override
  @Transactional
  public void changeCourseExamResultStatus(Long courseExamResultId,
      CourseExamResultStatus status) {
    CourseExamResult courseExamResult = this.getCourseExamResult(courseExamResultId);
    courseExamResult.setStatus(status);
  }
}
