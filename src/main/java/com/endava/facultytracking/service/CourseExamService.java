package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import java.util.List;

public interface CourseExamService {

  CourseExam addNewCourseExam(CourseExam newCourseExam);

  CourseExam getCourseExam(Long courseExamId);

  List<CourseExam> getCourseExams(Long courseId);

  List<CourseExam> getActiveCourseExams(Long courseId);

  CourseExam updateCourseExamInfo(CourseExam newCourseExamInfo);

  void deleteCourseExam(Long courseExamId);

  void addExamResult(Long courseExamId, CourseExamResult newCourseExamResult);

  List<CourseExamResult> getCourseExamResults(Long courseExamId);

  void deleteCourseExamResult(Long courseExamResultId);

  void createNewFinalExamSubmission(CourseExamSubmission courseExamSubmission);

  void approveFinalExamSubmission(Long finalExamSubmissionId);

  void rejectFinalExamSubmission(Long finalExamSubmissionId);

  void revokeFinalExamSubmission(Long finalExamSubmissionId);

  List<CourseExamSubmissionDto> getActiveSubmissions(Long finalCourseExamId, DtoMapper dtoMapper);

  void approveCourseExamResult(Long courseExamResultId);

  void rejectCourseExamResult(Long courseExamResultId);
}