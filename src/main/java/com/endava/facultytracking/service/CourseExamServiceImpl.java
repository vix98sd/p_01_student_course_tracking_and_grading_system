package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseExamRepository;
import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CourseExamServiceImpl implements CourseExamService {

  private static final String COURSE_EXAM_NOT_FOUND = "course exam with id %s does not exist";
  private final CourseExamRepository courseExamRepository;
  private final CourseService courseService;
  private final CourseExamResultService courseExamResultService;
  private final CourseExamSubmissionService courseExamSubmissionService;
  private final CourseScheduleService courseScheduleService;
  private final Clock clock;

  @Override
  public CourseExam addNewCourseExam(CourseExam newCourseExam) {

    if (newCourseExam.getCourse() == null ||
        newCourseExam.getCourse().getId() == null ||
        newCourseExam.getStartingDate() == null ||
        newCourseExam.getStartingTime() == null ||
        newCourseExam.getDescription() == null ||
        newCourseExam.getIsFinal() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    newCourseExam.setCourse(courseService.getCourse(newCourseExam.getCourse().getId()));

    if (newCourseExam.getIsFinal()) {

      if (newCourseExam.getStartingDate()
          .isBefore(newCourseExam.getCourse().getSemester().getEndingDate())) {
        throw new InvalidArgumentException("final exam must be after semester");
      }

      courseExamRepository.save(newCourseExam);

    } else {

      if (newCourseExam.getStartingDate()
          .isAfter(newCourseExam.getCourse().getSemester().getEndingDate()) ||
          newCourseExam.getStartingDate()
              .isBefore(newCourseExam.getCourse().getSemester().getStartingDate())
      ) {
        throw new InvalidArgumentException("exam must be during semester");
      }

      courseExamRepository.save(newCourseExam);

      newCourseExam
          .getCourse()
          .getEnrolledStudents()
          .forEach(student -> addCourseExamResultForStudent(newCourseExam, student));
    }

    return newCourseExam;
  }

  private void addCourseExamResultForStudent(CourseExam courseExam, Student student) {
    CourseExamResult courseExamResultForStudent = new CourseExamResult();
    courseExamResultForStudent.setStudent(student);
    courseExamResultForStudent.setCourseExam(courseExam);

    courseExamResultService.addNewCourseExamResult(courseExamResultForStudent);
  }

  @Override
  public CourseExam getCourseExam(Long courseExamId) {
    if (courseExamId == null) {
      throw new RecordNotFoundException(String.format(COURSE_EXAM_NOT_FOUND, courseExamId));
    }

    return courseExamRepository.findById(courseExamId).orElseThrow(() ->
        new RecordNotFoundException(String.format(COURSE_EXAM_NOT_FOUND, courseExamId)));
  }

  @Override
  public List<CourseExam> getCourseExams(Long courseId) {
    return courseExamRepository.findAllByCourse(courseService.getCourse(courseId));
  }

  @Override
  public List<CourseExam> getActiveCourseExams(Long courseId) {
    return courseExamRepository.findAllByCourseAndStartingDateGreaterThanEqual(
        courseService.getCourse(courseId),
        LocalDate.now(clock));
  }

  @Override
  @Transactional
  public CourseExam updateCourseExamInfo(CourseExam newCourseExamInfo) {
    CourseExam courseExamToBeUpdated = this.getCourseExam(newCourseExamInfo.getId());

    if (newCourseExamInfo.getStartingDate() != null &&
        !newCourseExamInfo.getStartingDate().equals(courseExamToBeUpdated.getStartingDate())
    ) {
      courseExamToBeUpdated.setStartingDate(newCourseExamInfo.getStartingDate());
    }

    if (newCourseExamInfo.getStartingTime() != null &&
        !newCourseExamInfo.getStartingTime().equals(courseExamToBeUpdated.getStartingTime())
    ) {
      courseExamToBeUpdated.setStartingTime(newCourseExamInfo.getStartingTime());
    }

    if (newCourseExamInfo.getDescription() != null &&
        !newCourseExamInfo.getDescription().equals(courseExamToBeUpdated.getDescription())
    ) {
      courseExamToBeUpdated.setDescription(newCourseExamInfo.getDescription());
    }

    if (newCourseExamInfo.getIsFinal() != null &&
        !newCourseExamInfo.getIsFinal().equals(courseExamToBeUpdated.getIsFinal())
    ) {
      courseExamToBeUpdated.setIsFinal(newCourseExamInfo.getIsFinal());
    }

    return courseExamToBeUpdated;
  }

  @Override
  public void deleteCourseExam(Long courseExamId) {
    courseExamRepository.delete(this.getCourseExam(courseExamId));
  }

  @Override
  public void addExamResult(Long courseExamId, CourseExamResult newCourseExamResult) {

    CourseExam courseExam = this.getCourseExam((courseExamId));
    CourseExamResult resultToBeUpdated = courseExam.getResults().stream()
        .filter(result ->
            result.getStudent().getId().equals(newCourseExamResult.getStudent().getId()))
        .findFirst().orElse(null);

    if (resultToBeUpdated != null && !resultToBeUpdated.getIsPresent()) {
      courseExamResultService.updateCourseExamResult(resultToBeUpdated.getId(),
          newCourseExamResult);
    } else {
      newCourseExamResult.setCourseExam(courseExam);
      newCourseExamResult.setIsPresent(true);
      courseExamResultService.addNewCourseExamResult(newCourseExamResult);
    }
  }

  @Override
  public List<CourseExamResult> getCourseExamResults(Long courseExamId) {
    return courseExamResultService.getCourseExamResults(
        this.getCourseExam(courseExamId));
  }

  @Override
  public void deleteCourseExamResult(Long courseExamResultId) {
    courseExamResultService.deleteCourseExamResult(courseExamResultId);
  }

  @Override
  public void createNewFinalExamSubmission(CourseExamSubmission courseExamSubmission) {
    courseExamSubmissionService.addNewFinalExamSubmission(courseExamSubmission);
  }

  @Override
  public void approveFinalExamSubmission(Long finalExamSubmissionId) {
    CourseExamSubmission courseExamSubmission = courseExamSubmissionService.resolveFinalExamSubmission(
        finalExamSubmissionId, SubmissionStatus.APPROVED);

    CourseExamResult courseExamResultForSubmission = new CourseExamResult();

    courseExamResultForSubmission.setCourseExam(courseExamSubmission.getFinalExam());
    courseExamResultForSubmission.setStudent(courseExamSubmission.getStudent());

    courseExamResultService.addNewCourseExamResult(courseExamResultForSubmission);
  }

  @Override
  public void rejectFinalExamSubmission(Long finalExamSubmissionId) {
    courseExamSubmissionService.resolveFinalExamSubmission(finalExamSubmissionId,
        SubmissionStatus.REJECTED);
  }

  @Override
  public void revokeFinalExamSubmission(Long finalExamSubmissionId) {
    courseExamSubmissionService.resolveFinalExamSubmission(finalExamSubmissionId,
        SubmissionStatus.REVOKED);
  }

  @Override
  public List<CourseExamSubmissionDto> getActiveSubmissions(Long finalCourseExamId,
      DtoMapper dtoMapper) {
    CourseExam finalExam = this.getCourseExam(finalCourseExamId);
    List<CourseExamSubmission> activeSubmissions =
        courseExamSubmissionService.getFinalExamSubmissions(finalExam);

    List<CourseSchedule> courseSchedules = courseScheduleService.getAllSchedulesForCourse(
        finalExam.getCourse().getId());

    List<CourseExam> activeExams = this.getCourseExams(finalExam.getCourse().getId())
        .stream().filter(courseExam -> !courseExam.getIsFinal()).collect(Collectors.toList());

    List<CourseExamResult> courseExamResults =
        activeExams.stream()
            .map(courseExam -> this.getCourseExamResults(courseExam.getId()))
            .flatMap(List::stream)
            .collect(Collectors.toList());

    return activeSubmissions.stream().map(activeSubmission ->
            dtoMapper.modelToDto(activeSubmission, courseSchedules, courseExamResults))
        .collect(Collectors.toList());
  }

  @Override
  public void approveCourseExamResult(Long courseExamResultId) {
    courseExamResultService.changeCourseExamResultStatus(courseExamResultId,
        CourseExamResultStatus.APPROVED);
  }

  @Override
  public void rejectCourseExamResult(Long courseExamResultId) {
    courseExamResultService.changeCourseExamResultStatus(courseExamResultId,
        CourseExamResultStatus.REJECTED);
  }
}
