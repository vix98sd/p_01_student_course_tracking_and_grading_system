package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import java.util.List;

public interface CourseExamSubmissionService {

  void addNewFinalExamSubmission(CourseExamSubmission newCourseExamSubmission);

  CourseExamSubmission getFinalExamSubmission(Long courseExamSubmissionId);

  List<CourseExamSubmission> getFinalExamSubmissions(CourseExam finalExam);

  CourseExamSubmission resolveFinalExamSubmission(Long courseExamSubmissionId,
      SubmissionStatus submissionStatus);
}