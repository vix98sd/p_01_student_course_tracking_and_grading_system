package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.repository.CourseExamSubmissionRepository;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourseExamSubmissionServiceImpl implements CourseExamSubmissionService {

  private static final String COURSE_EXAM_SUBMISSION_WAS_NOT_FOUND = "course exam submission with id %s was not found";
  private final CourseExamSubmissionRepository courseExamSubmissionRepository;
  private final Clock clock;

  @Override
  public void addNewFinalExamSubmission(CourseExamSubmission newCourseExamSubmission) {
    if (newCourseExamSubmission.getStudent() == null ||
        newCourseExamSubmission.getStudent().getId() == null ||
        newCourseExamSubmission.getFinalExam() == null ||
        newCourseExamSubmission.getFinalExam().getId() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    courseExamSubmissionRepository.save(newCourseExamSubmission);
  }

  @Override
  public CourseExamSubmission getFinalExamSubmission(Long courseExamSubmissionId) {
    if (courseExamSubmissionId == null) {
      throw new RecordNotFoundException(
          String.format(COURSE_EXAM_SUBMISSION_WAS_NOT_FOUND, courseExamSubmissionId));
    }

    return courseExamSubmissionRepository.findById(courseExamSubmissionId).orElseThrow(() ->
        new RecordNotFoundException(
            String.format(COURSE_EXAM_SUBMISSION_WAS_NOT_FOUND, courseExamSubmissionId)));
  }

  @Override
  public List<CourseExamSubmission> getFinalExamSubmissions(CourseExam finalExam) {
    return courseExamSubmissionRepository.findAllByFinalExamAndStatus(finalExam,
        SubmissionStatus.WAITING_FOR_APPROVAL);
  }

  @Override
  public CourseExamSubmission resolveFinalExamSubmission(Long courseExamSubmissionId,
      SubmissionStatus submissionStatus) {
    CourseExamSubmission courseExamSubmissionToBeResolved =
        this.getFinalExamSubmission(courseExamSubmissionId);

    courseExamSubmissionToBeResolved.setDateResolved(LocalDate.now(clock));
    courseExamSubmissionToBeResolved.setIsResolved(true);
    courseExamSubmissionToBeResolved.setStatus(submissionStatus);

    return courseExamSubmissionRepository.save(courseExamSubmissionToBeResolved);
  }
}