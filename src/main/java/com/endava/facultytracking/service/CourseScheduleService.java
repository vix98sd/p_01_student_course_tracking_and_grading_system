package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Student;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface CourseScheduleService {

  CourseSchedule addNewCourseSchedule(Long courseId, CourseSchedule newCourseSchedule);

  List<CourseSchedule> addNewCourseScheduleSeries(Long courseId, CourseSchedule newCourseSchedule);

  List<CourseSchedule> getCurrentFullScheduleForWeek(LocalDate dayLocalDate);

  List<CourseSchedule> getAllSchedulesForCourse(Long courseId);

  CourseSchedule getCourseScheduleById(Long courseScheduleId);

  CourseSchedule updateCourseScheduleInfo(Long courseScheduleId,
      CourseSchedule newCourseScheduleInfo);

  void cancelCourseSchedule(Long courseScheduleId, Boolean cancelSeries);

  void addAttendee(Long courseScheduleId, Long studentId);

  void addAttendees(Long courseScheduleId, List<Long> studentIds);

  void removeAttendee(Long courseScheduleId, Long studentId);

  Set<Student> getAttended(Long courseScheduleId);
}
