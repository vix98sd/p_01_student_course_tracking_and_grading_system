package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseScheduleRepository;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CourseScheduleServiceImpl implements CourseScheduleService {

  private final CourseScheduleRepository courseScheduleRepository;
  private final CourseService courseService;
  private final StudentService studentService;

  private static final Boolean IS_CANCELED = false;
  private static final String COURSE_SCHEDULE_NOT_FOUND = "course schedule with id %s was not found";

  @Override
  public CourseSchedule addNewCourseSchedule(Long courseId, CourseSchedule newCourseSchedule) {

    checkForCourseScheduleArguments(courseId, newCourseSchedule);

    Course newCourse = courseService.getCourse(courseId);
    newCourseSchedule.setCourse(newCourse);

    isCourseScheduleDuringSemester(newCourse, newCourseSchedule);

    checkForOverlapping(newCourseSchedule);

    return courseScheduleRepository.save(newCourseSchedule);
  }

  @Override
  public List<CourseSchedule> addNewCourseScheduleSeries(Long courseId,
      CourseSchedule newCourseSchedule) {

    checkForCourseScheduleArguments(courseId, newCourseSchedule);

    Course newCourse = courseService.getCourse(courseId);
    newCourseSchedule.setCourse(newCourse);

    isCourseScheduleDuringSemester(newCourse, newCourseSchedule);

    List<CourseSchedule> newCourseScheduleSeries = new ArrayList<>();

    int weeksLeft = (int) ChronoUnit.WEEKS.between(newCourseSchedule.getStartingDate(),
        newCourse.getSemester().getEndingDate()) + 1;

    IntStream.range(0, weeksLeft).forEach(e -> {
          checkForOverlapping(newCourseSchedule);
          newCourseScheduleSeries.add(new CourseSchedule(newCourseSchedule));
          newCourseSchedule.setStartingDate(newCourseSchedule.getStartingDate().plusWeeks(1));
        }
    );

    return courseScheduleRepository.saveAll(newCourseScheduleSeries);
  }

  @Override
  public List<CourseSchedule> getCurrentFullScheduleForWeek(LocalDate dayLocalDate) {

    LocalDate mondayLocalDate = dayLocalDate.minusDays(dayLocalDate.getDayOfWeek().getValue() - 1);

    return courseScheduleRepository.findAllBetweenDatesAndIsCanceled(mondayLocalDate,
        mondayLocalDate.plusDays(7), IS_CANCELED);
  }

  @Override
  public List<CourseSchedule> getAllSchedulesForCourse(Long courseId) {
    return courseScheduleRepository.findAllByCourse(this.courseService.getCourse(courseId));
  }

  @Override
  public CourseSchedule getCourseScheduleById(Long courseScheduleId) {
    if (courseScheduleId == null) {
      throw new RecordNotFoundException(String.format(COURSE_SCHEDULE_NOT_FOUND, courseScheduleId));
    }

    return courseScheduleRepository.findById(courseScheduleId).orElseThrow(() ->
        new RecordNotFoundException(String.format(COURSE_SCHEDULE_NOT_FOUND, courseScheduleId)));
  }

  @Override
  @Transactional
  public CourseSchedule updateCourseScheduleInfo(Long courseScheduleId,
      CourseSchedule newCourseScheduleInfo) {

    CourseSchedule courseScheduleToBeUpdated = this.getCourseScheduleById(courseScheduleId);

    if (newCourseScheduleInfo.getStartingDate() != null &&
        !newCourseScheduleInfo.getStartingDate().equals(courseScheduleToBeUpdated.getStartingDate())
        &&
        newCourseScheduleInfo.getStartingTime() != null &&
        !newCourseScheduleInfo.getStartingTime().equals(courseScheduleToBeUpdated.getStartingTime())
    ) {

      isCourseScheduleDuringSemester(courseScheduleToBeUpdated.getCourse(), newCourseScheduleInfo);

      checkForOverlapping(newCourseScheduleInfo);

      courseScheduleToBeUpdated.setStartingDate(newCourseScheduleInfo.getStartingDate());
      courseScheduleToBeUpdated.setStartingTime(newCourseScheduleInfo.getStartingTime());
    }

    if (newCourseScheduleInfo.getNumberOfClasses() != null &&
        !newCourseScheduleInfo.getNumberOfClasses()
            .equals(courseScheduleToBeUpdated.getNumberOfClasses())
    ) {
      courseScheduleToBeUpdated.setNumberOfClasses(newCourseScheduleInfo.getNumberOfClasses());
    }

    if (newCourseScheduleInfo.getType() != null &&
        !newCourseScheduleInfo.getType().equals(courseScheduleToBeUpdated.getType())
    ) {
      courseScheduleToBeUpdated.setType(newCourseScheduleInfo.getType());
    }

    return courseScheduleToBeUpdated;
  }

  @Override
  @Transactional
  public void cancelCourseSchedule(Long courseScheduleId, Boolean cancelSeries) {
    if (cancelSeries) {
      // TODO : Add series canceling
    } else {
      CourseSchedule courseSchedule = this.getCourseScheduleById(courseScheduleId);
      courseSchedule.setIsCanceled(true);
    }
  }

  @Override
  public void addAttendee(Long courseScheduleId, Long studentId) {
    CourseSchedule courseSchedule = this.getCourseScheduleById(courseScheduleId);
    Student attendee = studentService.getStudent(studentId);
    courseSchedule.addAttendee(attendee);
    courseScheduleRepository.save(courseSchedule);
  }

  @Override
  public void addAttendees(Long courseScheduleId, List<Long> studentIds) {
    CourseSchedule courseSchedule = this.getCourseScheduleById(courseScheduleId);
    List<Student> attendees = new ArrayList<>();
    studentIds.forEach(id -> attendees.add(studentService.getStudent(id)));
    courseSchedule.addAttendees(attendees);
    courseScheduleRepository.save(courseSchedule);
  }

  @Override
  public void removeAttendee(Long courseScheduleId, Long studentId) {
    CourseSchedule courseSchedule = this.getCourseScheduleById(courseScheduleId);
    Student attendee = studentService.getStudent(studentId);
    courseSchedule.removeAttendee(attendee);
    courseScheduleRepository.save(courseSchedule);
  }

  @Override
  public Set<Student> getAttended(Long courseScheduleId) {
    return this.getCourseScheduleById(courseScheduleId).getAttended();
  }

  private void checkForOverlapping(CourseSchedule newCourseSchedule){

    List<CourseSchedule> courseSchedules = courseScheduleRepository
        .findByStartingDateAndIsCanceledOrderByStartingDate(
            newCourseSchedule.getStartingDate(), IS_CANCELED);

    courseSchedules.stream()
        .filter(courseSchedule ->
            courseSchedule.getCourse().getModule().equals(newCourseSchedule.getCourse().getModule()))
        .filter(courseSchedule ->
            courseSchedule.getCourse().getSemester().equals(newCourseSchedule.getCourse().getSemester()))
        .forEach(courseSchedule -> checkForTimeOverlapping(courseSchedule, newCourseSchedule));
  }

  private void checkForTimeOverlapping(CourseSchedule existingCourseSchedule,
      CourseSchedule newCourseSchedule) {
    LocalTime oldCourseStartingTime = existingCourseSchedule.getStartingTime();
    LocalTime oldCourseEndingTime = oldCourseStartingTime.plusHours(
        existingCourseSchedule.getNumberOfClasses());
    LocalTime newCourseStartingTime = newCourseSchedule.getStartingTime();
    LocalTime newCourseEndingTime = newCourseStartingTime.plusHours(
        existingCourseSchedule.getNumberOfClasses());

    if ((newCourseStartingTime.isBefore(oldCourseEndingTime) &&
            newCourseStartingTime.isAfter(oldCourseStartingTime)) ||
            (newCourseEndingTime.isBefore(oldCourseEndingTime) &&
                newCourseEndingTime.isAfter(oldCourseStartingTime)) ||
            (newCourseStartingTime.equals(oldCourseStartingTime) &&
                newCourseEndingTime.equals(oldCourseEndingTime))
    ) {
      throw new RecordAlreadyExistException(String.format("your course overlaps with %1$s on %2$s",
          existingCourseSchedule.getCourse().getName(), existingCourseSchedule.getStartingDate()));
    }
  }

  private void checkForCourseScheduleArguments(Long courseId, CourseSchedule newCourseSchedule) {
    if (courseId == null ||
        newCourseSchedule.getStartingDate() == null ||
        newCourseSchedule.getStartingTime() == null ||
        newCourseSchedule.getNumberOfClasses() == null ||
        newCourseSchedule.getType() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

  }

  private void isCourseScheduleDuringSemester(Course newCourse, CourseSchedule newCourseSchedule) {
    if (newCourse.getSemester().getStartingDate().isAfter(newCourseSchedule.getStartingDate()) ||
        newCourse.getSemester().getEndingDate().isBefore(newCourseSchedule.getStartingDate())
    ) {
      throw new InvalidArgumentException("course schedule must be during the semester");
    }
  }
}