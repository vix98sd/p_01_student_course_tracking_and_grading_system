package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Student;
import java.util.List;
import java.util.Set;
import org.springframework.web.bind.annotation.PathVariable;

public interface CourseService {

  Course createNewCourse(Course course);

  List<Course> getAllCourses();

  Course getCourse(Long courseId);

  Course updateCourseInfo(Long courseId, Course course);

  void deleteCourse(Long courseId);

  void enrollStudent(Long courseId, Long studentId);

  Set<Student> getEnrolledStudents(Long courseId);

  void assignLecturer(Long courseId, Long lecturerId);

  Set<Lecturer> getCourseLecturers(Long courseId);

  List<CourseSubmission> getActiveSubmissions(Long courseId);

  void approveCourseSubmission(Long courseSubmissionId);

  void rejectCourseSubmission(Long courseSubmissionId);

  void revokeCourseSubmission(Long courseSubmissionId);

  List<Course> getCoursesToBeApproved();

  void approveCourseCreation(Long courseId);
}
