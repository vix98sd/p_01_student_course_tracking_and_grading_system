package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseRepository;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

  private final CourseRepository courseRepository;
  private final ModuleService moduleService;
  private final SemesterService semesterService;
  private final StudentService studentService;
  private final LecturerService lecturerService;
  private final CourseSubmissionService courseSubmissionService;
  private final Clock clock;

  private static final String NOT_FOUND_MSG = "course with id %s was not found";
  private static final String CODE_NAME_EXISTS_MSG = "course with code name %s already exists";

  @Override
  public Course createNewCourse(Course course) {
    if (course.getName() == null ||
        course.getCodeName() == null ||
        course.getLecturesPerWeek() == null ||
        course.getClassExercisesPerWeek() == null ||
        course.getLabExercisesPerWeek() == null ||
        course.getIsOptional() == null ||
        course.getModule() == null ||
        course.getSemester() == null
    ) {
      throw new InvalidArgumentException("all parameters are required");
    }

    if (courseRepository.existsByCodeName(course.getCodeName())) {
      throw new RecordAlreadyExistException(
          String.format(CODE_NAME_EXISTS_MSG, course.getCodeName()));
    }

    course.setModule(moduleService.getModule(course.getModule().getId()));
    course.setSemester(semesterService.getSemester(course.getSemester().getId()));
    return courseRepository.save(course);
  }

  @Override
  public List<Course> getAllCourses() {
    return courseRepository.findAllByIsApproved(true);
  }

  @Override
  public Course getCourse(Long courseId) {
    if (courseId == null) {
      throw new RecordNotFoundException(String.format(NOT_FOUND_MSG, courseId));
    }

    return courseRepository.findById(courseId).orElseThrow(() ->
        new RecordNotFoundException(String.format(NOT_FOUND_MSG, courseId)));
  }

  @Override
  @Transactional
  public Course updateCourseInfo(Long courseId, Course newCourseInfo) {
    Course updatedCourse = this.getCourse(courseId);

    if (newCourseInfo.getName() != null &&
        !newCourseInfo.getName().equals(updatedCourse.getName())
    ) {
      updatedCourse.setName(newCourseInfo.getName());
    }

    if (newCourseInfo.getCodeName() != null &&
        !newCourseInfo.getCodeName().equals(updatedCourse.getCodeName())
    ) {
      if (courseRepository.existsByCodeName(newCourseInfo.getCodeName())) {
        throw new RecordAlreadyExistException(
            String.format(CODE_NAME_EXISTS_MSG, newCourseInfo.getCodeName()));
      }
      updatedCourse.setCodeName(newCourseInfo.getCodeName());
    }

    if (newCourseInfo.getLecturesPerWeek() != null &&
        !newCourseInfo.getLecturesPerWeek().equals(updatedCourse.getLecturesPerWeek())
    ) {
      updatedCourse.setLecturesPerWeek(newCourseInfo.getLecturesPerWeek());
    }

    if (newCourseInfo.getClassExercisesPerWeek() != null &&
        !newCourseInfo.getClassExercisesPerWeek().equals(updatedCourse.getClassExercisesPerWeek())
    ) {
      updatedCourse.setClassExercisesPerWeek(newCourseInfo.getClassExercisesPerWeek());
    }

    if (newCourseInfo.getLabExercisesPerWeek() != null &&
        !newCourseInfo.getLabExercisesPerWeek().equals(updatedCourse.getLabExercisesPerWeek())
    ) {
      updatedCourse.setLabExercisesPerWeek(newCourseInfo.getLabExercisesPerWeek());
    }

    if (newCourseInfo.getIsOptional() != null &&
        !newCourseInfo.getIsOptional().equals(updatedCourse.getIsOptional())
    ) {
      updatedCourse.setIsOptional(newCourseInfo.getIsOptional());
    }

    if (newCourseInfo.getModule() != null &&
        !newCourseInfo.getModule().equals(updatedCourse.getModule())
    ) {
      updatedCourse.setModule(moduleService.getModule(newCourseInfo.getModule().getId()));
    }

    if (newCourseInfo.getSemester() != null &&
        !newCourseInfo.getSemester().equals(updatedCourse.getSemester())
    ) {
      updatedCourse.setSemester(semesterService.getSemester(newCourseInfo.getSemester().getId()));
    }

    return updatedCourse;
  }

  @Override
  public void deleteCourse(Long courseId) {
    courseRepository.delete(this.getCourse(courseId));
  }

  @Override
  public void enrollStudent(Long courseId, Long studentId) {
    Course course = this.getCourse(courseId);
    Student student = studentService.getStudent(studentId);

//    if (course.getSemester().getSubmissionPeriodFrom().isAfter(LocalDate.now(clock)) ||
//        course.getSemester().getSubmissionPeriodTo().isBefore(LocalDate.now(clock))
//    ) {
//      throw new InvalidArgumentException(
//          String.format("course submission can be created only in submission period (%1$s - %2$s)",
//              course.getSemester().getSubmissionPeriodFrom(),
//              course.getSemester().getSubmissionPeriodTo()));
//    }

    if (course.getIsOptional()) {
      CourseSubmission newCourseSubmission = new CourseSubmission();
      newCourseSubmission.setCourse(course);
      newCourseSubmission.setStudent(student);
      courseSubmissionService.addNewCourseSubmission(newCourseSubmission);
    } else {
      course.enrollStudent(student);
      courseRepository.save(course);
    }
  }

  @Override
  public Set<Student> getEnrolledStudents(Long courseId) {
    return this.getCourse(courseId).getEnrolledStudents();
  }

  @Override
  public void assignLecturer(Long courseId, Long lecturerId) {
    Lecturer lecturer = lecturerService.getLecturer(lecturerId);
    Course course = this.getCourse(courseId);

    course.assignLecturer(lecturer);

    courseRepository.save(course);
  }

  @Override
  public Set<Lecturer> getCourseLecturers(Long courseId) {
    return this.getCourse(courseId).getLecturers();
  }

  @Override
  public List<CourseSubmission> getActiveSubmissions(Long courseId) {
    return courseSubmissionService.getCourseSubmissions(this.getCourse(courseId));
  }

  @Override
  public void approveCourseSubmission(Long courseSubmissionId) {

    CourseSubmission courseSubmission = courseSubmissionService
        .resolveCourseSubmission(courseSubmissionId, SubmissionStatus.APPROVED);

    Course course = courseSubmission.getCourse();
    Student student = courseSubmission.getStudent();

    course.enrollStudent(student);
    courseRepository.save(course);
  }

  @Override
  public void rejectCourseSubmission(Long courseSubmissionId) {
    courseSubmissionService.resolveCourseSubmission(courseSubmissionId, SubmissionStatus.REJECTED);
  }

  @Override
  public void revokeCourseSubmission(Long courseSubmissionId) {
    courseSubmissionService.resolveCourseSubmission(courseSubmissionId, SubmissionStatus.REVOKED);
  }

  @Override
  public List<Course> getCoursesToBeApproved() {
    return courseRepository.findAllByIsApproved(false);
  }

  @Override
  @Transactional
  public void approveCourseCreation(Long courseId) {
    Course courseToBeApproved = this.getCourse(courseId);
    courseToBeApproved.setIsApproved(true);
  }
}