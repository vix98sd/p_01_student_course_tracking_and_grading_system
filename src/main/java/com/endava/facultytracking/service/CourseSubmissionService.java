package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import java.util.List;

public interface CourseSubmissionService {

  void addNewCourseSubmission(CourseSubmission newCourseSubmission);

  CourseSubmission getCourseSubmission(Long courseSubmissionId);

  List<CourseSubmission> getCourseSubmissions(Course course);

  CourseSubmission resolveCourseSubmission(Long courseSubmissionId,
      SubmissionStatus submissionStatus);
}