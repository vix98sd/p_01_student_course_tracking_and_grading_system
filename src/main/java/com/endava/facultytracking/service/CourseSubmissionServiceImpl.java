package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.repository.CourseSubmissionRepository;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourseSubmissionServiceImpl implements CourseSubmissionService {

  private static final String COURSE_SUBMISSION_NOT_FOUND = "course submission with id %s was not found";
  private static final Boolean IS_RESOLVED = false;
  private final CourseSubmissionRepository courseSubmissionRepository;
  private final Clock clock;

  @Override
  public void addNewCourseSubmission(CourseSubmission newCourseSubmission) {
    if (newCourseSubmission.getCourse() == null ||
        newCourseSubmission.getCourse().getId() == null ||
        newCourseSubmission.getStudent() == null ||
        newCourseSubmission.getStudent().getId() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    courseSubmissionRepository.save(newCourseSubmission);
  }

  @Override
  public CourseSubmission getCourseSubmission(Long courseSubmissionId) {
    if (courseSubmissionId == null) {
      throw new RecordNotFoundException(
          String.format(COURSE_SUBMISSION_NOT_FOUND, courseSubmissionId));
    }

    return courseSubmissionRepository.findById(courseSubmissionId).orElseThrow(() ->
        new RecordNotFoundException(
            String.format(COURSE_SUBMISSION_NOT_FOUND, courseSubmissionId)));
  }

  @Override
  public List<CourseSubmission> getCourseSubmissions(Course course) {
    return courseSubmissionRepository.findAllByCourseAndIsResolved(course, IS_RESOLVED);
  }

  @Override
  public CourseSubmission resolveCourseSubmission(Long courseSubmissionId,
      SubmissionStatus submissionStatus) {
    CourseSubmission courseSubmissionToBeUpdated =
        this.getCourseSubmission(courseSubmissionId);
    if (courseSubmissionToBeUpdated.getIsResolved()) {
      throw new RecordAlreadyExistException(
          String.format("submission with id %s is already resolved", courseSubmissionId));
    }
    courseSubmissionToBeUpdated.setDateResolved(LocalDate.now(clock));
    courseSubmissionToBeUpdated.setIsResolved(true);
    courseSubmissionToBeUpdated.setStatus(submissionStatus);

    return courseSubmissionRepository.save(courseSubmissionToBeUpdated);
  }
}
