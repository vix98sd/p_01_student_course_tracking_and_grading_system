package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Lecturer;
import java.util.List;
import java.util.Set;

public interface LecturerService {

  Lecturer createNewLecturer(Lecturer lecturer);

  List<Lecturer> getAllLecturers();

  Lecturer getLecturer(Long lecturerId);

  Lecturer getLecturer(String username);

  Lecturer updateLecturerInfo(Long lecturerId, Lecturer newLecturerInfo);

  void deleteLecturer(Long lecturerId);

  Set<Course> getLecturerCourses(Long lecturerId);
}
