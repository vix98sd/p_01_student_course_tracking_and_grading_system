package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.repository.LecturerRepository;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class LecturerServiceImpl implements LecturerService {

  private final LecturerRepository lecturerRepository;
  private final PasswordEncoder passwordEncoder;
  private final Clock clock;

  private static final String NOT_FOUND_MSG = "lecturer with id %s was not found";
  private static final String USERNAME_NOT_FOUND_MSG = "lecturer with username %s was not found";

  @Override
  public Lecturer createNewLecturer(Lecturer lecturer) {
    if (lecturer.getFirstName() == null ||
        lecturer.getLastName() == null ||
        lecturer.getPhoneNumber() == null ||
        lecturer.getDateOfBirth() == null ||
        lecturer.getDegree() == null ||
        lecturer.getVocation() == null ||
        lecturer.getUsername() == null ||
        lecturer.getPassword() == null
    ) {
      throw new InvalidArgumentException("all parameters are required");
    }

    lecturer.setPassword(passwordEncoder.encode(lecturer.getPassword()));
    lecturer.setDateOfEntry(LocalDate.now(clock));
    lecturer.setDateOfPromotion(LocalDate.now(clock));

    lecturer.setAccountNonExpired(true);
    lecturer.setAccountNonLocked(true);
    lecturer.setCredentialsNonExpired(true);
    lecturer.setEnabled(true);

    return lecturerRepository.save(lecturer);
  }

  @Override
  public List<Lecturer> getAllLecturers() {
    return lecturerRepository.findAll();
  }

  @Override
  public Lecturer getLecturer(Long lecturerId) {
    if (lecturerId == null) {
      throw new RecordNotFoundException(String.format(NOT_FOUND_MSG, lecturerId));
    }

    return lecturerRepository.findById(lecturerId).orElseThrow(() ->
        new RecordNotFoundException(String.format(NOT_FOUND_MSG, lecturerId)));
  }

  @Override
  public Lecturer getLecturer(String username) {
    if (username == null) {
      throw new RecordNotFoundException(String.format(USERNAME_NOT_FOUND_MSG, username));
    }

    return lecturerRepository.findByUsername(username).orElseThrow(() ->
        new RecordNotFoundException(String.format(USERNAME_NOT_FOUND_MSG, username)));
  }

  @Override
  @Transactional
  public Lecturer updateLecturerInfo(Long lecturerId, Lecturer newLecturerInfo) {
    Lecturer lecturerToUpdate = this.getLecturer(lecturerId);

    if (newLecturerInfo.getFirstName() != null &&
        !newLecturerInfo.getFirstName().equals(lecturerToUpdate.getFirstName())
    ) {
      lecturerToUpdate.setFirstName(newLecturerInfo.getFirstName());
    }

    if (newLecturerInfo.getLastName() != null &&
        !newLecturerInfo.getLastName().equals(lecturerToUpdate.getLastName())
    ) {
      lecturerToUpdate.setLastName(newLecturerInfo.getLastName());
    }

    if (newLecturerInfo.getPhoneNumber() != null &&
        !newLecturerInfo.getPhoneNumber().equals(lecturerToUpdate.getPhoneNumber())
    ) {
      lecturerToUpdate.setPhoneNumber(newLecturerInfo.getPhoneNumber());
    }

    if (newLecturerInfo.getDateOfBirth() != null &&
        !newLecturerInfo.getDateOfBirth().equals(lecturerToUpdate.getDateOfBirth())
    ) {
      lecturerToUpdate.setDateOfBirth(newLecturerInfo.getDateOfBirth());
    }

    if (newLecturerInfo.getDegree() != null &&
        !newLecturerInfo.getDegree().equals(lecturerToUpdate.getDegree())
    ) {
      lecturerToUpdate.setDegree(newLecturerInfo.getDegree());
    }

    if (newLecturerInfo.getVocation() != null &&
        !newLecturerInfo.getVocation().equals(lecturerToUpdate.getVocation())
    ) {
      lecturerToUpdate.setVocation(newLecturerInfo.getVocation());
      lecturerToUpdate.setDateOfPromotion(LocalDate.now(clock));
    }

    if (newLecturerInfo.getUsername() != null &&
        !newLecturerInfo.getUsername().equals(lecturerToUpdate.getUsername())
    ) {
      lecturerToUpdate.setUsername(newLecturerInfo.getUsername());
    }

    if (newLecturerInfo.getPassword() != null &&
        !newLecturerInfo.getPassword().equals(lecturerToUpdate.getPassword())
    ) {
      lecturerToUpdate.setPassword(newLecturerInfo.getPassword());
    }

    return lecturerToUpdate;
  }

  @Override
  public void deleteLecturer(Long lecturerId) {
    lecturerRepository.delete(this.getLecturer(lecturerId));
  }

  @Override
  public Set<Course> getLecturerCourses(Long lecturerId) {
    return this.getLecturer(lecturerId).getTeachesOn();
  }
}
