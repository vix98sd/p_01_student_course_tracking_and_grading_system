package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Module;
import java.util.List;

public interface ModuleService {

  Module createModule(Module module);

  List<Module> getAllModules();

  Module getModule(Long moduleId);

  Module updateModuleInfo(Long moduleId, Module module);

  String deleteModule(Long moduleId);
}
