package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.repository.ModuleRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ModuleServiceImpl implements ModuleService {

  private final ModuleRepository moduleRepository;
  private static final String NOT_FOUND_MSG = "module with id %s does not exist";
  private static final String CODE_NAME_EXISTS_MSG = "module with code name %s already exists";

  @Override
  public Module createModule(Module module) {
    if (module.getName() == null ||
        module.getCodeName() == null ||
        module.getGrade() == null
    ) {
      throw new InvalidArgumentException("all parameters are required");
    }

    if (moduleRepository.existsByCodeName(module.getCodeName())) {
      throw new RecordAlreadyExistException(
          String.format(CODE_NAME_EXISTS_MSG, module.getCodeName()));
    }

    return moduleRepository.save(module);
  }

  @Override
  public List<Module> getAllModules() {
    return moduleRepository.findAll();
  }

  @Override
  public Module getModule(Long moduleId) {
    if(moduleId == null){
      throw new RecordNotFoundException(String.format(NOT_FOUND_MSG, moduleId));
    }

    return moduleRepository.findById(moduleId).orElseThrow(
        () -> new RecordNotFoundException(String.format(NOT_FOUND_MSG, moduleId)));
  }

  @Override
  @Transactional
  public Module updateModuleInfo(Long moduleId, Module module) {
    Module updatedModule = this.getModule(moduleId);

    if (module.getName() != null &&
        !module.getName().equals(updatedModule.getName())
    ) {
      updatedModule.setName(module.getName());
    }

    if (module.getCodeName() != null &&
        !module.getCodeName().equals(updatedModule.getCodeName())
    ) {
      updatedModule.setCodeName(module.getCodeName());
    }

    if (module.getGrade() != null &&
        !module.getGrade().equals(updatedModule.getGrade())
    ) {
      updatedModule.setGrade(module.getGrade());
    }

    return updatedModule;
  }

  @Override
  public String deleteModule(Long moduleId) {

    moduleRepository.delete(this.getModule(moduleId));
    return "Module deleted";
  }
}
