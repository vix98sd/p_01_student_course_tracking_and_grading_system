package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Role;

public interface RoleService {

  void saveRole(Role role);

  Role getRole(String roleName);
}
