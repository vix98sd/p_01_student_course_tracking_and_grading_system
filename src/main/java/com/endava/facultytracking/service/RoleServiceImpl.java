package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService{

  private static final String ROLE_NOT_FOUND = "role with name %s was not found";
  private final RoleRepository roleRepository;

  @Override
  public void saveRole(Role role) {
    if(role.getName() == null){
      throw new InvalidArgumentException("all arguments are required");
    }

    roleRepository.save(role);
  }

  @Override
  public Role getRole(String roleName) {
    if(roleName == null){
      throw new RecordNotFoundException(String.format(ROLE_NOT_FOUND, roleName));
    }

    return roleRepository.findRoleByName(roleName).orElseThrow(() ->
        new RecordNotFoundException(String.format(ROLE_NOT_FOUND, roleName))
    );
  }
}
