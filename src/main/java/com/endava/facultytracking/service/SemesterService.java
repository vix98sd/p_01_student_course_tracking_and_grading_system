package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Semester;
import java.util.List;

public interface SemesterService {

  Semester createSemester(Semester semester);

  List<Semester> getAllSemesters();

  Semester getSemester(Long semesterId);

  Semester updateSemesterInfo(Long semesterId, Semester semester);

  String deleteSemester(Long semesterId);
}
