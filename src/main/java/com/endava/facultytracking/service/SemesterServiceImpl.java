package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.repository.SemesterRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SemesterServiceImpl implements SemesterService {

  private final SemesterRepository semesterRepository;
  private static final String NOT_FOUND_MSG = "semester with id %s does not exist";

  @Override
  public Semester createSemester(Semester semester) {

    if (semester.getStartingDate() == null) {
      throw new InvalidArgumentException("starting date is required");
    }

    semester.setDatesByStartingDate();
    return semesterRepository.save(semester);
  }

  @Override
  public List<Semester> getAllSemesters() {

    return semesterRepository.findAll();
  }

  @Override
  public Semester getSemester(Long semesterId) {
    return semesterRepository.findById(semesterId).orElseThrow(
        () -> new RecordNotFoundException(
            String.format(NOT_FOUND_MSG, semesterId)));
  }

  @Override
  @Transactional
  public Semester updateSemesterInfo(Long semesterId, Semester semester) {
    Semester updatedSemester = semesterRepository.findById(semesterId).orElseThrow(
        () -> new InvalidArgumentException(String.format(NOT_FOUND_MSG, semesterId)));

    if (semester.getStartingDate() != null &&
        !semester.getStartingDate().equals(updatedSemester.getStartingDate())
    ) {
      updatedSemester.setStartingDate(semester.getStartingDate());
    }

    if (semester.getEndingDate() != null &&
        !semester.getEndingDate().equals(updatedSemester.getEndingDate())
    ) {
      updatedSemester.setEndingDate(semester.getEndingDate());
    }

    if (semester.getSubmissionPeriodFrom() != null &&
        !semester.getSubmissionPeriodFrom().equals(updatedSemester.getSubmissionPeriodFrom())
    ) {
      updatedSemester.setSubmissionPeriodFrom(semester.getSubmissionPeriodFrom());
    }

    if (semester.getSubmissionPeriodTo() != null &&
        !semester.getSubmissionPeriodTo().equals(updatedSemester.getSubmissionPeriodTo())
    ) {
      updatedSemester.setSubmissionPeriodTo(semester.getSubmissionPeriodTo());
    }

    if (semester.getFinalExamPeriodFrom() != null &&
        !semester.getFinalExamPeriodFrom().equals(updatedSemester.getFinalExamPeriodFrom())
    ) {
      updatedSemester.setFinalExamPeriodFrom(semester.getFinalExamPeriodFrom());
    }

    if (semester.getFinalExamPeriodTo() != null &&
        !semester.getFinalExamPeriodTo().equals(updatedSemester.getFinalExamPeriodTo())
    ) {
      updatedSemester.setFinalExamPeriodTo(semester.getFinalExamPeriodTo());
    }

    return updatedSemester;
  }

  @Override
  public String deleteSemester(Long semesterId) {
    if (!semesterRepository.existsById(semesterId)) {
      throw new RecordNotFoundException(String.format(NOT_FOUND_MSG, semesterId));
    }

    semesterRepository.deleteById(semesterId);
    return "Semester deleted";
  }
}
