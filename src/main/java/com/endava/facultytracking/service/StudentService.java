package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Student;
import java.util.List;
import java.util.Set;

public interface StudentService {

  Student createNewStudent(Student student);

  List<Student> getAllStudents();

  Student getStudent(Long studentId);

  Student getStudent(String username);

  Student updateStudent(Long studentId, Student student);

  void deleteStudent(Long studentId);

  Set<Course> getEnrolledCourses(Long studentId);

  void assignModule(Long studentId, Long moduleId);

  Set<Module> getAssignedModules(Long studentId);
}
