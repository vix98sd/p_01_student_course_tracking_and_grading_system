package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.StudentRepository;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

  private final StudentRepository studentRepository;
  private final ModuleService moduleService;
  private final PasswordEncoder passwordEncoder;
  private final Clock clock;

  private static final String STUDENT_NOT_FOUND = "student with id %s was not found";
  private static final String STUDENT_USERNAME_NOT_FOUND = "student with username %s was not found";
  private static final String USERNAME_EXISTS_MSG = "username %s is already taken";

  @Override
  public Student createNewStudent(Student student) {
    if (student.getFirstName() == null ||
        student.getLastName() == null ||
        student.getIndexNumber() == null ||
        student.getDateOfBirth() == null ||
        student.getUsername() == null ||
        student.getPassword() == null
    ) {
      throw new InvalidArgumentException("all parameters are required");
    }

    if (studentRepository.existsByUsername(student.getUsername())) {
      throw new RecordAlreadyExistException(
          String.format(USERNAME_EXISTS_MSG, student.getUsername()));
    }

    student.setPassword(passwordEncoder.encode(student.getPassword()));
    student.setDateOfEntry(LocalDate.now(clock));
    student.setGrade(1);
    student.setAccountNonExpired(true);
    student.setAccountNonLocked(true);
    student.setCredentialsNonExpired(true);
    student.setEnabled(true);

    return studentRepository.save(student);
  }

  @Override
  public List<Student> getAllStudents() {
    return studentRepository.findAll();
  }

  @Override
  public Student getStudent(Long studentId) {
    if (studentId == null) {
      throw new RecordNotFoundException(String.format(STUDENT_NOT_FOUND, studentId));
    }

    return studentRepository.findById(studentId).orElseThrow(() ->
        new RecordNotFoundException(String.format(STUDENT_NOT_FOUND, studentId)));
  }

  @Override
  public Student getStudent(String username) {
    if (username == null) {
      throw new RecordNotFoundException(String.format(STUDENT_USERNAME_NOT_FOUND, username));
    }

    return studentRepository.findByUsername(username).orElseThrow(() ->
        new RecordNotFoundException(String.format(STUDENT_USERNAME_NOT_FOUND, username)));
  }

  @Override
  @Transactional
  public Student updateStudent(Long studentId, Student newStudentInfo) {
    Student updatedStudentInfo = this.getStudent(studentId);

    if (newStudentInfo.getIndexNumber() != null &&
        !newStudentInfo.getIndexNumber().equals(updatedStudentInfo.getIndexNumber())
    ) {
      updatedStudentInfo.setIndexNumber(newStudentInfo.getIndexNumber());
    }

    if (newStudentInfo.getDateOfBirth() != null &&
        !newStudentInfo.getDateOfBirth().equals(updatedStudentInfo.getDateOfBirth())
    ) {
      updatedStudentInfo.setDateOfBirth(newStudentInfo.getDateOfBirth());
    }

    if (newStudentInfo.getDateOfEntry() != null &&
        !newStudentInfo.getDateOfEntry().equals(updatedStudentInfo.getDateOfEntry())
    ) {
      updatedStudentInfo.setDateOfEntry(newStudentInfo.getDateOfEntry());
    }

    if (newStudentInfo.getGrade() != null &&
        !newStudentInfo.getGrade().equals(updatedStudentInfo.getGrade())
    ) {
      updatedStudentInfo.setGrade(newStudentInfo.getGrade());
    }

    if (newStudentInfo.getUsername() != null &&
        !newStudentInfo.getUsername().equals(updatedStudentInfo.getUsername())
    ) {
      updatedStudentInfo.setUsername(newStudentInfo.getUsername());
    }

    if (newStudentInfo.getPassword() != null &&
        !newStudentInfo.getPassword().equals(updatedStudentInfo.getPassword())
    ) {
      updatedStudentInfo.setPassword(newStudentInfo.getPassword());
    }

    return updatedStudentInfo;
  }

  @Override
  public void deleteStudent(Long studentId) {
      studentRepository.delete(this.getStudent(studentId));
  }

  @Override
  public Set<Course> getEnrolledCourses(Long studentId) {
    return this.getStudent(studentId).getStudentsCourses();
  }

  @Override
  public void assignModule(Long studentId, Long moduleId) {
    Student student = this.getStudent(studentId);
    Module module = moduleService.getModule(moduleId);
    student.assignNewModule(module);
    studentRepository.save(student);
  }

  @Override
  public Set<Module> getAssignedModules(Long studentId) {
    return this.getStudent(studentId).getStudentsModules();
  }
}
