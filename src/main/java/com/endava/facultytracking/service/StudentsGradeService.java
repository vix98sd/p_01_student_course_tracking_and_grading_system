package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.model.StudentsGrade;
import java.util.List;
import java.util.Map;

public interface StudentsGradeService {

  StudentsGrade addNewStudentsGrade(StudentsGrade newStudentsGrade);

  List<Map<String, Number>> getAllStudentsGrades(Long studentId);

  List<Map<String, Number>> getAllGradesByCourse(Long courseId);

  StudentsGrade getStudentsGradeByCourse(Long studentId, Long courseId);

//  StudentsGrade updateStudentsGradeInfo(StudentsGrade newStudentsGradeInfo);

  void annulGrade(StudentsGrade studentsGradeToAnnul);
}
