package com.endava.facultytracking.service;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.StudentsGrade;
import com.endava.facultytracking.repository.StudentsGradeRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentsGradeServiceImpl implements StudentsGradeService {

  private final StudentsGradeRepository studentsGradeRepository;
  private final StudentService studentService;
  private final CourseService courseService;

  private static final Boolean IS_ANNULLED = false;
  private static final String STUDENTS_GRADE_NOT_FOUND = "student with id %1$s does not have any grades on course with id %2$s";

  @Override
  public StudentsGrade addNewStudentsGrade(StudentsGrade newStudentsGrade) {
    if (newStudentsGrade.getStudent().getId() == null ||
        newStudentsGrade.getCourse().getId() == null ||
        newStudentsGrade.getPoints() == null ||
        newStudentsGrade.getGrade() == null
    ) {
      throw new InvalidArgumentException("all arguments are required");
    }

    try {
      this.annulGrade(newStudentsGrade);
      newStudentsGrade = new StudentsGrade(
          studentService.getStudent(newStudentsGrade.getStudent().getId()),
          courseService.getCourse(newStudentsGrade.getCourse().getId()),
          newStudentsGrade.getPoints(),
          newStudentsGrade.getGrade()
      );
    } catch (RecordNotFoundException e) {
      newStudentsGrade = new StudentsGrade(
          studentService.getStudent(newStudentsGrade.getStudent().getId()),
          courseService.getCourse(newStudentsGrade.getCourse().getId()),
          newStudentsGrade.getPoints(),
          newStudentsGrade.getGrade()
      );
    }

    return studentsGradeRepository.save(newStudentsGrade);

//    return this.convertToMap(newStudentsGrade);
  }

  @Override
  public List<Map<String, Number>> getAllStudentsGrades(Long studentId) {
    List<StudentsGrade> studentsGrades = studentsGradeRepository.findAllByStudentIdAndIsAnnulled(
        studentId, IS_ANNULLED);
    if (studentsGrades.isEmpty()) {
      throw new RecordNotFoundException(
          String.format("student with id %s does not have any grades", studentId));
    }

    List<Map<String, Number>> studentGrades = new ArrayList<>();
    studentsGrades.stream().map(this::convertToMap).forEach(studentGrades::add);

    return studentGrades;
  }

  @Override
  public List<Map<String, Number>> getAllGradesByCourse(Long courseId) {
    List<StudentsGrade> studentsGrades = studentsGradeRepository.findAllByCourseIdAndIsAnnulled(
        courseId, IS_ANNULLED);
    if (studentsGrades.isEmpty()) {
      throw new RecordNotFoundException(
          String.format("course with id %s does not have any grades", courseId));
    }

    List<Map<String, Number>> studentGrades = new ArrayList<>();
    studentsGrades.stream().map(this::convertToMap).forEach(studentGrades::add);

    return studentGrades;
  }

  @Override
  public StudentsGrade getStudentsGradeByCourse(Long studentId, Long courseId) {

    if (studentId == null || courseId == null) {
      throw new RecordNotFoundException(
          String.format(STUDENTS_GRADE_NOT_FOUND, studentId, courseId));
    }

    return studentsGradeRepository.findAllByStudentIdAndCourseIdAndIsAnnulled(studentId, courseId,
        IS_ANNULLED).orElseThrow(() ->
        new RecordNotFoundException(
            String.format(STUDENTS_GRADE_NOT_FOUND, studentId, courseId)));
  }

  @Override
  public void annulGrade(StudentsGrade studentsGradeToAnnul) {
    studentsGradeToAnnul = this.getStudentsGradeByCourse(
        studentsGradeToAnnul.getStudent().getId(),
        studentsGradeToAnnul.getCourse().getId()
    );
    studentsGradeToAnnul.setIsAnnulled(true);

    studentsGradeRepository.save(studentsGradeToAnnul);
  }

  private Map<String, Number> convertToMap(StudentsGrade studentsGrade) {
    HashMap<String, Number> studentsGradeMap = new HashMap<>();

    studentsGradeMap.put("studentsGradeId", studentsGrade.getId());
    studentsGradeMap.put("studentId", studentsGrade.getStudent().getId());
    studentsGradeMap.put("courseId", studentsGrade.getCourse().getId());
    studentsGradeMap.put("points", studentsGrade.getPoints());
    studentsGradeMap.put("grade", studentsGrade.getGrade());

    return studentsGradeMap;
  }
}
