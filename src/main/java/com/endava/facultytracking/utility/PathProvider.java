package com.endava.facultytracking.utility;

public class PathProvider {
  public static final String API_V1 = "/v1/";
  public static final String LOGIN_ENDPOINT = API_V1 + "login";
  public static final String SWAGGER_UI_ENDPOINT = API_V1 + "swagger/";
  public static final String SWAGGER_ENDPOINT = SWAGGER_UI_ENDPOINT + "api-docs";
  public static final String ADMINISTRATOR_ENDPOINT = API_V1 + "administrators/";
  public static final String COURSE_ENDPOINT = API_V1 + "courses/";
  public static final String COURSE_ASSIGNMENT_ENDPOINT = API_V1 + "assignments/";
  public static final String COURSE_EXAM_ENDPOINT = API_V1 + "exams/";
  public static final String COURSE_EXAM_SUBMISSION_ENDPOINT = COURSE_EXAM_ENDPOINT + "submissions/";
  public static final String COURSE_SCHEDULE_ENDPOINT = API_V1 + "schedules/";
  public static final String LECTURER_ENDPOINT = API_V1 + "lecturers/";
  public static final String MODULE_ENDPOINT = API_V1 + "modules/";
  public static final String SEMESTER_ENDPOINT = API_V1 + "semesters/";
  public static final String STUDENT_ENDPOINT = API_V1 + "students/";
  public static final String STUDENTS_GRADE_ENDPOINT = API_V1 + "grades/";
}
