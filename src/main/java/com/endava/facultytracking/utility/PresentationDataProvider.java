package com.endava.facultytracking.utility;

import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.domain.model.StudentsGrade;
import com.endava.facultytracking.service.AppUserDetailsService;
import com.endava.facultytracking.service.CourseAssignmentResultService;
import com.endava.facultytracking.service.CourseAssignmentService;
import com.endava.facultytracking.service.CourseExamResultService;
import com.endava.facultytracking.service.CourseExamService;
import com.endava.facultytracking.service.CourseExamSubmissionService;
import com.endava.facultytracking.service.CourseScheduleService;
import com.endava.facultytracking.service.CourseService;
import com.endava.facultytracking.service.CourseSubmissionService;
import com.endava.facultytracking.service.ModuleService;
import com.endava.facultytracking.service.SemesterService;
import com.endava.facultytracking.service.StudentService;
import com.endava.facultytracking.service.StudentsGradeService;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PresentationDataProvider {

  private final AppUserDetailsService appUserDetailsService;
  private final SemesterService semesterService;
  private final ModuleService moduleService;
  private final CourseService courseService;
  private final CourseAssignmentService courseAssignmentService;
  private final CourseExamService courseExamService;
  private final CourseScheduleService courseScheduleService;
  private final StudentService studentService;

  public void LoadPresentationData() {
    loadRoles();
    loadAdmins();
    loadLecturers();
    loadStudents();
    assignRoles();

    loadSemester();
    loadModules();

    loadCourses();
    assignModulesToStudents();
    enrollStudentsOnCourses();
    assignLecturersToCourse();

    loadCourseSchedules();
    loadCourseAssignment();
    loadCourseAssignmentResults();
    loadCourseExam();
    loadCourseExamResults();
    loadCourseExamSubmissions();
    loadAttendees();
  }

  private void loadRoles() {
    appUserDetailsService.createNewRole(new Role(null, "ROLE_ADMIN"));
    appUserDetailsService.createNewRole(new Role(null, "ROLE_LECTURER"));
    appUserDetailsService.createNewRole(new Role(null, "ROLE_STUDENT"));
  }

  private void loadAdmins() {
    appUserDetailsService.createNewUser(
        new Administrator(
            null,
            "Admin1",
            "AdminLN1",
            "admin1",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        )
    );
    appUserDetailsService.createNewUser(
        new Administrator(
            null,
            "Admin2",
            "AdminLN2",
            "admin2",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        )
    );
    appUserDetailsService.createNewUser(
        new Administrator(
            null,
            "Admin3",
            "AdminLN3",
            "admin3",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        )
    );
  }

  private void loadLecturers() {
    appUserDetailsService.createNewUser(
        new Lecturer(
            null,
            "Lecturer1",
            "LecturerLN1",
            "+381691234567",
            LocalDate.of(1998, Month.JUNE, 05),
            LocalDate.of(2021, Month.AUGUST, 24),
            "Dr",
            "Professor",
            LocalDate.of(2021, Month.AUGUST, 24),
            "lecturer1",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        ));
    appUserDetailsService.createNewUser(
        new Lecturer(
            null,
            "Lecturer2",
            "LecturerLN2",
            "+381691234567",
            LocalDate.of(1998, Month.JUNE, 05),
            LocalDate.of(2021, Month.AUGUST, 24),
            "Dr",
            "Professor",
            LocalDate.of(2021, Month.AUGUST, 24),
            "lecturer2",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        ));
    appUserDetailsService.createNewUser(
        new Lecturer(
            null,
            "Lecturer3",
            "LecturerLN3",
            "+381691234567",
            LocalDate.of(1998, Month.JUNE, 05),
            LocalDate.of(2021, Month.AUGUST, 24),
            "Dr",
            "Professor",
            LocalDate.of(2021, Month.AUGUST, 24),
            "lecturer3",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        ));
  }

  private void loadStudents() {
    appUserDetailsService.createNewUser(
        new Student(
            null,
            "Stefan",
            "Marjanovic",
            "4/2017",
            LocalDate.of(1998, Month.JUNE, 5),
            LocalDate.of(2017, Month.JULY, 10),
            1,
            "makisha",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>(),
            new HashSet<>()
        )
    );
    appUserDetailsService.createNewUser(
        new Student(
            null,
            "Spasoje",
            "Vostic",
            "5/2017",
            LocalDate.of(1998, Month.JUNE, 5),
            LocalDate.of(2017, Month.JULY, 10),
            1,
            "spajk",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>(),
            new HashSet<>()
        )
    );
    appUserDetailsService.createNewUser(
        new Student(
            null,
            "Lazar",
            "Starcevic",
            "8/2017",
            LocalDate.of(1998, Month.JUNE, 5),
            LocalDate.of(2017, Month.JULY, 10),
            2,
            "laki",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>(),
            new HashSet<>()
        ));
    appUserDetailsService.createNewUser(
        new Student(
            null,
            "Vanja",
            "Stepanoski",
            "9/2017",
            LocalDate.of(1998, Month.JUNE, 5),
            LocalDate.of(2017, Month.JULY, 10),
            3,
            "vanjasd",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>(),
            new HashSet<>()
        ));
  }

  private void assignRoles() {
    appUserDetailsService.addRoleToUser("admin1", "ROLE_ADMIN");
    appUserDetailsService.addRoleToUser("admin2", "ROLE_ADMIN");
    appUserDetailsService.addRoleToUser("admin3", "ROLE_ADMIN");
    appUserDetailsService.addRoleToUser("lecturer1", "ROLE_LECTURER");
    appUserDetailsService.addRoleToUser("lecturer2", "ROLE_LECTURER");
    appUserDetailsService.addRoleToUser("lecturer3", "ROLE_LECTURER");
    appUserDetailsService.addRoleToUser("makisha", "ROLE_STUDENT");
    appUserDetailsService.addRoleToUser("spajk", "ROLE_STUDENT");
    appUserDetailsService.addRoleToUser("laki", "ROLE_STUDENT");
    appUserDetailsService.addRoleToUser("vanjasd", "ROLE_STUDENT");
  }

  private void loadSemester() {
    semesterService.createSemester(new Semester(LocalDate.of(2021, Month.MARCH, 1)));
  }

  private void loadModules() {
    moduleService.createModule(
        new Module(
            null,
            "Information Technologies Year 1",
            "IT-1",
            1
        ));
    moduleService.createModule(
        new Module(
            null,
            "Information Technologies Year 2",
            "IT-2",
            2
        ));
    moduleService.createModule(
        new Module(
            null,
            "Information Technologies Year 3",
            "IT-3",
            3
        ));
    moduleService.createModule(
        new Module(
            null,
            "Information Technologies Year 4",
            "IT-4",
            4
        ));
    moduleService.createModule(
        new Module(
            null,
            "Information Technologies Master",
            "IT-M",
            5
        ));
  }

  private void loadCourses() {
    courseService.createNewCourse(
        new Course(
            null,
            "Programming Introduction",
            "PI",
            3,
            0,
            3,
            false,
            true,
            new Module(1L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Databases Introduction",
            "DBI",
            2,
            0,
            2,
            false,
            true,
            new Module(1L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Computer Networks and Communications",
            "CNC",
            3,
            0,
            2,
            false,
            true,
            new Module(1L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Frontend Technologies",
            "FRT",
            2,
            0,
            2,
            true,
            true,
            new Module(1L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Backend Technologies",
            "BET",
            2,
            0,
            2,
            true,
            true,
            new Module(1L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Programming Languages",
            "PL",
            3,
            0,
            3,
            false,
            true,
            new Module(2L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Object Oriented Programming",
            "OOP",
            3,
            0,
            3,
            false,
            true,
            new Module(3L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
    courseService.createNewCourse(
        new Course(
            null,
            "Advanced OOP",
            "AOOP",
            3,
            0,
            3,
            false,
            true,
            new Module(4L, null, null, null),
            new Semester(1L, null, null, null, null, null, null)
        ));
  }

  private void assignModulesToStudents() {
    studentService.assignModule(1L, 1L);
    studentService.assignModule(2L, 1L);
    studentService.assignModule(3L, 2L);
    studentService.assignModule(4L, 3L);
  }

  private void enrollStudentsOnCourses() {
    courseService.enrollStudent(1L, 1L);
    courseService.enrollStudent(2L, 1L);
    courseService.enrollStudent(3L, 1L);
    courseService.enrollStudent(4L, 1L);
    courseService.enrollStudent(1L, 2L);
    courseService.enrollStudent(2L, 2L);
    courseService.enrollStudent(3L, 2L);
    courseService.enrollStudent(5L, 2L);
    courseService.enrollStudent(6L, 3L);
    courseService.enrollStudent(7L, 4L);
  }

  private void assignLecturersToCourse() {
    courseService.assignLecturer(1L, 1L);
    courseService.assignLecturer(1L, 3L);
    courseService.assignLecturer(2L, 2L);
    courseService.assignLecturer(3L, 3L);
    courseService.assignLecturer(4L, 1L);
    courseService.assignLecturer(5L, 2L);
    courseService.assignLecturer(6L, 1L);
    courseService.assignLecturer(7L, 3L);
  }

  private void loadCourseSchedules() {
    courseScheduleService.addNewCourseScheduleSeries(
        1L,
        new CourseSchedule(
            null,
            LocalDate.of(2021, Month.MARCH, 1),
            LocalTime.of(7, 0),
            2,
            "Lectures",
            false,
            null
        ));
    courseScheduleService.addNewCourseScheduleSeries(
        2L,
        new CourseSchedule(
            null,
            LocalDate.of(2021, Month.MARCH, 1),
            LocalTime.of(9, 30),
            2,
            "Lectures",
            false,
            null
        ));
    courseScheduleService.addNewCourseScheduleSeries(
        3L,
        new CourseSchedule(
            null,
            LocalDate.of(2021, Month.MARCH, 1),
            LocalTime.of(12, 0),
            2,
            "Lectures",
            false,
            null
        ));
    courseScheduleService.addNewCourseScheduleSeries(
        4L,
        new CourseSchedule(
            null,
            LocalDate.of(2021, Month.MARCH, 1),
            LocalTime.of(14, 30),
            2,
            "Lectures",
            false,
            null
        ));
    courseScheduleService.addNewCourseScheduleSeries(
        5L,
        new CourseSchedule(
            null,
            LocalDate.of(2021, Month.MARCH, 1),
            LocalTime.of(17, 0),
            2,
            "Lectures",
            false,
            null
        ));
  }

  private void loadCourseAssignment() {
    courseAssignmentService.addNewCourseAssignment(
        new CourseAssignment(
            null,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(8, 0),
            LocalTime.of(19, 30),
            "You need to create simple function for printing students",
            new Course(1L, null, null, null, null, null, null, null, null, null)
        ));
    courseAssignmentService.addNewCourseAssignment(
        new CourseAssignment(
            null,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(8, 0),
            LocalTime.of(19, 30),
            "You need to create database for student tracking system",
            new Course(2L, null, null, null, null, null, null, null, null, null)
        ));
  }

  private void loadCourseAssignmentResults() {
    courseAssignmentService.addNewResult(
        1L,
        new CourseAssignmentResult(
            null,
            85L,
            new Student(1L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            null
        )
    );
    courseAssignmentService.addNewResult(
        1L,
        new CourseAssignmentResult(
            null,
            88L,
            new Student(2L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            null
        )
    );
    courseAssignmentService.addNewResult(
        2L,
        new CourseAssignmentResult(
            null,
            98L,
            new Student(1L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            null
        )
    );
    courseAssignmentService.addNewResult(
        2L,
        new CourseAssignmentResult(
            null,
            91L,
            new Student(2L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            null
        )
    );
  }

  private void loadCourseExam() {
    courseExamService.addNewCourseExam(
        new CourseExam(
            null,
            LocalDate.of(2021, Month.APRIL, 20),
            LocalTime.of(8, 0),
            false,
            "First test",
            new Course(1L, null, null, null, null, null, null, null, null, null),
            null
        )
    );
    courseExamService.addNewCourseExam(
        new CourseExam(
            null,
            LocalDate.of(2021, Month.MARCH, 5),
            LocalTime.of(8, 0),
            false,
            "First test",
            new Course(2L, null, null, null, null, null, null, null, null, null),
            null
        )
    );
    courseExamService.addNewCourseExam(
        new CourseExam(
            null,
            LocalDate.of(2021, Month.MAY, 4),
            LocalTime.of(8, 0),
            false,
            "Second test",
            new Course(2L, null, null, null, null, null, null, null, null, null),
            null
        )
    );
    courseExamService.addNewCourseExam(
        new CourseExam(
            null,
            LocalDate.of(2021, Month.JUNE, 21),
            LocalTime.of(8, 0),
            true,
            "Final exam",
            new Course(2L, null, null, null, null, null, null, null, null, null),
            null
        )
    );
  }

  private void loadCourseExamResults() {
    courseExamService.addExamResult(
        1L,
        new CourseExamResult(
            null,
            95L,
            new Student(1L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            false,
            false,
            CourseExamResultStatus.WAITING_FOR_APPROVAL,
            null
        )
    );
    courseExamService.addExamResult(
        1L,
        new CourseExamResult(
            null,
            91L,
            new Student(2L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            false,
            false,
            CourseExamResultStatus.WAITING_FOR_APPROVAL,
            null
        )
    );
  }

  private void loadCourseExamSubmissions() {
    courseExamService.createNewFinalExamSubmission(
        new CourseExamSubmission(
            null,
            LocalDate.of(2021, Month.APRIL, 20),
            LocalDate.of(2021, Month.APRIL, 20),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            new Student(1L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            new CourseExam(4L, null, null, null, null, null, null)
        )
    );
    courseExamService.createNewFinalExamSubmission(
        new CourseExamSubmission(
            null,
            LocalDate.of(2021, Month.APRIL, 20),
            LocalDate.of(2021, Month.APRIL, 20),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            new Student(2L, null, null, null, null, null, null, null, null, true, true, true, true,
                null, null),
            new CourseExam(4L, null, null, null, null, null, null)
        )
    );
  }

  private void loadAttendees(){
    courseScheduleService.addAttendee(1L, 1L);
    courseScheduleService.addAttendee(2L, 1L);
    courseScheduleService.addAttendee(3L, 1L);
    courseScheduleService.addAttendee(4L, 1L);
    courseScheduleService.addAttendee(1L, 2L);
    courseScheduleService.addAttendee(2L, 2L);
    courseScheduleService.addAttendee(3L, 2L);
    courseScheduleService.addAttendee(4L, 2L);
    courseScheduleService.addAttendee(5L, 2L);
    courseScheduleService.addAttendee(6L, 2L);
  }
}
