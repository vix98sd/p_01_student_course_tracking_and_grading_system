package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.service.AdministratorService;
import com.endava.facultytracking.utility.mockObjects;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AdministratorControllerTest {

  @Mock
  private AdministratorService administratorService;
  @InjectMocks
  private AdministratorController administratorController;

  private final Administrator mockAdministrator = mockObjects.getAdministrator();

  @Test
  void createNewAdministrator() {
    // Given
    Administrator expectedAdministrator = mockAdministrator;
    Mockito.when(administratorService.createNewAdministrator(Mockito.any(Administrator.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    Administrator actualAdministrator = administratorController.createNewAdministrator(mockAdministrator);
    // Then
    assertEquals(expectedAdministrator, actualAdministrator);
  }

  @Test
  void getAdministrator() {
    // Given
    Administrator expectedAdministrator = mockAdministrator;
    Long administratorId = expectedAdministrator.getId();
    Mockito.when(administratorService.getAdministrator(Mockito.anyLong()))
        .thenReturn(mockAdministrator);
    // When
    Administrator actualAdministrator = administratorController.getAdministrator(administratorId);
    // Then
    assertEquals(expectedAdministrator, actualAdministrator);
  }

  @Test
  void getAllAdministrators() {
    // Given
    List<Administrator> expectedAdministrators =
        Arrays.asList(mockAdministrator, mockAdministrator, mockAdministrator);
    Mockito.when(administratorService.getAllAdministrators()).thenReturn(expectedAdministrators);
    // When
    List<Administrator> actualAdministrators = administratorController.getAllAdministrators();
    // Then
    assertEquals(expectedAdministrators, actualAdministrators);
  }

  @Test
  void updateAdministratorInfo() {
    // Given
    Mockito.doNothing().when(administratorService)
        .updateAdministratorInfo(Mockito.any(Administrator.class));
    // When
    administratorController.updateAdministratorInfo(mockAdministrator);
    // Then
    Mockito.verify(administratorService, Mockito.times(1))
        .updateAdministratorInfo(Mockito.any(Administrator.class));
  }

  @Test
  void deleteAdministrator() {
    // Given
    Long administratorId = mockAdministrator.getId();
    Mockito.doNothing().when(administratorService).deleteAdministrator(Mockito.anyLong());
    // When
    administratorController.deleteAdministrator(administratorId);
    // Then
    Mockito.verify(administratorService, Mockito.times(1))
        .deleteAdministrator(Mockito.anyLong());
  }
}