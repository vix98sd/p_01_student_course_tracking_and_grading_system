package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.service.CourseAssignmentService;
import com.endava.facultytracking.utility.mockObjects;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseAssignmentControllerTest {

  @Mock
  private CourseAssignmentService courseAssignmentService;
  @InjectMocks
  private CourseAssignmentController courseAssignmentController;

  private final CourseAssignment mockCourseAssignment = mockObjects.getCourseAssignment();

  @Test
  void addNewCourseAssignment() {
    // Given
    CourseAssignment expectedCourseAssignment = mockObjects.getCourseAssignment();
    Mockito.when(
            courseAssignmentService.addNewCourseAssignment(Mockito.any(CourseAssignment.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseAssignment actualCourseAssignment = courseAssignmentController.addNewCourseAssignment(
        mockCourseAssignment);
    // Then
    assertEquals(expectedCourseAssignment, actualCourseAssignment);
  }

  @Test
  void getCourseAssignment() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    CourseAssignment expectedCourseAssignment = mockObjects.getCourseAssignment();
    Mockito.when(
            courseAssignmentService.getCourseAssignment(Mockito.anyLong()))
        .thenReturn(mockCourseAssignment);
    // When
    CourseAssignment actualCourseAssignment = courseAssignmentController.getCourseAssignment(
        courseAssignmentId);
    // Then
    assertEquals(expectedCourseAssignment, actualCourseAssignment);
  }

  @Test
  void getCourseAssignments() {
    // Given
    Long courseId = mockCourseAssignment.getCourse().getId();
    CourseAssignment expectedCourseAssignment = mockObjects.getCourseAssignment();
    List<CourseAssignment> expectedCourseAssignments =
        Arrays.asList(expectedCourseAssignment, expectedCourseAssignment, expectedCourseAssignment);
    Mockito.when(courseAssignmentService.getCourseAssignments(Mockito.anyLong()))
        .thenReturn(Arrays.asList(mockCourseAssignment,mockCourseAssignment,mockCourseAssignment));
    // When
    List<CourseAssignment> actualCourseAssignments = courseAssignmentController
        .getCourseAssignments(courseId);
    // Then
    assertEquals(expectedCourseAssignments, actualCourseAssignments);
  }

  @Test
  void updateCourseAssignmentInfo() {
    // Given
    CourseAssignment expectedCourseAssignment = mockObjects.getCourseAssignment();
    Mockito.when(
            courseAssignmentService.updateCourseAssignmentInfo(Mockito.any(CourseAssignment.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseAssignment actualCourseAssignment = courseAssignmentController.updateCourseAssignmentInfo(
        mockCourseAssignment);
    // Then
    assertEquals(expectedCourseAssignment, actualCourseAssignment);
  }

  @Test
  void deleteCourseAssignment() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    Mockito.doNothing().when(courseAssignmentService).deleteCourseAssignment(Mockito.anyLong());
    // When
    courseAssignmentController.deleteCourseAssignment(courseAssignmentId);
    // Then
    Mockito.verify(courseAssignmentService, Mockito.times(1))
        .deleteCourseAssignment(Mockito.anyLong());
  }

  @Test
  void addNewResult() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    CourseAssignmentResult courseAssignmentResult = mockObjects.getCourseAssignmentResult();
    Mockito.doNothing().when(courseAssignmentService).addNewResult(Mockito.anyLong(),
        Mockito.any(CourseAssignmentResult.class));
    // When
    courseAssignmentController.addNewResult(courseAssignmentId, courseAssignmentResult);
    // Then
    Mockito.verify(courseAssignmentService, Mockito.times(1))
        .addNewResult(Mockito.anyLong(), Mockito.any(CourseAssignmentResult.class));
  }

  @Test
  void getCourseAssignmentResults() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    List<CourseAssignmentResult> expectedCourseExamResults =
        new ArrayList<>(mockObjects.getCourseAssignmentWithResults().getResults());
    Mockito.when(courseAssignmentService.getCourseAssignmentResults(Mockito.anyLong()))
        .thenReturn(expectedCourseExamResults);
    // When
    List<CourseAssignmentResult> actualCourseExamResults =
        courseAssignmentController.getCourseAssignmentResults(courseAssignmentId);
    // Then
    assertEquals(expectedCourseExamResults, actualCourseExamResults);
  }

  @Test
  void deleteCourseAssignmentResult() {
    // Given
    Long courseAssignmentResultId = 1L;
    Mockito.doNothing().when(courseAssignmentService).deleteCourseAssignmentResult(Mockito.anyLong());
    // When
    courseAssignmentController.deleteCourseAssignmentResult(courseAssignmentResultId);
    // Then
    Mockito.verify(courseAssignmentService).deleteCourseAssignmentResult(Mockito.anyLong());
  }
}