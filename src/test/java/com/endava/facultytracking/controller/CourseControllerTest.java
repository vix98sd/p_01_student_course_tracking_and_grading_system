package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.dto.CourseDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.dto.mapper.DtoMapperImpl;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.CourseService;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

@ExtendWith(MockitoExtension.class)
class CourseControllerTest {

  @Mock
  private CourseService courseService;
  @Mock
  private DtoMapper dtoMapper;
  @InjectMocks
  private CourseController courseController;

  private final Student mockStudent = mockObjects.getStudent();
  private final Lecturer mockLecturer = mockObjects.getLecturer();
  private final Course mockCourse = mockObjects.getCourse();
  private final CourseSubmission mockCourseSubmission = mockObjects.getCourseSubmission();
  private final DtoMapper mapper = new DtoMapperImpl();
  private final CourseDto mockCourseDto = mapper.modelToDto(mockCourse);

  @Test
  void createNewCourse() {
    // Given
    CourseDto expectedCourse = mockCourseDto;
    Mockito.when(courseService.createNewCourse(Mockito.any(Course.class)))
        .thenAnswer(i -> i.getArgument(0));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Course.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Course.class)));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(CourseDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, CourseDto.class)));
    // When
    CourseDto actualCourse = courseController.createNewCourse(mockCourseDto);
    // Then
    assertEquals(expectedCourse, actualCourse);
  }

  @Test
  void getAllCourses() {
    // Given
    List<CourseDto> expectedCourses = Arrays.asList(mockCourseDto,mockCourseDto,mockCourseDto);
    Mockito.when(courseService.getAllCourses())
        .thenReturn(Arrays.asList(mockCourse,mockCourse,mockCourse));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Course.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Course.class)));
    // When
    List<CourseDto> actualCourses = courseController.getAllCourses();
    // Then
    assertEquals(expectedCourses, actualCourses);
  }

  @Test
  void getCourse() {
    // Given
    Long courseId = mockCourse.getId();
    CourseDto expectedCourse = mockCourseDto;
    Mockito.when(courseService.getCourse(Mockito.anyLong()))
        .thenReturn(mockCourse);
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Course.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Course.class)));
    // When
    CourseDto actualCourse = courseController.getCourse(courseId);
    // Then
    assertEquals(expectedCourse, actualCourse);
  }

  @Test
  void updateCourse() {
    // Given
    Long courseId = mockCourse.getId();
    CourseDto expectedCourse = mockCourseDto;
    Mockito.when(courseService.updateCourseInfo(Mockito.anyLong(), Mockito.any(Course.class)))
        .thenAnswer(i -> i.getArgument(1));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Course.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Course.class)));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(CourseDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, CourseDto.class)));
    // When
    CourseDto actualCourse = courseController.updateCourse(courseId, mockCourseDto);
    // Then
    assertEquals(expectedCourse, actualCourse);
  }

  @Test
  void deleteCourse() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.doNothing().when(courseService).deleteCourse(Mockito.anyLong());
    // When
    courseController.deleteCourse(courseId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .deleteCourse(Mockito.anyLong());
  }

  @Test
  void enrollStudent() {
    // Given
    Long courseId = mockCourse.getId();
    Long studentId = mockStudent.getId();
    Mockito.doNothing().when(courseService).enrollStudent(Mockito.anyLong(),Mockito.anyLong());
    // When
    courseController.enrollStudent(courseId, studentId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .enrollStudent(Mockito.anyLong(),Mockito.anyLong());
  }

  @Test
  void getEnrolledStudents() {
    // Given
    Long courseId = mockCourse.getId();
    Set<Student> expectedStudents = new HashSet<>(
        Arrays.asList(mockStudent, mockStudent, mockStudent));
    Mockito.when(courseService.getEnrolledStudents(Mockito.anyLong()))
        .thenReturn(new HashSet<>(Arrays.asList(mockStudent, mockStudent, mockStudent)));
    // When
    Set<Student> actualStudents = courseController.getEnrolledStudents(courseId);
    // Then
    assertEquals(expectedStudents, actualStudents);
  }

  @Test
  void assignLecturer() {
    // Given
    Long courseId = mockCourse.getId();
    Long lecturerId = mockLecturer.getId();
    Mockito.doNothing().when(courseService).assignLecturer(Mockito.anyLong(), Mockito.anyLong());
    // When
    courseController.assignLecturer(courseId, lecturerId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .assignLecturer(Mockito.anyLong(), Mockito.anyLong());
  }

  @Test
  void getCourseLecturers() {
    // Given
    Long courseId = mockCourse.getId();
    Set<Lecturer> expectedLecturers = new HashSet<>(
        Arrays.asList(mockLecturer,mockLecturer,mockLecturer));
    Mockito.when(courseService.getCourseLecturers(Mockito.anyLong()))
        .thenReturn(new HashSet<>(Arrays.asList(mockLecturer,mockLecturer,mockLecturer)));
    // When
    Set<Lecturer> actualLecturers = courseController.getCourseLecturers(courseId);
    // Then
    assertEquals(expectedLecturers, actualLecturers);
    
  }

  @Test
  void getActiveSubmissions() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseSubmission> expectedCourseSubmissions =
        Arrays.asList(mockCourseSubmission, mockCourseSubmission, mockCourseSubmission);
    Mockito.when(courseService.getActiveSubmissions(Mockito.anyLong()))
        .thenReturn(expectedCourseSubmissions);
    // When
    List<CourseSubmission> actualCourseSubmissions = courseController.getActiveSubmissions(
        courseId);
    // Then
    assertEquals(expectedCourseSubmissions, actualCourseSubmissions);
  }

  @Test
  void approveCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    Mockito.doNothing().when(courseService)
        .approveCourseSubmission(Mockito.anyLong());
    // When
    courseController.approveCourseSubmission(courseSubmissionId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .approveCourseSubmission(Mockito.anyLong());
  }

  @Test
  void rejectCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    Mockito.doNothing().when(courseService)
        .rejectCourseSubmission(Mockito.anyLong());
    // When
    courseController.rejectCourseSubmission(courseSubmissionId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .rejectCourseSubmission(Mockito.anyLong());
  }

  @Test
  void revokeCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    Mockito.doNothing().when(courseService)
        .revokeCourseSubmission(Mockito.anyLong());
    // When
    courseController.revokeCourseSubmission(courseSubmissionId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .revokeCourseSubmission(Mockito.anyLong());
  }

  @Test
  void getCoursesToBeApproved() {
    // Given
    List<CourseDto> expectedCourses = Arrays.asList(mockCourseDto, mockCourseDto, mockCourseDto);
    Mockito.when(courseService.getCoursesToBeApproved())
        .thenReturn(Arrays.asList(mockCourse, mockCourse, mockCourse));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Course.class))).thenAnswer(
        i -> mapper.modelToDto(i.getArgument(0, Course.class)));
    // When
    List<CourseDto> actualCourses = courseController.getCoursesToBeApproved();
    // Then
    assertEquals(expectedCourses, actualCourses);
  }

  @Test
  void approveCourseCreation() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.doNothing().when(courseService).approveCourseCreation(Mockito.anyLong());
    // When
    courseController.approveCourseCreation(courseId);
    // Then
    Mockito.verify(courseService, Mockito.times(1))
        .approveCourseCreation(Mockito.anyLong());
  }
}