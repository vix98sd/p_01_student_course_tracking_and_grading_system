package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.CourseExamService;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseExamControllerTest {

  @Mock
  private CourseExamService courseExamService;
  @InjectMocks
  private CourseExamController courseExamController;

  private final Course mockCourse = mockObjects.getCourse();
  private final CourseExam mockCourseExam = mockObjects.getCourseExam();
  private final CourseExamResult mockCourseExamResult = mockObjects.getCourseExamResult();

  @Test
  void addNewCourseExam() {
    // Given
    CourseExam expectedCourseExam = mockCourseExam;
    Mockito.when(courseExamService.addNewCourseExam(Mockito.any(CourseExam.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseExam actualCourseExam = courseExamController.addNewCourseExam(mockCourseExam);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void getCourseExam() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    CourseExam expectedCourseExam = mockCourseExam;
    Mockito.when(courseExamService.getCourseExam(Mockito.anyLong())).thenReturn(mockCourseExam);
    // When
    CourseExam actualCourseExam = courseExamController.getCourseExam(courseExamId);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void getCourseExams() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseExam> expectedCourseExams =
        Arrays.asList(mockCourseExam, mockCourseExam, mockCourseExam);
    Mockito.when(courseExamService.getCourseExams(Mockito.anyLong()))
        .thenReturn(expectedCourseExams);
    // When
    List<CourseExam> actualCourseExams = courseExamController.getCourseExams(courseId);
    // Then
    assertEquals(expectedCourseExams, actualCourseExams);
  }

  @Test
  void getActiveCourseExams() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseExam> expectedCourseExams =
        Arrays.asList(mockCourseExam, mockCourseExam, mockCourseExam);
    Mockito.when(courseExamService.getActiveCourseExams(Mockito.anyLong()))
        .thenReturn(expectedCourseExams);
    // When
    List<CourseExam> actualCourseExams = courseExamController.getActiveCourseExams(courseId);
    // Then
    assertEquals(expectedCourseExams, actualCourseExams);
  }

  @Test
  void updateCourseExamInfo() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    CourseExam expectedCourseExam = mockCourseExam;
    Mockito.when(courseExamService.updateCourseExamInfo(Mockito.any(CourseExam.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseExam actualCourseExam = courseExamController.updateCourseExamInfo(mockCourseExam);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void deleteCourseExam() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    Mockito.doNothing().when(courseExamService).deleteCourseExam(Mockito.anyLong());
    // When
    courseExamController.deleteCourseExam(courseExamId);
    // Then
    Mockito.verify(courseExamService, Mockito.times(1))
        .deleteCourseExam(Mockito.anyLong());
  }

  @Test
  void addExamResult() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    Mockito.doNothing().when(courseExamService)
        .addExamResult(Mockito.anyLong(), Mockito.any(CourseExamResult.class));
    // When
    courseExamController.addExamResult(courseExamId, mockCourseExamResult);
    // Then
    Mockito.verify(courseExamService, Mockito.times(1))
        .addExamResult(Mockito.anyLong(), Mockito.any(CourseExamResult.class));
  }

  @Test
  void getCourseExamResults() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    List<CourseExamResult> expectedCourseExamResults =
        Arrays.asList(mockCourseExamResult, mockCourseExamResult, mockCourseExamResult);
    Mockito.when(courseExamService.getCourseExamResults(Mockito.anyLong()))
        .thenReturn(expectedCourseExamResults);
    // When
    List<CourseExamResult> actualCourseExamResults = courseExamController
        .getCourseExamResults(courseExamId);
    // Then
    assertEquals(expectedCourseExamResults, actualCourseExamResults);
  }

  @Test
  void deleteCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.doNothing().when(courseExamService).deleteCourseExamResult(Mockito.anyLong());
    // When
    courseExamController.deleteCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamService, Mockito.times(1))
        .deleteCourseExamResult(Mockito.anyLong());
  }

  @Test
  void approveCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.doNothing().when(courseExamService).approveCourseExamResult(Mockito.anyLong());
    // When
    courseExamController.approveCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamService, Mockito.times(1))
        .approveCourseExamResult(Mockito.anyLong());
  }

  @Test
  void rejectCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.doNothing().when(courseExamService).rejectCourseExamResult(Mockito.anyLong());
    // When
    courseExamController.rejectCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamService, Mockito.times(1))
        .rejectCourseExamResult(Mockito.anyLong());
  }
}