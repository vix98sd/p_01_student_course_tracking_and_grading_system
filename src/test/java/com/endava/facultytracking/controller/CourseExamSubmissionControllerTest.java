package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.dto.mapper.DtoMapperImpl;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.CourseExamService;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseExamSubmissionControllerTest {

  @Mock
  private CourseExamService courseExamService;
  @Mock
  private DtoMapper dtoMapper;
  @InjectMocks
  private CourseExamSubmissionController courseExamSubmissionController;
  private DtoMapper mapper = new DtoMapperImpl();

  private final CourseExamSubmission mockCourseExamSubmission = mockObjects.getCourseExamSubmission();
  private final CourseExamSubmissionDto mockCourseExamSubmissionDto =
      mapper.modelToDto(mockCourseExamSubmission, List.of(mockObjects.getCourseSchedule()),
          List.of(mockObjects.getCourseExamResult()));

  @Test
  void createNewFinalExamSubmission() {
    // Given
    Mockito.doNothing().when(courseExamService).createNewFinalExamSubmission(Mockito.any(
        CourseExamSubmission.class));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(CourseExamSubmissionDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, CourseExamSubmissionDto.class)));
    // When
    courseExamSubmissionController.createNewFinalExamSubmission(mockCourseExamSubmissionDto);
    // Then
    Mockito.verify(courseExamService)
        .createNewFinalExamSubmission(Mockito.any(CourseExamSubmission.class));
  }

  @Test
  void approveFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    Mockito.doNothing().when(courseExamService).approveFinalExamSubmission(Mockito.anyLong());
    // When
    courseExamSubmissionController.approveFinalExamSubmission(finalExamSubmissionId);
    // Then
    Mockito.verify(courseExamService).approveFinalExamSubmission(Mockito.anyLong());
  }

  @Test
  void rejectFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    Mockito.doNothing().when(courseExamService).rejectFinalExamSubmission(Mockito.anyLong());
    // When
    courseExamSubmissionController.rejectFinalExamSubmission(finalExamSubmissionId);
    // Then
    Mockito.verify(courseExamService).rejectFinalExamSubmission(Mockito.anyLong());
  }

  @Test
  void revokeFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    Mockito.doNothing().when(courseExamService).revokeFinalExamSubmission(Mockito.anyLong());
    // When
    courseExamSubmissionController.revokeFinalExamSubmission(finalExamSubmissionId);
    // Then
    Mockito.verify(courseExamService).revokeFinalExamSubmission(Mockito.anyLong());
  }

  @Test
  void getActiveSubmissions() {
    // Given
    Long finalCourseExamId = 1L;
    List<CourseExamSubmissionDto> expectedCourseExamSubmissions =
        Arrays.asList(mockCourseExamSubmissionDto,mockCourseExamSubmissionDto,mockCourseExamSubmissionDto);
    Mockito.when(courseExamService.getActiveSubmissions(Mockito.anyLong(), Mockito.any(DtoMapper.class)))
        .thenReturn(expectedCourseExamSubmissions);
    // When
    List<CourseExamSubmissionDto> actualCurseExamSubmissions = courseExamSubmissionController
        .getActiveSubmissions(finalCourseExamId);
    // Then
    assertEquals(expectedCourseExamSubmissions, actualCurseExamSubmissions);
  }
}