package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.CourseScheduleService;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseScheduleControllerTest {

  @Mock
  private CourseScheduleService courseScheduleService;
  @InjectMocks
  private CourseScheduleController courseScheduleController;

  private final CourseSchedule mockCourseSchedule = mockObjects.getCourseSchedule();


  @Test
  void addNewCourseSchedule() {
    // Given
    Long courseId = mockCourseSchedule.getCourse().getId();
    CourseSchedule expectedCourseSchedule = mockObjects.getCourseSchedule();
    Mockito.when(courseScheduleService.addNewCourseSchedule(Mockito.anyLong(),
        Mockito.any(CourseSchedule.class))).thenAnswer(i -> i.getArgument(1));
    // When
    CourseSchedule actualCourseSchedule = courseScheduleController
        .addNewCourseSchedule(courseId, mockCourseSchedule);
    // Then
    assertEquals(expectedCourseSchedule, actualCourseSchedule);
  }

  @Test
  void addNewCourseScheduleSeries() {
    // Given
    Long courseId = mockCourseSchedule.getCourse().getId();
    CourseSchedule expectedCourseSchedule = mockObjects.getCourseSchedule();
    List<CourseSchedule> expectedCourseSchedules =
        Arrays.asList(expectedCourseSchedule, expectedCourseSchedule, expectedCourseSchedule);
    Mockito.when(courseScheduleService.addNewCourseScheduleSeries(Mockito.anyLong(),
        Mockito.any(CourseSchedule.class))).thenReturn(
        Arrays.asList(mockCourseSchedule, mockCourseSchedule, mockCourseSchedule));
    // When
    List<CourseSchedule> actualCourseSchedules = courseScheduleController
        .addNewCourseScheduleSeries(courseId, mockCourseSchedule);
    // Then
    assertEquals(expectedCourseSchedules, actualCourseSchedules);
  }

  @Test
  void getCurrentFullScheduleForWeek() {
    // Given
    Long courseId = mockCourseSchedule.getCourse().getId();
    int day = 1;
    int month = 10;
    int year = 2021;
    CourseSchedule expectedCourseSchedule = mockObjects.getCourseSchedule();
    List<CourseSchedule> expectedCourseSchedules =
        Arrays.asList(expectedCourseSchedule, expectedCourseSchedule, expectedCourseSchedule);
    Mockito.when(courseScheduleService.getCurrentFullScheduleForWeek(
        LocalDate.of(year, month, day))).thenReturn(
        Arrays.asList(mockCourseSchedule, mockCourseSchedule, mockCourseSchedule));
    // When
    List<CourseSchedule> actualCourseSchedules = courseScheduleController
        .getCurrentFullScheduleForWeek(day, month, year);
    // Then
    assertEquals(expectedCourseSchedules, actualCourseSchedules);
  }

  @Test
  void getCourseScheduleById() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    CourseSchedule expectedCourseSchedule = mockObjects.getCourseSchedule();
    Mockito.when(courseScheduleService.getCourseScheduleById(Mockito.anyLong()))
        .thenReturn(mockCourseSchedule);
    // When
    CourseSchedule actualCourseSchedule = courseScheduleController
        .getCourseScheduleById(courseScheduleId);
    // Then
    assertEquals(expectedCourseSchedule, actualCourseSchedule);
  }

  @Test
  void updateCourseScheduleInfo() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getCourse().getId();
    CourseSchedule expectedCourseSchedule = mockObjects.getCourseSchedule();
    Mockito.when(courseScheduleService.updateCourseScheduleInfo(Mockito.anyLong(),
        Mockito.any(CourseSchedule.class))).thenAnswer(i -> i.getArgument(1));
    // When
    CourseSchedule actualCourseSchedule = courseScheduleController
        .updateCourseScheduleInfo(courseScheduleId, mockCourseSchedule);
    // Then
    assertEquals(expectedCourseSchedule, actualCourseSchedule);
  }

  @Test
  void cancelCourseSchedule() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getCourse().getId();
    Mockito.doNothing().when(courseScheduleService)
        .cancelCourseSchedule(Mockito.anyLong(), Mockito.anyBoolean());
    // When
    courseScheduleController.cancelCourseSchedule(courseScheduleId);
    // Then
    Mockito.verify(courseScheduleService, Mockito.times(1))
        .cancelCourseSchedule(Mockito.anyLong(), Mockito.anyBoolean());
  }

  @Test
  void addAttendee() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getCourse().getId();
    Long studentId = mockObjects.getStudent().getId();
    Mockito.doNothing().when(courseScheduleService)
        .addAttendee(Mockito.anyLong(), Mockito.anyLong());
    // When
    courseScheduleController.addAttendee(courseScheduleId, studentId);
    // Then
    Mockito.verify(courseScheduleService, Mockito.times(1))
        .addAttendee(Mockito.anyLong(), Mockito.anyLong());
  }

  @Test
  void addAttendees() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getCourse().getId();
    Long studentId = mockObjects.getStudent().getId();
    List<Long> studentIds = Arrays.asList(studentId, studentId, studentId);
    Mockito.doNothing().when(courseScheduleService)
        .addAttendees(Mockito.anyLong(), Mockito.anyList());
    // When
    courseScheduleController.addAttendees(courseScheduleId, studentIds);
    // Then
    Mockito.verify(courseScheduleService, Mockito.times(1))
        .addAttendees(Mockito.anyLong(), Mockito.anyList());
  }

  @Test
  void removeAttendee() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getCourse().getId();
    Long studentId = mockObjects.getStudent().getId();
    Mockito.doNothing().when(courseScheduleService)
        .removeAttendee(Mockito.anyLong(), Mockito.anyLong());
    // When
    courseScheduleController.removeAttendee(courseScheduleId, studentId);
    // Then
    Mockito.verify(courseScheduleService, Mockito.times(1))
        .removeAttendee(Mockito.anyLong(), Mockito.anyLong());
  }

  @Test
  void getAttended() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getCourse().getId();
    Student student = mockObjects.getStudent();
    Set<Student> expectedStudents = new HashSet<>();
    expectedStudents.add(student);
    Mockito.when(courseScheduleService.getAttended(Mockito.anyLong()))
        .thenReturn(expectedStudents);
    // When
    Set<Student> actualStudents = courseScheduleController.getAttended(courseScheduleId);
    // Then
    assertEquals(expectedStudents, actualStudents);
  }
}