package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.service.LecturerService;
import com.endava.facultytracking.utility.mockObjects;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LecturerControllerTest {

  @Mock
  private LecturerService lecturerService;
  @InjectMocks
  private LecturerController lecturerController;

  private final Lecturer mockLecturer = mockObjects.getLecturer();

  @Test
  void addNewLecturer() {
    // Given
    Lecturer expectedLecturer = mockObjects.getLecturer();
    Mockito.when(lecturerService.createNewLecturer(Mockito.any(Lecturer.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    Lecturer actualLecturer = lecturerController.addNewLecturer(mockLecturer);
    // Then
    assertEquals(expectedLecturer, actualLecturer);
  }

  @Test
  void getAllLecturers() {
    // Given
    Lecturer expectedLecturer = mockObjects.getLecturer();
    List<Lecturer> expectedLecturers =
        Arrays.asList(expectedLecturer, expectedLecturer, expectedLecturer);
    Mockito.when(lecturerService.getAllLecturers())
        .thenReturn(Arrays.asList(mockLecturer, mockLecturer, mockLecturer));
    // When
    List<Lecturer> actualLecturers = lecturerController.getAllLecturers();
    // Then
    assertEquals(expectedLecturers, actualLecturers);
  }

  @Test
  void getLecturer() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Lecturer expectedLecturer = mockObjects.getLecturer();
    Mockito.when(lecturerService.getLecturer(Mockito.anyLong())).thenReturn(mockLecturer);
    // When
    Lecturer actualLecturer = lecturerController.getLecturer(lecturerId);
    // Then
    assertEquals(expectedLecturer, actualLecturer);
  }

  @Test
  void updateLecturerInfo() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Lecturer expectedLecturer = mockObjects.getLecturer();
    Mockito.when(lecturerService.updateLecturerInfo(Mockito.anyLong(),
            Mockito.any(Lecturer.class))).thenAnswer(i -> i.getArgument(1));
    // When
    Lecturer actualLecturer = lecturerController
        .updateLecturerInfo(lecturerId, mockLecturer);
    // Then
    assertEquals(expectedLecturer, actualLecturer);
  }

  @Test
  void deleteLecturer() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Mockito.doNothing().when(lecturerService).deleteLecturer(Mockito.anyLong());
    // When
    lecturerController.deleteLecturer(lecturerId);
    // Then
    Mockito.verify(lecturerService, Mockito.times(1))
        .deleteLecturer(Mockito.anyLong());
  }

  @Test
  void getLecturerCourses() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Course course = mockObjects.getCourse();
    Set<Course> expectedLecturerCourses = new HashSet<>();
    expectedLecturerCourses.add(course);
    Mockito.when(lecturerService.getLecturerCourses(Mockito.anyLong()))
        .thenReturn(expectedLecturerCourses);
    // When
    Set<Course> actualLecturerCourses = lecturerController.getLecturerCourses(lecturerId);
    // Then
    assertEquals(expectedLecturerCourses, actualLecturerCourses);
  }
}