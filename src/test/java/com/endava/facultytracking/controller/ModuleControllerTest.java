package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.dto.ModuleDto;
import com.endava.facultytracking.domain.dto.SemesterDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.dto.mapper.DtoMapperImpl;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.service.ModuleService;
import com.endava.facultytracking.service.SemesterService;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ModuleControllerTest {

  @Mock
  private ModuleService moduleService;
  @Mock
  private DtoMapper dtoMapper;
  @InjectMocks
  private ModuleController moduleController;

  private final Module mockModule =
      new Module(1L, "Information Technologies", "IT", 3);
  private final DtoMapper mapper = new DtoMapperImpl();
  private final ModuleDto mockModuleDto = mapper.modelToDto(mockModule);

  @Test
  void createNewModule() {
    // Given
    ModuleDto expectedModule = mockModuleDto;
    Mockito.when(moduleService.createModule(Mockito.any(Module.class)))
        .thenAnswer(i -> i.getArgument(0, Module.class));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Module.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Module.class)));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(ModuleDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, ModuleDto.class)));
    // When
    ModuleDto actualModule = moduleController.createNewModule(mockModuleDto);
    // Then
    assertEquals(expectedModule, actualModule);
  }

  @Test
  void getAllModules() {
    // Given
    List<ModuleDto> expectedModules = Arrays.asList(mockModuleDto, mockModuleDto, mockModuleDto);
    Mockito.when(moduleService.getAllModules())
        .thenReturn(Arrays.asList(mockModule,mockModule,mockModule));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Module.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Module.class)));
    // When
    List<ModuleDto> actualModules = moduleController.getAllModules();
    // Then
    assertEquals(expectedModules, actualModules);
  }

  @Test
  void getModule() {
    // Given
    Long moduleId = mockModule.getId();
    ModuleDto expectedModule = mockModuleDto;
    Mockito.when(moduleService.getModule(Mockito.anyLong()))
        .thenReturn(mockModule);
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Module.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Module.class)));
    // When
    ModuleDto actualModule = moduleController.getModule(moduleId);
    // Then
    assertEquals(expectedModule, actualModule);
  }

  @Test
  void updateModuleInfo() {
    // Given
    Long moduleId = mockModule.getId();
    ModuleDto expectedModule = mockModuleDto;
    Mockito.when(moduleService.updateModuleInfo(Mockito.anyLong(), Mockito.any(Module.class)))
        .thenAnswer(i -> i.getArgument(1, Module.class));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Module.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Module.class)));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(ModuleDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, ModuleDto.class)));
    // When
    ModuleDto actualModule = moduleController.updateModuleInfo(moduleId, mockModuleDto);
    // Then
    assertEquals(expectedModule, actualModule);
  }

  @Test
  void deleteModule() {
    // Given
    Long moduleId = mockModule.getId();
    String expectedMessage = "Module deleted";
    Mockito.when(moduleService.deleteModule(Mockito.anyLong()))
        .thenReturn("Module deleted");
    // When
    String actualMessage = moduleController.deleteModule(moduleId);
    // Then
    assertEquals(expectedMessage, actualMessage);
  }
}