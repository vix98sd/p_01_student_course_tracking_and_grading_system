package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.dto.SemesterDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.dto.mapper.DtoMapperImpl;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.service.SemesterService;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

@ExtendWith(MockitoExtension.class)
class SemesterControllerTest {

  @Mock
  private SemesterService semesterService;
  @Mock
  private DtoMapper dtoMapper;
  @InjectMocks
  private SemesterController semesterController;

  private final Semester mockSemester =
      new Semester(LocalDate.of(2021, Month.MARCH, 01));
  private final DtoMapper mapper = new DtoMapperImpl();
  private final SemesterDto mockSemesterDto = mapper.modelToDto(mockSemester);

  @Test
  void createSemester() {
    // Given
    SemesterDto expectedSemester = mockSemesterDto;
    Mockito.when(semesterService.createSemester(Mockito.any(Semester.class)))
        .thenAnswer(i -> i.getArgument(0, Semester.class));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Semester.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Semester.class)));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(SemesterDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, SemesterDto.class)));
    // When
    SemesterDto actualSemester = semesterController.createSemester(mockSemesterDto);
    // Then
    assertEquals(expectedSemester, actualSemester);
  }

  @Test
  void getAllSemesters() {
    // Given
    List<SemesterDto> expectedSemesters =
        Arrays.asList(mockSemesterDto, mockSemesterDto, mockSemesterDto);
    Mockito.when(semesterService.getAllSemesters())
        .thenReturn(Arrays.asList(mockSemester, mockSemester, mockSemester));
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Semester.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Semester.class)));
    // When
    List<SemesterDto> actualSemesters = semesterController.getAllSemesters();
    // Then
    assertEquals(expectedSemesters, actualSemesters);
  }

  @Test
  void getSemester() {
    // Given
    Long semesterId = 1L;
    SemesterDto expectedSemester = mockSemesterDto;
    Mockito.when(semesterService.getSemester(Mockito.anyLong()))
        .thenReturn(mockSemester);
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Semester.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Semester.class)));
    // When
    SemesterDto actualSemester = semesterController.getSemester(semesterId);
    // Then
    assertEquals(expectedSemester, actualSemester);
  }

  @Test
  void updateSemesterInfo() {
    // Given
    Long semesterId = 1L;
    SemesterDto expectedSemester = mockSemesterDto;
    Mockito.when(semesterService.updateSemesterInfo(Mockito.anyLong(), Mockito.any(Semester.class)))
        .thenReturn(mockSemester);
    Mockito.when(dtoMapper.modelToDto(Mockito.any(Semester.class)))
        .thenAnswer(i -> mapper.modelToDto(i.getArgument(0, Semester.class)));
    Mockito.when(dtoMapper.dtoToModel(Mockito.any(SemesterDto.class)))
        .thenAnswer(i -> mapper.dtoToModel(i.getArgument(0, SemesterDto.class)));
    // When
    SemesterDto actualSemester = semesterController.updateSemesterInfo(semesterId, mockSemesterDto);
    // Then
    assertEquals(expectedSemester, actualSemester);
  }

  @Test
  void deleteSemester() {
    // Given
    Long semesterId = 1L;
    String expectedMessage = "Semester deleted";
    Mockito.when(semesterService.deleteSemester(Mockito.anyLong()))
        .thenReturn("Semester deleted");
    // When
    String actualMessage = semesterController.deleteSemester(semesterId);
    // Then
    assertEquals(expectedMessage, actualMessage);

  }
}