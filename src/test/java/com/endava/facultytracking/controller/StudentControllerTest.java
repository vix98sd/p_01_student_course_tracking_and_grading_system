package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.service.StudentService;
import com.endava.facultytracking.utility.mockObjects;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StudentControllerTest {

  @Mock
  private StudentService studentService;
  @InjectMocks
  private StudentController studentController;

  private final Student mockStudent = mockObjects.getStudent();

  @Test
  void createNewStudent() {
    // Given
    Student expectedStudent = mockObjects.getStudent();
    Mockito.when(studentService.createNewStudent(Mockito.any(Student.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    Student actualStudent = studentController.createNewStudent(mockStudent);
    // Then
    assertEquals(expectedStudent, actualStudent);
  }

  @Test
  void getAllStudents() {
    // Given
    Student expectedStudent = mockObjects.getStudent();
    List<Student> expectedStudents =
        Arrays.asList(expectedStudent, expectedStudent, expectedStudent);
    Mockito.when(studentService.getAllStudents())
        .thenReturn(Arrays.asList(mockStudent, mockStudent, mockStudent));
    // When
    List<Student> actualStudents = studentController.getAllStudents();
    // Then
    assertEquals(expectedStudents, actualStudents);
  }

  @Test
  void getStudent() {
    // Given
    Long studentId = mockStudent.getId();
    Student expectedStudent = mockObjects.getStudent();
    Mockito.when(studentService.getStudent(Mockito.anyLong()))
        .thenReturn(mockStudent);
    // When
    Student actualStudent = studentController.getStudent(studentId);
    // Then
    assertEquals(expectedStudent, actualStudent);
  }

  @Test
  void updateStudent() {
    // Given
    Long studentId = mockStudent.getId();
    Student expectedStudent = mockObjects.getStudent();
    Mockito.when(studentService.updateStudent(Mockito.anyLong(),
        Mockito.any(Student.class))).thenAnswer(i -> i.getArgument(1));
    // When
    Student actualStudent = studentController.updateStudent(studentId, mockStudent);
    // Then
    assertEquals(expectedStudent, actualStudent);
  }

  @Test
  void deleteStudent() {
    // Given
    Long studentId = mockStudent.getId();
    Mockito.doNothing().when(studentService).deleteStudent(Mockito.anyLong());
    // When
    studentController.deleteStudent(studentId);
    // Then
    Mockito.verify(studentService, Mockito.times(1))
        .deleteStudent(Mockito.anyLong());
  }

  @Test
  void getEnrolledCourses() {
    // Given
    Long studentId = mockStudent.getId();
    Course course = mockObjects.getCourse();
    Set<Course> expectedCourses = new HashSet<>();
    expectedCourses.add(course);
    Mockito.when(studentService.getEnrolledCourses(Mockito.anyLong())).thenReturn(expectedCourses);
    // When
    Set<Course> actualCourses = studentController.getEnrolledCourses(studentId);
    // Then
    assertEquals(expectedCourses, actualCourses);
  }

  @Test
  void assignModule() {
    // Given
    Long studentId = mockStudent.getId();
    Long moduleId = mockObjects.getModule().getId();
    Mockito.doNothing().when(studentService).assignModule(Mockito.anyLong(), Mockito.anyLong());
    // When
    studentController.assignModule(studentId, moduleId);
    // Then
    Mockito.verify(studentService, Mockito.times(1))
        .assignModule(Mockito.anyLong(), Mockito.anyLong());
  }

  @Test
  void getAssignedModules() {
    // Given
    Long studentId = mockStudent.getId();
    Module module = mockObjects.getModule();
    Set<Module> expectedModules = new HashSet<>();
    expectedModules.add(module);
    Mockito.when(studentService.getAssignedModules(Mockito.anyLong()))
        .thenReturn(expectedModules);
    // When
    Set<Module> actualModules = studentController.getAssignedModules(studentId);
    // Then
    assertEquals(expectedModules, actualModules);
  }
}