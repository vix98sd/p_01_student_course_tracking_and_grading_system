package com.endava.facultytracking.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.model.StudentsGrade;
import com.endava.facultytracking.service.StudentsGradeService;
import com.endava.facultytracking.utility.mockObjects;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StudentsGradeControllerTest {

  @Mock
  private StudentsGradeService studentsGradeService;
  @InjectMocks
  private StudentsGradeController studentsGradeController;

  private final StudentsGrade mockStudentsGrade = mockObjects.getStudentsGrade();

  @Test
  void addNewStudentsGrade() {
    // Given
    // When
    // Then
  }

  @Test
  void getAllStudentsGrades() {
    // Given
    // When
    // Then
  }

  @Test
  void getAllGradesByCourse() {
    // Given
    // When
    // Then
  }

  @Test
  void getStudentsGradeByCourse() {
    // Given
    // When
    // Then
  }

  @Test
  void annulGrade() {
    // Given
    // When
    // Then
  }
}