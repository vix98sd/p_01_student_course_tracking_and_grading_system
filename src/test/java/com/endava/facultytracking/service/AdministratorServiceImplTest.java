package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.repository.AdministratorRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
class AdministratorServiceImplTest {

  @Mock
  private final Administrator newAdministratorInfo = mockObjects.getNewAdministrator();

  @Mock
  private AdministratorRepository administratorRepository;
  @Mock
  private PasswordEncoder passwordEncoder;
  @InjectMocks
  private AdministratorServiceImpl administratorService;

  private final Administrator mockAdministrator = mockObjects.getAdministrator();

  @Test
  void createNewAdministrator() {
    // Given
    Administrator expectedAdministrator = mockAdministrator;
    Mockito.when(administratorRepository.save(Mockito.any(Administrator.class)))
        .thenAnswer(i -> i.getArgument(0));
    Mockito.when(passwordEncoder.encode(Mockito.anyString()))
        .thenAnswer(i -> i.getArgument(0));
    // When
    Administrator actualAdministrator =
        administratorService.createNewAdministrator(mockAdministrator);
    // Then
    assertEquals(expectedAdministrator, actualAdministrator);
  }

  @Test
  void createNewAdministratorInvalidArgumentException() {
    // Given
    String expectedMessage = "all arguments are required";
    Administrator noFirstNameAdministrator = mockObjects.getNewAdministrator();
    noFirstNameAdministrator.setFirstName(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        administratorService.createNewAdministrator(noFirstNameAdministrator)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewAdministratorInvalidArgumentException2() {
    // Given
    String expectedMessage = "all arguments are required";
    Administrator noLastNameAdministrator = mockObjects.getAdministrator();
    noLastNameAdministrator.setLastName(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        administratorService.createNewAdministrator(noLastNameAdministrator)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewAdministratorInvalidArgumentException3() {
    // Given
    String expectedMessage = "all arguments are required";
    Administrator noUsernameAdministrator = mockObjects.getAdministrator();
    noUsernameAdministrator.setUsername(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        administratorService.createNewAdministrator(noUsernameAdministrator)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewAdministratorInvalidArgumentException4() {
    // Given
    String expectedMessage = "all arguments are required";
    Administrator noPasswordAdministrator = mockObjects.getAdministrator();
    noPasswordAdministrator.setPassword(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        administratorService.createNewAdministrator(noPasswordAdministrator)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAdministratorById() {
    // Given
    Administrator expectedAdministrator = mockAdministrator;
    Long administratorId = expectedAdministrator.getId();
    Mockito.when(administratorRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockAdministrator));
    // When
    Administrator actualAdministrator = administratorService.getAdministrator(administratorId);
    // Then
    assertEquals(expectedAdministrator, actualAdministrator);
  }

  @Test
  void getAdministratorByIdNoIdException() {
    // Given
    Long administratorId = null;
    String expectedMessage =
        String.format("administrator with id %s was not found", administratorId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        administratorService.getAdministrator(administratorId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAdministratorByIdNotFoundException() {
    // Given
    Long administratorId = mockAdministrator.getId();
    String expectedMessage =
        String.format("administrator with id %s was not found", administratorId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        administratorService.getAdministrator(administratorId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAdministratorByUsername() {
    // Given
    String administratorUsername = mockAdministrator.getUsername();
    Administrator expectedAdministrator = mockObjects.getAdministrator();
    Mockito.when(administratorRepository.findByUsername(Mockito.anyString()))
        .thenReturn(java.util.Optional.of(mockAdministrator));
    // When
    Administrator actualAdministrator = administratorService
        .getAdministrator(administratorUsername);
    // Then
    assertEquals(expectedAdministrator, actualAdministrator);
  }

  @Test
  void getAdministratorByUsernameException() {
    // Given
    String administratorUsername = null;
    String expectedMessage =
        String.format("administrator with username %s was not found", administratorUsername);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        administratorService.getAdministrator(administratorUsername)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAdministratorByUsernameException2() {
    // Given
    String administratorUsername = mockAdministrator.getUsername();
    String expectedMessage =
        String.format("administrator with username %s was not found", administratorUsername);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        administratorService.getAdministrator(administratorUsername)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAllAdministrators() {
    // Given
    List<Administrator> expectedAdministrators =
        Arrays.asList(mockAdministrator, mockAdministrator, mockAdministrator);
    Mockito.when(administratorRepository.findAll()).thenReturn(expectedAdministrators);
    // When
    List<Administrator> actualAdministrators = administratorService.getAllAdministrators();
    // Then
    assertEquals(expectedAdministrators, actualAdministrators);
  }

  @Test
  void updateAdministratorInfo() {
    // Given
    Mockito.when(administratorRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(newAdministratorInfo));
    Mockito.doNothing().when(newAdministratorInfo).setFirstName(Mockito.anyString());
    Mockito.doNothing().when(newAdministratorInfo).setLastName(Mockito.anyString());
    Mockito.doNothing().when(newAdministratorInfo).setPassword(Mockito.anyString());
    // When
    administratorService.updateAdministratorInfo(mockAdministrator);
    // Then
    Mockito.verify(newAdministratorInfo, Mockito.times(1))
        .setFirstName(Mockito.anyString());
    Mockito.verify(newAdministratorInfo, Mockito.times(1))
        .setLastName(Mockito.anyString());
    Mockito.verify(newAdministratorInfo, Mockito.times(1))
        .setPassword(Mockito.anyString());
  }

  @Test
  void deleteAdministrator() {
    // Given
    Long administratorId = mockAdministrator.getId();
    Mockito.when(administratorRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockAdministrator));
    Mockito.doNothing().when(administratorRepository).delete(Mockito.any(Administrator.class));
    // When
    administratorService.deleteAdministrator(administratorId);
    // Then
    Mockito.verify(administratorRepository, Mockito.times(1))
        .delete(Mockito.any(Administrator.class));
  }
}