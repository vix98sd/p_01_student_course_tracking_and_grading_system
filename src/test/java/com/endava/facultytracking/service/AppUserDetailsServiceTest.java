package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.utility.mockObjects;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
class AppUserDetailsServiceTest {

  @Mock
  private AdministratorService administratorService;
  @Mock
  private LecturerService lecturerService;
  @Mock
  private StudentService studentService;
  @Mock
  private RoleService roleService;
  @InjectMocks
  private AppUserDetailsService appUserDetailsService;

  private final UserDetails mockUser = mockObjects.getAdministrator();
  private final Administrator mockAdministrator = mockObjects.getAdministrator();
  private final Lecturer mockLecturer = mockObjects.getLecturer();
  private final Student mockStudent = mockObjects.getStudent();
  private final Role mockRole = mockObjects.getRoleAdmin();

  @Test
  void loadUserByUsernameAdmin() {
    // Given
    Administrator expectedUser = mockAdministrator;
    String username = mockUser.getUsername();
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenReturn(expectedUser);
    // When
    UserDetails actualUser = appUserDetailsService.loadUserByUsername(username);
    // Then
    assertEquals(expectedUser, actualUser);
  }

  @Test
  void loadUserByUsernameLecturer() {
    // Given
    Lecturer expectedUser = mockLecturer;
    String username = mockUser.getUsername();
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(lecturerService.getLecturer(Mockito.anyString()))
        .thenReturn(expectedUser);
    // When
    UserDetails actualUser = appUserDetailsService.loadUserByUsername(username);
    // Then
    assertEquals(expectedUser, actualUser);
  }

  @Test
  void loadUserByUsernameStudent() {
    // Given
    Student expectedUser = mockStudent;
    String username = mockUser.getUsername();
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(lecturerService.getLecturer(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(studentService.getStudent(Mockito.anyString()))
        .thenReturn(expectedUser);
    // When
    UserDetails actualUser = appUserDetailsService.loadUserByUsername(username);
    // Then
    assertEquals(expectedUser, actualUser);
  }

  @Test
  void loadUserByUsernameNotFoundException() {
    // Given
    String username = mockUser.getUsername();
    String expectedMessage = String.format("user with username %s was not found", username);
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(lecturerService.getLecturer(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(studentService.getStudent(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        appUserDetailsService.loadUserByUsername(username)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addRoleToUserAdmin() {
    // Given
    String username = mockUser.getUsername();
    String roleName = mockRole.getName();
    Mockito.when(roleService.getRole(Mockito.anyString()))
        .thenReturn(mockRole);
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenReturn(mockAdministrator);
    // When
    appUserDetailsService.addRoleToUser(username, roleName);
    // Then
    Mockito.verify(administratorService, Mockito.times(1))
        .getAdministrator(Mockito.anyString());
  }

  @Test
  void addRoleToUserLecturer() {
    // Given
    String username = mockUser.getUsername();
    String roleName = mockRole.getName();
    Mockito.when(roleService.getRole(Mockito.anyString()))
        .thenReturn(mockRole);
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(lecturerService.getLecturer(Mockito.anyString()))
        .thenReturn(mockLecturer);
    // When
    appUserDetailsService.addRoleToUser(username, roleName);
    // Then
    Mockito.verify(administratorService, Mockito.times(1))
        .getAdministrator(Mockito.anyString());
  }

  @Test
  void addRoleToUserStudent() {
    // Given
    String username = mockUser.getUsername();
    String roleName = mockRole.getName();
    Mockito.when(roleService.getRole(Mockito.anyString()))
        .thenReturn(mockRole);
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(lecturerService.getLecturer(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(studentService.getStudent(Mockito.anyString()))
        .thenReturn(mockStudent);
    // When
    appUserDetailsService.addRoleToUser(username, roleName);
    // Then
    Mockito.verify(administratorService, Mockito.times(1))
        .getAdministrator(Mockito.anyString());
  }

  @Test
  void addRoleToUserException() {
    // Given
    String username = mockUser.getUsername();
    String roleName = mockRole.getName();
    String expectedMessage = String.format("user with username %s was not found", username);
    Mockito.when(roleService.getRole(Mockito.anyString()))
        .thenReturn(mockRole);
    Mockito.when(administratorService.getAdministrator(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(lecturerService.getLecturer(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    Mockito.when(studentService.getStudent(Mockito.anyString()))
        .thenThrow(new RecordNotFoundException("administrator not found"));
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        appUserDetailsService.addRoleToUser(username, roleName)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewUserAdministrator() {
    // Given
    Mockito.when(administratorService.createNewAdministrator(Mockito.any(Administrator.class)))
        .thenReturn(mockAdministrator);
    // When
    appUserDetailsService.createNewUser(mockAdministrator);
    // Then
    Mockito.verify(administratorService, Mockito.times(1))
        .createNewAdministrator(Mockito.any(Administrator.class));
  }

  @Test
  void createNewUserLecturer() {
    // Given
    Mockito.when(lecturerService.createNewLecturer(Mockito.any(Lecturer.class)))
        .thenReturn(mockLecturer);
    // When
    appUserDetailsService.createNewUser(mockLecturer);
    // Then
    Mockito.verify(lecturerService, Mockito.times(1))
        .createNewLecturer(Mockito.any(Lecturer.class));
  }

  @Test
  void createNewUserStudent() {
    // Given
    Mockito.when(studentService.createNewStudent(Mockito.any(Student.class)))
        .thenReturn(mockStudent);
    // When
    appUserDetailsService.createNewUser(mockStudent);
    // Then
    Mockito.verify(studentService, Mockito.times(1))
        .createNewStudent(Mockito.any(Student.class));
  }

  @Test
  void createNewRole() {
    // Given
    Mockito.doNothing().when(roleService).saveRole(Mockito.any(Role.class));
    // When
    appUserDetailsService.createNewRole(mockRole);
    // Then
    Mockito.verify(roleService, Mockito.times(1))
        .saveRole(Mockito.any(Role.class));
  }
}