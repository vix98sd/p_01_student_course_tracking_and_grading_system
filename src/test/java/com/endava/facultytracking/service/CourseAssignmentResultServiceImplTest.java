package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseAssignmentResultRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class CourseAssignmentResultServiceImplTest {

  @Mock
  private CourseAssignmentResultRepository courseAssignmentResultRepository;
  @Mock
  private StudentService studentService;
  @InjectMocks
  private CourseAssignmentResultServiceImpl courseAssignmentResultService;

  private Student mockStudent = mockObjects.getStudent();
  private final CourseAssignment mockCourseAssignment = mockObjects.getCourseAssignment();
  private final CourseAssignmentResult mockCourseAssignmentResult = mockObjects.getCourseAssignmentResult();

  @Test
  void addNewCourseAssignmentResult() {
    // Given
    CourseAssignmentResult expectedCourseAssignmentResult = mockCourseAssignmentResult;
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(mockStudent);
    Mockito.when(courseAssignmentResultRepository.save(Mockito.any(CourseAssignmentResult.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseAssignmentResult actualCourseAssignmentResult = courseAssignmentResultService.addNewCourseAssignmentResult(
        mockCourseAssignmentResult);
    // Then
    assertEquals(expectedCourseAssignmentResult, actualCourseAssignmentResult);
  }

  @Test
  void addNewCourseAssignmentResultInvalidArgumentException() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignmentResult noPercentDoneCourseAssignmentResult =
        mockObjects.getCourseAssignmentResult();
    noPercentDoneCourseAssignmentResult.setPercentDone(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentResultService
            .addNewCourseAssignmentResult(noPercentDoneCourseAssignmentResult)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentResultInvalidArgumentException2() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignmentResult noStudentCourseAssignmentResult =
        mockObjects.getCourseAssignmentResult();
    noStudentCourseAssignmentResult.setStudent(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentResultService
            .addNewCourseAssignmentResult(noStudentCourseAssignmentResult)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentResultInvalidArgumentException3() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignmentResult noCourseAssignmentCourseAssignmentResult =
        mockObjects.getCourseAssignmentResult();
    noCourseAssignmentCourseAssignmentResult.setCourseAssignment(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentResultService
            .addNewCourseAssignmentResult(noCourseAssignmentCourseAssignmentResult)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseAssignmentResult() {
    // Given
    Long courseAssignmentResultId = mockCourseAssignmentResult.getId();
    CourseAssignmentResult expectedCourseAssignmentResult = mockCourseAssignmentResult;
    Mockito.when(courseAssignmentResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignmentResult));
    // When
    CourseAssignmentResult actualCourseAssignmentResult = courseAssignmentResultService
        .getCourseAssignmentResult(courseAssignmentResultId);
    // Then
    assertEquals(expectedCourseAssignmentResult, actualCourseAssignmentResult);
  }

  @Test
  void getCourseAssignmentResultRecordNotFoundException() {
    // Given
    Long courseAssignmentResultId = null;
    String expectedMessage = String.format("course assignment result with id %s does not exist",
        courseAssignmentResultId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseAssignmentResultService.getCourseAssignmentResult(
            courseAssignmentResultId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseAssignmentResultRecordNotFoundException2() {
    // Given
    Long courseAssignmentResultId = 2L;
    String expectedMessage = String.format("course assignment result with id %s does not exist",
        courseAssignmentResultId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseAssignmentResultService.getCourseAssignmentResult(
            courseAssignmentResultId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseAssignmentResults() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    List<CourseAssignmentResult> expectedCourseAssignmentResults =
        Arrays.asList(mockCourseAssignmentResult, mockCourseAssignmentResult,
            mockCourseAssignmentResult);
    Mockito.when(courseAssignmentResultRepository.findAllByCourseAssignment(
        Mockito.any(CourseAssignment.class))).thenReturn(expectedCourseAssignmentResults);
    // When
    List<CourseAssignmentResult> actualCourseAssignmentResults = courseAssignmentResultService.getCourseAssignmentResults(
        mockCourseAssignment);
    // Then
    assertEquals(expectedCourseAssignmentResults, actualCourseAssignmentResults);
  }

  @Test
  void deleteCourseAssignmentResult() {
    // Given
    Long courseAssignmentResultId = mockCourseAssignmentResult.getId();
    Mockito.when(courseAssignmentResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignmentResult));
    Mockito.doNothing().when(courseAssignmentResultRepository)
        .delete(Mockito.any(CourseAssignmentResult.class));
    // When
    courseAssignmentResultService.deleteCourseAssignmentResult(courseAssignmentResultId);
    // Then
    Mockito.verify(courseAssignmentResultRepository, Mockito.times(1)).findById(Mockito.anyLong());
    Mockito.verify(courseAssignmentResultRepository, Mockito.times(1))
        .delete(Mockito.any(CourseAssignmentResult.class));
  }
}