package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseAssignmentRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class CourseAssignmentServiceImplTest {

  @Mock
  private CourseAssignmentRepository courseAssignmentRepository;
  @Mock
  private CourseService courseService;
  @Mock
  private CourseAssignmentResultService courseAssignmentResultService;

  @InjectMocks
  private CourseAssignmentServiceImpl courseAssignmentService;

  private final Course mockCourse = mockObjects.getCourse();
  private final CourseAssignment mockCourseAssignment = mockObjects.getCourseAssignment();

  @Test
  void addNewCourseAssignment() {
    // Given
    CourseAssignment expectedCourseAssignment = mockCourseAssignment;
    Mockito.when(courseAssignmentRepository.save(Mockito.any(CourseAssignment.class)))
        .thenAnswer(i -> i.getArgument(0));
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    CourseAssignment actualAssignment = courseAssignmentService.addNewCourseAssignment(
        mockCourseAssignment);
    // Then
    assertEquals(expectedCourseAssignment, actualAssignment);
  }

  @Test
  void addNewCourseAssignmentInvalidArgumentException() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignment noStartingDateCourseAssignment =
        new CourseAssignment(
            1L,
            null,
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            mockCourse
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(
            noStartingDateCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentInvalidArgumentException2() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignment noEndingDateCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 1),
            null,
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            mockCourse
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(noEndingDateCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentInvalidArgumentException3() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignment noStartingTimeCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            null,
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            mockCourse
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(
            noStartingTimeCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentInvalidArgumentException4() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignment noEndingTimeCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(13, 30),
            null,
            "Lorem ipsum dolor",
            mockCourse
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(noEndingTimeCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentInvalidArgumentException5() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignment noDescriptionCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            null,
            mockCourse
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(noDescriptionCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentInvalidArgumentException6() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseAssignment noCourseIdCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            null
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(noCourseIdCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentNotDuringSemesterException() {
    // Given
    String expectedMessage = "course assignment must be during the semester";
    CourseAssignment beforeSemesterCourseAssignment =
        new CourseAssignment(
            1L,
            mockCourse.getSemester().getStartingDate().minusDays(10),
            mockCourse.getSemester().getStartingDate().minusDays(5),
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            mockCourse
        );
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(
            beforeSemesterCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentNotDuringSemesterException2() {
    // Given
    String expectedMessage = "course assignment must be during the semester";
    CourseAssignment afterSemesterCourseAssignment =
        new CourseAssignment(
            1L,
            mockCourse.getSemester().getEndingDate().plusDays(5),
            mockCourse.getSemester().getEndingDate().plusDays(7),
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            mockCourse
        );
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(afterSemesterCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseAssignmentNotDuringSemesterException3() {
    // Given
    String expectedMessage = "course assignment must be during the semester";
    CourseAssignment endingBeforeStartingTimeCourseAssignment =
        new CourseAssignment(
            1L,
            mockCourse.getSemester().getStartingDate().plusDays(1),
            mockCourse.getSemester().getStartingDate().plusDays(1),
            LocalTime.of(13, 30),
            LocalTime.of(13, 00),
            "Lorem ipsum dolor",
            mockCourse
        );
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseAssignmentService.addNewCourseAssignment(endingBeforeStartingTimeCourseAssignment)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseAssignment() {
    // Given
    CourseAssignment expectedCourseAssignment = mockCourseAssignment;
    Long courseAssignmentId = expectedCourseAssignment.getId();
    Mockito.when(courseAssignmentRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignment));
    // When
    CourseAssignment actualCourseAssignment = courseAssignmentService.getCourseAssignment(
        courseAssignmentId);
    // Then
    assertEquals(expectedCourseAssignment, actualCourseAssignment);
  }

  @Test
  void getCourseAssignmentRecordNotFoundException() {
    // Given
    Long courseAssignmentId = null;
    String expectedMessage = String.format("course assignment with id %s does not exist",
        courseAssignmentId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseAssignmentService.getCourseAssignment(courseAssignmentId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseAssignmentRecordNotFoundException2() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    String expectedMessage = String.format("course assignment with id %s does not exist",
        courseAssignmentId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseAssignmentService.getCourseAssignment(courseAssignmentId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseAssignments() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseAssignment> expectedCourseAssignment =
        Arrays.asList(mockCourseAssignment, mockCourseAssignment, mockCourseAssignment);
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    Mockito.when(courseAssignmentRepository.findAllByCourse(Mockito.any(Course.class)))
        .thenReturn(expectedCourseAssignment);
    // When
    List<CourseAssignment> actualCourseAssignment = courseAssignmentService
        .getCourseAssignments(courseId);
    // Then
    assertEquals(expectedCourseAssignment, actualCourseAssignment);
  }

  @Test
  void updateCourseAssignmentInfo() {
    // Given
    CourseAssignment expectedCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 2),
            LocalDate.of(2021, Month.APRIL, 16),
            LocalTime.of(12, 30),
            LocalTime.of(16, 30),
            "Lorem ipsum dolor sit amet",
            mockCourse
        );
    Mockito.when(courseAssignmentRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignment));
    // When
    CourseAssignment actualCourseAssignment = courseAssignmentService
        .updateCourseAssignmentInfo(expectedCourseAssignment);
    // Then
    assertEquals(expectedCourseAssignment, actualCourseAssignment);
  }

  @Test
  void deleteCourseAssignment() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    Mockito.when(courseAssignmentRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignment));
    Mockito.doNothing().when(courseAssignmentRepository)
        .delete(Mockito.any(CourseAssignment.class));
    // When
    courseAssignmentService.deleteCourseAssignment(courseAssignmentId);
    // Then
    Mockito.verify(courseAssignmentRepository, Mockito.times(1))
        .findById(Mockito.anyLong());
    Mockito.verify(courseAssignmentRepository, Mockito.times(1))
        .delete(Mockito.any(CourseAssignment.class));
  }

  @Test
  void addNewResult() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    CourseAssignmentResult courseAssignmentResult =
        new CourseAssignmentResult(
            1L,
            95L,
            new Student(),
            mockCourseAssignment
        );
    Mockito.when(courseAssignmentRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignment));
    Mockito.when(courseAssignmentResultService
            .addNewCourseAssignmentResult(Mockito.any(CourseAssignmentResult.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseAssignmentService.addNewResult(courseAssignmentId, courseAssignmentResult);
    // Then
    Mockito.verify(courseAssignmentResultService, Mockito.times(1))
        .addNewCourseAssignmentResult(Mockito.any(CourseAssignmentResult.class));
  }

  @Test
  void getCourseAssignmentResults() {
    // Given
    Long courseAssignmentId = mockCourseAssignment.getId();
    CourseAssignmentResult courseAssignmentResult =
        new CourseAssignmentResult(
            1L,
            95L,
            new Student(),
            mockCourseAssignment
        );
    List<CourseAssignmentResult> expectedCourseAssignmentResults =
        Arrays.asList(courseAssignmentResult, courseAssignmentResult, courseAssignmentResult);
    Mockito.when(courseAssignmentRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseAssignment));
    Mockito.when(courseAssignmentResultService.getCourseAssignmentResults(
        Mockito.any(CourseAssignment.class))).thenReturn(expectedCourseAssignmentResults);
    // When
    List<CourseAssignmentResult> actualCourseAssignmentResults = courseAssignmentService
        .getCourseAssignmentResults(courseAssignmentId);
    // Then
    assertEquals(expectedCourseAssignmentResults, actualCourseAssignmentResults);
  }

  @Test
  void deleteCourseAssignmentResult() {
    // Given
    Long courseAssignmentResultId = 1L;
    Mockito.doNothing().when(courseAssignmentResultService)
        .deleteCourseAssignmentResult(Mockito.anyLong());
    // When
    courseAssignmentService.deleteCourseAssignmentResult(courseAssignmentResultId);
    // Then
    Mockito.verify(courseAssignmentResultService, Mockito.times(1))
        .deleteCourseAssignmentResult(Mockito.anyLong());
  }
}