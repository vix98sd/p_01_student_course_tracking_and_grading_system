package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseExamResultRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseExamResultServiceImplTest {

  @Mock
  private CourseExamResultRepository courseExamResultRepository;
  @Mock
  private StudentService studentService;
  @InjectMocks
  private CourseExamResultServiceImpl courseExamResultService;

  private final CourseExam mockCourseExam = mockObjects.getCourseExam();
  private final Student mockStudent = mockObjects.getStudent();
  private final CourseExamResult mockCourseExamResult = mockObjects.getCourseExamResult();

  @Test
  void addNewCourseExamResult() {
    // Given
    CourseExamResult expectedCourseExamResult = mockCourseExamResult;
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(mockStudent);
    Mockito.when(courseExamResultRepository.save(Mockito.any(CourseExamResult.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseExamResult actualCourseExamResult = courseExamResultService.addNewCourseExamResult(
        mockCourseExamResult);
    // Then
    assertEquals(expectedCourseExamResult, actualCourseExamResult);
  }

  @Test
  void addNewCourseExamResultWithAnnulling() {
    // Given
    CourseExamResult expectedCourseExamResult = mockCourseExamResult;
    CourseExamResult oldCourseExamResultToBeAnnulled = mockObjects.getCourseExamResult();
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(mockStudent);
    Mockito.when(courseExamResultRepository.findAllByCourseExamAndStudentAndIsAnnulled(
        Mockito.any(CourseExam.class), Mockito.any(Student.class), Mockito.anyBoolean()))
            .thenReturn(java.util.Optional.ofNullable(oldCourseExamResultToBeAnnulled));
    Mockito.when(courseExamResultRepository.save(Mockito.any(CourseExamResult.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseExamResult actualCourseExamResult = courseExamResultService.addNewCourseExamResult(
        mockCourseExamResult);
    // Then
    assertEquals(expectedCourseExamResult, actualCourseExamResult);
    Mockito.verify(courseExamResultRepository, Mockito.times(2))
        .save(Mockito.any(CourseExamResult.class));
    Mockito.verify(courseExamResultRepository, Mockito.times(1))
        .findAllByCourseExamAndStudentAndIsAnnulled(
            Mockito.any(CourseExam.class),
            Mockito.any(Student.class),
            Mockito.anyBoolean()
        );
  }

  @Test
  void addNewCourseExamResultInvalidArgumentException() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseExamResult noPercentDoneCourseExamResult = mockObjects.getCourseExamResult();
    noPercentDoneCourseExamResult.setPercentDone(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamResultService.addNewCourseExamResult(noPercentDoneCourseExamResult)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamResultInvalidArgumentException2() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseExamResult noStudentCourseExamResult = mockObjects.getCourseExamResult();
    noStudentCourseExamResult.setStudent(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamResultService.addNewCourseExamResult(noStudentCourseExamResult)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamResultInvalidArgumentException3() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseExamResult noCourseExamCourseExamResult = mockObjects.getCourseExamResult();
    noCourseExamCourseExamResult.setCourseExam(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamResultService.addNewCourseExamResult(noCourseExamCourseExamResult)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    CourseExamResult expectedCourseExamResult = mockCourseExamResult;
    Mockito.when(courseExamResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExamResult));
    // When
    CourseExamResult actualCourseExamResult = courseExamResultService
        .getCourseExamResult(courseExamResultId);
    // Then
    assertEquals(expectedCourseExamResult, actualCourseExamResult);
  }

  @Test
  void getCourseExamResultNoIdException() {
    // Given
    Long courseExamResultId = null;
    String expectedMessage = String.format("course exam result with id %s was not found",
        courseExamResultId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseExamResultService.getCourseExamResult(courseExamResultId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseExamResultNotFoundException() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    String expectedMessage = String.format("course exam result with id %s was not found",
        courseExamResultId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseExamResultService.getCourseExamResult(courseExamResultId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseExamResults() {
    // Given
    List<CourseExamResult> expectedCourseExamResults =
        Arrays.asList(mockCourseExamResult, mockCourseExamResult, mockCourseExamResult);
    Mockito.when(courseExamResultRepository.findAllByCourseExam(Mockito.any(CourseExam.class)))
        .thenReturn(expectedCourseExamResults);
    // When
    List<CourseExamResult> actualCourseExamResults = courseExamResultService
        .getCourseExamResults(mockCourseExam);
    // Then
    assertEquals(expectedCourseExamResults, actualCourseExamResults);
  }

  @Test
  void updateCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    CourseExamResult newCourseExamResult = mockObjects.getCourseExamResult();
    newCourseExamResult.setPercentDone(78L);
    Mockito.when(courseExamResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExamResult));
    // When
    courseExamResultService.updateCourseExamResult(courseExamResultId, newCourseExamResult);
    // Then
    Mockito.verify(courseExamResultRepository, Mockito.times(1))
        .findById(Mockito.anyLong());
  }

  @Test
  void deleteCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.when(courseExamResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExamResult));
    Mockito.doNothing().when(courseExamResultRepository)
        .delete(Mockito.any(CourseExamResult.class));
    // When
    courseExamResultService.deleteCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamResultRepository, Mockito.times(1))
        .delete(Mockito.any(CourseExamResult.class));
  }

  @Test
  void changeCourseExamResultStatusApprove() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    CourseExamResultStatus status = CourseExamResultStatus.APPROVED;
    Mockito.when(courseExamResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExamResult));
    // When
    courseExamResultService.changeCourseExamResultStatus(courseExamResultId, status);
    // Then
    Mockito.verify(courseExamResultRepository, Mockito.times(1))
        .findById(Mockito.anyLong());
  }

  @Test
  void changeCourseExamResultStatusReject() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    CourseExamResultStatus status = CourseExamResultStatus.REJECTED;
    Mockito.when(courseExamResultRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExamResult));
    // When
    courseExamResultService.changeCourseExamResultStatus(courseExamResultId, status);
    // Then
    Mockito.verify(courseExamResultRepository, Mockito.times(1))
        .findById(Mockito.anyLong());
  }
}