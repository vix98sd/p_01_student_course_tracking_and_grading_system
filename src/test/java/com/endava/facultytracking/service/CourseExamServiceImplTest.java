package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.dto.CourseExamSubmissionDto;
import com.endava.facultytracking.domain.dto.mapper.DtoMapper;
import com.endava.facultytracking.domain.dto.mapper.DtoMapperImpl;
import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseExamRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.tomcat.jni.Local;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseExamServiceImplTest {

  @Mock
  private CourseExamRepository courseExamRepository;
  @Mock
  private CourseService courseService;
  @Mock
  private CourseExamResultService courseExamResultService;
  @Mock
  private CourseExamSubmissionService courseExamSubmissionService;
  @Mock
  private CourseScheduleService courseScheduleService;
  @Mock
  private Clock clock;
  private final Clock defaultClock = Clock.systemDefaultZone();
  @InjectMocks
  private CourseExamServiceImpl courseExamService;

  @Mock
  private DtoMapper dtoMapper;

  private final Course mockCourse = mockObjects.getCourse();
  private final CourseExam mockCourseExam = mockObjects.getCourseExam();
  private final CourseExamResult mockCourseExamResult = mockObjects.getCourseExamResult();
  private final CourseExamSubmission mockCourseExamSubmission = mockObjects.getCourseExamSubmission();
  private final DtoMapper mapper = new DtoMapperImpl();
  private final CourseExamSubmissionDto mockCourseExamSubmissionDto =
      mapper.modelToDto(mockCourseExamSubmission, new ArrayList<>(),
          Arrays.asList(mockCourseExamResult, mockCourseExamResult, mockCourseExamResult));

  @Test
  void addNewCourseExamFinal() {
    // Given
    CourseExam expectedCourseExam = mockCourseExam;
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    Mockito.when(courseExamRepository.save(Mockito.any(CourseExam.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseExam actualCourseExam = courseExamService.addNewCourseExam(mockCourseExam);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void addNewCourseExam() {
    // Given
    Course mockCourseForNotFinal = mockObjects.getCourse();
    mockCourseForNotFinal.enrollStudent(mockObjects.getStudent());
    CourseExam expectedCourseExam = mockObjects.getCourseExam();
    expectedCourseExam.setStartingDate(expectedCourseExam.getCourse()
        .getSemester().getEndingDate().minusDays(1));
    expectedCourseExam.setIsFinal(false);
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourseForNotFinal);
    Mockito.when(courseExamRepository.save(Mockito.any(CourseExam.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    CourseExam actualCourseExam = courseExamService.addNewCourseExam(expectedCourseExam);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void addNewCourseExamInvalidArgumentException() {
    // Given
    CourseExam noStartingDateCourseExam = mockObjects.getCourseExam();
    noStartingDateCourseExam.setStartingDate(null);
    String expectedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(noStartingDateCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamInvalidArgumentException2() {
    // Given
    CourseExam noStartingTimeCourseExam = mockObjects.getCourseExam();
    noStartingTimeCourseExam.setStartingTime(null);
    String expectedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(noStartingTimeCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamInvalidArgumentException3() {
    // Given
    CourseExam noDescriptionCourseExam = mockObjects.getCourseExam();
    noDescriptionCourseExam.setDescription(null);
    String expectedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(noDescriptionCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamInvalidArgumentException4() {
    // Given
    CourseExam noCourseCourseExam = mockObjects.getCourseExam();
    noCourseCourseExam.setCourse(null);
    String expectedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(noCourseCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamNotDuringSemester() {
    // Given
    CourseExam beforeSemesterCourseExam = mockObjects.getCourseExam();
    beforeSemesterCourseExam.setIsFinal(false);
    beforeSemesterCourseExam.setStartingDate(beforeSemesterCourseExam.getCourse()
        .getSemester().getStartingDate().minusDays(1));
    String expectedMessage = "exam must be during semester";
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(beforeSemesterCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamNotDuringSemester2() {
    // Given
    CourseExam afterSemesterCourseExam = mockObjects.getCourseExam();
    afterSemesterCourseExam.setIsFinal(false);
    afterSemesterCourseExam.setStartingDate(afterSemesterCourseExam.getCourse()
        .getSemester().getEndingDate().plusDays(1));
    String expectedMessage = "exam must be during semester";
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(afterSemesterCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseExamFinalNotAfterSemester() {
    // Given
    CourseExam beforeEndOfSemesterCourseExam = mockObjects.getCourseExam();
    beforeEndOfSemesterCourseExam.setStartingDate(beforeEndOfSemesterCourseExam.getCourse()
        .getSemester().getStartingDate().minusDays(1));
    String expectedMessage = "final exam must be after semester";
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamService.addNewCourseExam(beforeEndOfSemesterCourseExam)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseExam() {
    // Given
    CourseExam expectedCourseExam = mockCourseExam;
    Long courseExamId = mockCourseExam.getId();
    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExam));
    // When
    CourseExam actualCourseExam = courseExamService.getCourseExam(courseExamId);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void getCourseExamNoIdException() {
    // Given
    Long courseExamId = null;
    String expectedMessage = String.format("course exam with id %s does not exist", courseExamId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseExamService.getCourseExam(courseExamId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseExamNotFoundException() {
    // Given
    Long courseExamId = 1L;
    String expectedMessage = String.format("course exam with id %s does not exist", courseExamId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseExamService.getCourseExam(courseExamId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseExams() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseExam> expectedCourseExams =
        Arrays.asList(mockCourseExam, mockCourseExam, mockCourseExam);
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    Mockito.when(courseExamRepository.findAllByCourse(Mockito.any(Course.class)))
        .thenReturn(expectedCourseExams);
    // When
    List<CourseExam> actualCourseExams = courseExamService.getCourseExams(courseId);
    // Then
    assertEquals(expectedCourseExams, actualCourseExams);
  }

  @Nested
  @DisplayName("Tests that needs clock")
  class NestedTests {

    @BeforeEach
    void setUpClock() {
      Mockito.doReturn(defaultClock.instant()).when(clock).instant();
      Mockito.doReturn(defaultClock.getZone()).when(clock).getZone();
    }

    @Test
    void getActiveCourseExams() {
      // Given
      Long courseId = mockCourse.getId();
      List<CourseExam> expectedCourseExams =
          Arrays.asList(mockCourseExam, mockCourseExam, mockCourseExam);
      Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
      Mockito.when(courseExamRepository.findAllByCourseAndStartingDateGreaterThanEqual(
          Mockito.any(Course.class), Mockito.any(LocalDate.class))).thenReturn(expectedCourseExams);
      // When
      List<CourseExam> actualCourseExams = courseExamService.getActiveCourseExams(courseId);
      // Then
      assertEquals(expectedCourseExams, actualCourseExams);
    }
  }

  @Test
  void updateCourseExamInfo() {
    // Given
    CourseExam expectedCourseExam = mockObjects.getCourseExam();
    expectedCourseExam.setStartingDate(LocalDate.now());
    expectedCourseExam.setStartingTime(expectedCourseExam.getStartingTime().plusMinutes(15));
    expectedCourseExam.setIsFinal(false);
    expectedCourseExam.setDescription("New description");
    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExam));
    // When
    CourseExam actualCourseExam = courseExamService.updateCourseExamInfo(expectedCourseExam);
    // Then
    assertEquals(expectedCourseExam, actualCourseExam);
  }

  @Test
  void deleteCourseExam() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExam));
    Mockito.doNothing().when(courseExamRepository).delete(Mockito.any(CourseExam.class));
    // When
    courseExamService.deleteCourseExam(courseId);
    // Then
    Mockito.verify(courseExamRepository, Mockito.times(1))
        .delete(Mockito.any(CourseExam.class));
  }

  @Test
  void updateExamResult() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExam));
    Mockito.doNothing().when(courseExamResultService)
        .updateCourseExamResult(Mockito.anyLong(), Mockito.any(CourseExamResult.class));
    // When
    courseExamService.addExamResult(courseExamId, mockCourseExamResult);
    // Then
    Mockito.verify(courseExamResultService, Mockito.times(1))
        .updateCourseExamResult(Mockito.anyLong(), Mockito.any(CourseExamResult.class));
  }

  @Test
  void addNewExamResult() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    CourseExam courseExamWithoutResults = mockObjects.getCourseExam();
    courseExamWithoutResults.setResults(new HashSet<>());
    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(courseExamWithoutResults));
    Mockito.when(courseExamResultService.addNewCourseExamResult(Mockito.any(CourseExamResult.class)))
        .thenReturn(mockCourseExamResult);
    // When
    courseExamService.addExamResult(courseExamId, mockCourseExamResult);
    // Then
    Mockito.verify(courseExamResultService, Mockito.times(1))
        .addNewCourseExamResult(Mockito.any(CourseExamResult.class));
  }

  @Test
  void getCourseExamResults() {
    // Given
    Long courseExamId = mockCourseExam.getId();
    List<CourseExamResult> expectedCourseExamResults =
        Arrays.asList(mockCourseExamResult, mockCourseExamResult, mockCourseExamResult);
    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExam));
    Mockito.when(courseExamResultService.getCourseExamResults(Mockito.any(CourseExam.class)))
        .thenReturn(expectedCourseExamResults);
    // When
    List<CourseExamResult> actualCourseExamResults = courseExamService
        .getCourseExamResults(courseExamId);
    // Then
    assertEquals(expectedCourseExamResults, actualCourseExamResults);
  }

  @Test
  void deleteCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.doNothing().when(courseExamResultService).deleteCourseExamResult(Mockito.anyLong());
    // When
    courseExamService.deleteCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamResultService, Mockito.times(1))
        .deleteCourseExamResult(Mockito.anyLong());
  }

  @Test
  void createNewFinalExamSubmission() {
    // Given
    Mockito.doNothing().when(courseExamSubmissionService)
        .addNewFinalExamSubmission(Mockito.any(CourseExamSubmission.class));
    // When
    courseExamService.createNewFinalExamSubmission(mockCourseExamSubmission);
    // Then
    Mockito.verify(courseExamSubmissionService, Mockito.times(1))
        .addNewFinalExamSubmission(Mockito.any(CourseExamSubmission.class));
  }

  @Test
  void approveFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    Mockito.when(courseExamSubmissionService.resolveFinalExamSubmission(Mockito.anyLong(),
        Mockito.any(SubmissionStatus.class))).thenReturn(mockCourseExamSubmission);
    Mockito.when(
            courseExamResultService.addNewCourseExamResult(Mockito.any(CourseExamResult.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseExamService.approveFinalExamSubmission(finalExamSubmissionId);
    // Then
    Mockito.verify(courseExamSubmissionService, Mockito.times(1))
        .resolveFinalExamSubmission(Mockito.anyLong(),
            Mockito.any(SubmissionStatus.class));
    Mockito.verify(courseExamResultService, Mockito.times(1))
        .addNewCourseExamResult(Mockito.any(CourseExamResult.class));
  }

  @Test
  void rejectFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    Mockito.when(courseExamSubmissionService.resolveFinalExamSubmission(Mockito.anyLong(),
        Mockito.any(SubmissionStatus.class))).thenReturn(mockCourseExamSubmission);
    // When
    courseExamService.rejectFinalExamSubmission(finalExamSubmissionId);
    // Then
    Mockito.verify(courseExamSubmissionService, Mockito.times(1))
        .resolveFinalExamSubmission(Mockito.anyLong(),
            Mockito.any(SubmissionStatus.class));
  }

  @Test
  void revokeFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    Mockito.when(courseExamSubmissionService.resolveFinalExamSubmission(Mockito.anyLong(),
        Mockito.any(SubmissionStatus.class))).thenReturn(mockCourseExamSubmission);
    // When
    courseExamService.revokeFinalExamSubmission(finalExamSubmissionId);
    // Then
    Mockito.verify(courseExamSubmissionService, Mockito.times(1))
        .resolveFinalExamSubmission(Mockito.anyLong(),
            Mockito.any(SubmissionStatus.class));
  }

  @Test
  void getActiveSubmissions() {
    // Given
    Long finalExamId = mockCourseExam.getId();
    List<CourseExamSubmissionDto> expectedCourseExamSubmissionsDto =
        Arrays.asList(mockCourseExamSubmissionDto, mockCourseExamSubmissionDto,
            mockCourseExamSubmissionDto);
    expectedCourseExamSubmissionsDto.forEach(submission -> submission.setNumberOfClasses(2));

    Mockito.when(courseExamRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExam));

    Mockito.when(courseExamSubmissionService.getFinalExamSubmissions(Mockito.any(CourseExam.class)))
        .thenReturn(Arrays.asList(mockCourseExamSubmission, mockCourseExamSubmission,
            mockCourseExamSubmission));

    Mockito.when(courseScheduleService.getAllSchedulesForCourse(Mockito.anyLong()))
        .thenReturn(Arrays.asList(mockObjects.getCourseSchedule(), mockObjects.getCourseSchedule()));

    CourseExam mockMidTermCourseExam = mockObjects.getCourseExam();
    mockMidTermCourseExam.setStartingDate(mockMidTermCourseExam.getStartingDate().minusDays(15));
    mockMidTermCourseExam.setIsFinal(false);
    mockMidTermCourseExam.getResults().forEach(r -> {
      r.setPercentDone(55L);
      r.setIsPresent(true);
    });
    List<CourseExam> mockMidTermCourseExams =
        Arrays.asList(mockMidTermCourseExam, mockMidTermCourseExam, mockMidTermCourseExam);

    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);

    Mockito.when(courseExamRepository.findAllByCourse(Mockito.any(Course.class)))
        .thenReturn(mockMidTermCourseExams);

    Mockito.when(courseExamResultService.getCourseExamResults(Mockito.any(CourseExam.class)))
        .thenAnswer(i -> new ArrayList<>(i.getArgument(0, CourseExam.class).getResults()));

    Mockito.when(dtoMapper.modelToDto(Mockito.any(CourseExamSubmission.class), Mockito.anyList(), Mockito.anyList()))
        .thenAnswer(i ->
            mapper.modelToDto(
                i.getArgument(0, CourseExamSubmission.class),
                i.getArgument(1, List.class),
                i.getArgument(2, ArrayList.class)
            )
        );
    // When
    List<CourseExamSubmissionDto> actualCourseExamSubmissionsDto = courseExamService
        .getActiveSubmissions(finalExamId, dtoMapper);
    // Then
    assertEquals(expectedCourseExamSubmissionsDto, actualCourseExamSubmissionsDto);
  }

  @Test
  void approveCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.doNothing().when(courseExamResultService)
        .changeCourseExamResultStatus(Mockito.anyLong(), Mockito.any(CourseExamResultStatus.class));
    // When
    courseExamService.approveCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamResultService, Mockito.times(1))
        .changeCourseExamResultStatus(Mockito.anyLong(), Mockito.any(CourseExamResultStatus.class));
  }

  @Test
  void rejectCourseExamResult() {
    // Given
    Long courseExamResultId = mockCourseExamResult.getId();
    Mockito.doNothing().when(courseExamResultService)
        .changeCourseExamResultStatus(Mockito.anyLong(), Mockito.any(CourseExamResultStatus.class));
    // When
    courseExamService.rejectCourseExamResult(courseExamResultId);
    // Then
    Mockito.verify(courseExamResultService, Mockito.times(1))
        .changeCourseExamResultStatus(Mockito.anyLong(), Mockito.any(CourseExamResultStatus.class));
  }
}