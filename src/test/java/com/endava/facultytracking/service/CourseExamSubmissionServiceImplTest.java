package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseExamSubmissionRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseExamSubmissionServiceImplTest {

  @Mock
  private CourseExamSubmissionRepository courseExamSubmissionRepository;
  @Mock
  private Clock clock;
  private final Clock defaultClock = Clock.systemDefaultZone();
  @InjectMocks
  private CourseExamSubmissionServiceImpl courseExamSubmissionService;

  private final CourseExam mockCourseExam = mockObjects.getCourseExam();
  private final Student mockStudent = mockObjects.getStudent();
  private final CourseExamSubmission mockCourseExamSubmission = mockObjects.getCourseExamSubmission();

  @Test
  void addNewFinalExamSubmission() {
    // Given
    Mockito.when(courseExamSubmissionRepository.save(Mockito.any(CourseExamSubmission.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseExamSubmissionService.addNewFinalExamSubmission(mockCourseExamSubmission);
    // Then
    Mockito.verify(courseExamSubmissionRepository, Mockito.times(1))
        .save(Mockito.any(CourseExamSubmission.class));
  }

  @Test
  void addNewFinalExamSubmissionInvalidArgumentException() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseExamSubmission noStudentCourseExamSubmission =
        new CourseExamSubmission(
            1L,
            LocalDate.now(),
            LocalDate.now(),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            null,
            mockCourseExam
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamSubmissionService.addNewFinalExamSubmission(
            noStudentCourseExamSubmission)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewFinalExamSubmissionInvalidArgumentException2() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseExamSubmission noFinalExamCourseExamSubmission =
        new CourseExamSubmission(
            1L,
            LocalDate.now(),
            LocalDate.now(),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            mockStudent,
            null
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseExamSubmissionService.addNewFinalExamSubmission(
            noFinalExamCourseExamSubmission)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getFinalExamSubmission() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    CourseExamSubmission expectedCourseExamSubmission = mockCourseExamSubmission;
    Mockito.when(courseExamSubmissionRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseExamSubmission));
    // When
    CourseExamSubmission actualCurseExamSubmission = courseExamSubmissionService
        .getFinalExamSubmission(finalExamSubmissionId);
    // Then
    assertEquals(expectedCourseExamSubmission, actualCurseExamSubmission);
  }

  @Test
  void getFinalExamSubmissionNoIdException() {
    // Given
    Long finalExamSubmissionId = null;
    String expectedMessage = String.format("course exam submission with id %s was not found",
        finalExamSubmissionId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseExamSubmissionService.getFinalExamSubmission(finalExamSubmissionId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getFinalExamSubmissionNotFoundException() {
    // Given
    Long finalExamSubmissionId = mockCourseExamSubmission.getId();
    String expectedMessage = String.format("course exam submission with id %s was not found",
        finalExamSubmissionId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseExamSubmissionService.getFinalExamSubmission(finalExamSubmissionId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getFinalExamSubmissions() {
    // Given
    Long finalExamId = mockCourseExam.getId();
    List<CourseExamSubmission> expectedCourseExamSubmissions =
        Arrays.asList(mockCourseExamSubmission, mockCourseExamSubmission, mockCourseExamSubmission);
    Mockito.when(courseExamSubmissionRepository
            .findAllByFinalExamAndStatus(Mockito.any(CourseExam.class),
                Mockito.any(SubmissionStatus.class)))
        .thenReturn(expectedCourseExamSubmissions);
    // When
    List<CourseExamSubmission> actualCurseExamSubmissions = courseExamSubmissionService
        .getFinalExamSubmissions(mockCourseExam);
    // Then
    assertEquals(expectedCourseExamSubmissions, actualCurseExamSubmissions);
  }

  @Nested
  @DisplayName("Tests that needs clock")
  class NestedTests {

    @BeforeEach
    void setUpClock() {
      Mockito.doReturn(defaultClock.instant()).when(clock).instant();
      Mockito.doReturn(defaultClock.getZone()).when(clock).getZone();
    }

    @Test
    void resolveFinalExamSubmission() {
      // Given
      Long courseExamSubmissionId = mockCourseExamSubmission.getId();
      CourseExamSubmission expectedCourseExamSubmission =
          new CourseExamSubmission(
              1L,
              mockCourseExamSubmission.getDateSubmitted(),
              mockCourseExamSubmission.getDateResolved(),
              true,
              SubmissionStatus.REJECTED,
              mockCourseExamSubmission.getStudent(),
              mockCourseExamSubmission.getFinalExam()
          );
      Mockito.when(courseExamSubmissionRepository.findById(Mockito.anyLong()))
          .thenReturn(java.util.Optional.of(mockCourseExamSubmission));
      Mockito.when(courseExamSubmissionRepository.save(Mockito.any(CourseExamSubmission.class)))
          .thenAnswer(i -> i.getArgument(0));
      // When
      CourseExamSubmission actualCourseExamSubmission = courseExamSubmissionService.resolveFinalExamSubmission(
          courseExamSubmissionId, SubmissionStatus.REJECTED);
      // Then
      assertEquals(expectedCourseExamSubmission, actualCourseExamSubmission);
    }
  }
}