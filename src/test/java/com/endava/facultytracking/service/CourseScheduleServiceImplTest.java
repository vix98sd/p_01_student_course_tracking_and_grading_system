package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseScheduleRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class CourseScheduleServiceImplTest {

  @Mock
  private CourseScheduleRepository courseScheduleRepository;
  @Mock
  private CourseService courseService;
  @Mock
  private StudentService studentService;

  @InjectMocks
  private CourseScheduleServiceImpl courseScheduleService;

  private final Course mockCourse = mockObjects.getCourse();
  private final CourseSchedule mockCourseSchedule = mockObjects.getCourseSchedule();

  @Test
  void addNewCourseSchedule() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.when(courseScheduleRepository.save(Mockito.any(CourseSchedule.class)))
        .thenReturn(mockCourseSchedule);
    Mockito.when(courseScheduleRepository.findByStartingDateAndIsCanceledOrderByStartingDate(
        Mockito.any(LocalDate.class), Mockito.anyBoolean())).thenReturn(new ArrayList<>());
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    CourseSchedule actualCourseSchedule = courseScheduleService
        .addNewCourseSchedule(courseId, mockCourseSchedule);
    // Then
    assertEquals(mockCourseSchedule, actualCourseSchedule);
  }

  @Test
  void addNewCourseScheduleInvalidArgumentsException() {
    // Given
    Long noCourseId = null;
    String expectedMessage = "all arguments are required";
    CourseSchedule courseSchedule = new CourseSchedule(mockCourseSchedule);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(noCourseId, courseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleInvalidArgumentsException2() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = "all arguments are required";
    CourseSchedule noStartingDateCourseSchedule = new CourseSchedule(mockCourseSchedule);
    noStartingDateCourseSchedule.setStartingDate(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId,
            noStartingDateCourseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleInvalidArgumentsException3() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = "all arguments are required";
    CourseSchedule noStartingTimeCourseSchedule = new CourseSchedule(mockCourseSchedule);
    noStartingTimeCourseSchedule.setStartingTime(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId,
            noStartingTimeCourseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleInvalidArgumentsException4() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = "all arguments are required";
    CourseSchedule noNumberOfClassesCourseSchedule = new CourseSchedule(mockCourseSchedule);
    noNumberOfClassesCourseSchedule.setNumberOfClasses(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId,
            noNumberOfClassesCourseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleInvalidArgumentsException5() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = "all arguments are required";
    CourseSchedule noTypeCourseSchedule = new CourseSchedule(mockCourseSchedule);
    noTypeCourseSchedule.setType(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId, noTypeCourseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleNotDuringSemesterException() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = "course schedule must be during the semester";
    CourseSchedule courseSchedule = new CourseSchedule(mockCourseSchedule);
    courseSchedule
        .setStartingDate(courseSchedule.getCourse().getSemester().getStartingDate().minusDays(5));
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId, courseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleNotDuringSemesterException2() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = "course schedule must be during the semester";
    CourseSchedule courseSchedule = new CourseSchedule(mockCourseSchedule);
    courseSchedule
        .setStartingDate(courseSchedule.getCourse().getSemester().getEndingDate().plusDays(5));
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId, courseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleOverlapsException() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = String.format("your course overlaps with %1$s on %2$s",
        mockCourseSchedule.getCourse().getName(), mockCourseSchedule.getStartingDate());
    CourseSchedule courseSchedule = new CourseSchedule(mockCourseSchedule);
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    Mockito.when(courseScheduleRepository.findByStartingDateAndIsCanceledOrderByStartingDate(
            Mockito.any(LocalDate.class), Mockito.anyBoolean()))
        .thenReturn(Arrays.asList(courseSchedule));
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseScheduleService.addNewCourseSchedule(courseId, courseSchedule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseScheduleSeries() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseSchedule> expectedCourseSchedules = new ArrayList<>();
    for (int i = 0; i < 16; i++) {
      CourseSchedule weaklyCourseSchedule = new CourseSchedule(mockCourseSchedule);
      weaklyCourseSchedule.setStartingDate(mockCourseSchedule.getStartingDate().plusWeeks(i));
      expectedCourseSchedules.add(weaklyCourseSchedule);
    }
    Mockito.when(courseScheduleRepository.saveAll(Mockito.anyIterable()))
        .thenAnswer(i -> i.getArgument(0));
    CourseSchedule mockCourseSchedule2 = new CourseSchedule(mockCourseSchedule);
    mockCourseSchedule2.setStartingTime(mockCourseSchedule.getStartingTime().plusHours(5));
    Mockito.when(courseScheduleRepository.findByStartingDateAndIsCanceledOrderByStartingDate(
            Mockito.any(LocalDate.class), Mockito.anyBoolean()))
        .thenReturn(Arrays.asList(mockCourseSchedule2, mockCourseSchedule2));
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    // When
    List<CourseSchedule> actualCourseSchedules = courseScheduleService
        .addNewCourseScheduleSeries(courseId, mockCourseSchedule);
    // Then
    assertEquals(expectedCourseSchedules, actualCourseSchedules);
  }

  @Test
  void getCurrentFullScheduleForWeek() {
    // Given
    List<CourseSchedule> expectedCourseSchedules = Arrays.asList(mockCourseSchedule,
        mockCourseSchedule, mockCourseSchedule);
    Mockito.when(
            courseScheduleRepository.findAllBetweenDatesAndIsCanceled(Mockito.any(LocalDate.class),
                Mockito.any(LocalDate.class), Mockito.anyBoolean()))
        .thenReturn(expectedCourseSchedules);
    // When
    List<CourseSchedule> actualCourseSchedules = courseScheduleService
        .getCurrentFullScheduleForWeek(mockCourseSchedule.getStartingDate());
    // Then
    assertEquals(expectedCourseSchedules, actualCourseSchedules);
  }

  @Test
  void getAllSchedulesForCourse() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseSchedule> expectedSchedules =
        Arrays.asList(mockCourseSchedule, mockCourseSchedule, mockCourseSchedule);
    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
    Mockito.when(courseScheduleRepository.findAllByCourse(Mockito.any(Course.class)))
        .thenReturn(expectedSchedules);
    // When
    List<CourseSchedule> actualCourseSchedules =
        courseScheduleService.getAllSchedulesForCourse(courseId);
    // Then
    assertEquals(expectedSchedules, actualCourseSchedules);
  }

  @Test
  void getCourseScheduleById() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourseSchedule));
    // When
    CourseSchedule actualCourseSchedule = courseScheduleService.getCourseScheduleById(
        courseScheduleId);
    // Then
    assertEquals(mockCourseSchedule, actualCourseSchedule);
  }

  @Test
  void getCourseScheduleByIdNoIdException() {
    // Given
    Long courseScheduleId = null;
    String expectedMessage = String.format("course schedule with id %s was not found",
        courseScheduleId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseScheduleService.getCourseScheduleById(courseScheduleId)).getMessage();
    // Then
    assertEquals(expectedMessage, expectedMessage);
  }

  @Test
  void getCourseScheduleByIdNotFoundException() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    String expectedMessage = String.format("course schedule with id %s was not found",
        courseScheduleId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseScheduleService.getCourseScheduleById(courseScheduleId)).getMessage();
    // Then
    assertEquals(expectedMessage, expectedMessage);
  }

  @Test
  void updateCourseScheduleInfo() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    CourseSchedule newCourseScheduleInfo =
        new CourseSchedule(
            1L,
            mockCourseSchedule.getStartingDate().plusDays(1),
            mockCourseSchedule.getStartingTime().plusHours(1),
            mockCourseSchedule.getNumberOfClasses() + 1,
            mockCourseSchedule.getType() + "test",
            false,
            mockCourse
        );
    CourseSchedule mockCourseSchedule2 = new CourseSchedule(mockCourseSchedule);
    mockCourseSchedule2.setStartingTime(mockCourseSchedule.getStartingTime().plusHours(5));
    Mockito.when(courseScheduleRepository.findByStartingDateAndIsCanceledOrderByStartingDate(
            Mockito.any(LocalDate.class), Mockito.anyBoolean()))
        .thenReturn(Arrays.asList(mockCourseSchedule2, mockCourseSchedule2));
    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourseSchedule));
    // When
    CourseSchedule actualCourseScheduleInfo = courseScheduleService
        .updateCourseScheduleInfo(courseScheduleId, newCourseScheduleInfo);
    // Then
    assertEquals(newCourseScheduleInfo, actualCourseScheduleInfo);
  }

  @Test
  void cancelCourseSchedule() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourseSchedule));
    // When
    courseScheduleService.cancelCourseSchedule(courseScheduleId, false);
    // Then
    Mockito.verify(courseScheduleRepository, Mockito.times(1))
        .findById(Mockito.anyLong());
  }

  @Test
  void addAttendee() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    Long studentId = 1L;
    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourseSchedule));
    Mockito.when(studentService.getStudent(Mockito.anyLong()))
        .thenReturn(new Student());
    Mockito.when(courseScheduleRepository.save(Mockito.any(CourseSchedule.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseScheduleService.addAttendee(courseScheduleId, studentId);
    // Then
    Mockito.verify(courseScheduleRepository, Mockito.times(1))
        .save(Mockito.any(CourseSchedule.class));
  }

  @Test
  void addAttendees() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    List<Long> studentIds = Arrays.asList(1L, 2L, 3L);
    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourseSchedule));
    Mockito.when(studentService.getStudent(Mockito.anyLong()))
        .thenReturn(new Student());
    Mockito.when(courseScheduleRepository.save(Mockito.any(CourseSchedule.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseScheduleService.addAttendees(courseScheduleId, studentIds);
    // Then
    Mockito.verify(courseScheduleRepository, Mockito.times(1))
        .save(Mockito.any(CourseSchedule.class));
  }

  @Test
  void removeAttendee() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    Long studentId = 1L;
    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourseSchedule));
    Mockito.when(studentService.getStudent(Mockito.anyLong()))
        .thenReturn(new Student());
    Mockito.when(courseScheduleRepository.save(Mockito.any(CourseSchedule.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseScheduleService.removeAttendee(courseScheduleId, studentId);
    // Then
    Mockito.verify(courseScheduleRepository, Mockito.times(1))
        .save(Mockito.any(CourseSchedule.class));
  }

  @Test
  void getAttended() {
    // Given
    Long courseScheduleId = mockCourseSchedule.getId();
    Student mockStudent = mockObjects.getStudent();
    CourseSchedule expectedCourseSchedule = new CourseSchedule(mockCourseSchedule);
    expectedCourseSchedule.addAttendee(mockStudent);
    Set<Student> expectedAttendees = new HashSet<>();
    expectedAttendees.add(mockStudent);

    Mockito.when(courseScheduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(expectedCourseSchedule));
    // When
    Set<Student> actualAttendees = courseScheduleService.getAttended(courseScheduleId);
    // Then
    assertEquals(expectedAttendees, actualAttendees);
  }

}