package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

  @Mock
  private CourseRepository courseRepository;
  @Mock
  private ModuleService moduleService;
  @Mock
  private SemesterService semesterService;
  @Mock
  private StudentService studentService;
  @Mock
  private LecturerService lecturerService;
  @Mock
  private CourseSubmissionService courseSubmissionService;
  @Mock
  private Clock clock;
  @InjectMocks
  private CourseServiceImpl courseService;


  private final Module mockModule = mockObjects.getModule();
  private final Semester mockSemester = mockObjects.getSemester();
  private final Course mockCourse = mockObjects.getCourse();
  private final CourseSubmission mockCourseSubmission = mockObjects.getCourseSubmission();
  private final LocalDate LOCAL_DATE = mockSemester.getSubmissionPeriodFrom().plusDays(3);
  private Clock fixedClock;

  @Test
  void createNewCourseTest() {
    // Given
    Mockito.when(courseRepository.existsByCodeName(Mockito.anyString())).thenReturn(false);
    Mockito.when(moduleService.getModule(Mockito.anyLong())).thenReturn(mockModule);
    Mockito.when(courseRepository.save(Mockito.any(Course.class))).thenReturn(mockCourse);
    // When
    Course actualCourse = courseService.createNewCourse(mockCourse);
    // Then
    assertEquals(mockCourse, actualCourse);
  }

  @Test
  void createNewCourseExceptionTest() {
    // Given
    Course nullNameCourse =
        new Course(
            1L,
            null,
            "DSA",
            3,
            2,
            2,
            false,
            true,
            mockModule,
            null
        );
    String expectedMessage = "all parameters are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.createNewCourse(nullNameCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewCourseExceptionTest2() {
    // Given
    Course nullCodeNameCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            null,
            3,
            2,
            2,
            false,
            true,
            mockModule,
            null
        );
    String expectedMessage = "all parameters are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.createNewCourse(nullCodeNameCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewCourseExceptionTest3() {
    // Given
    Course nullLecturesPerWeekCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            "DSA",
            null,
            2,
            2,
            false,
            true,
            mockModule,
            null
        );
    String expectedMessage = "all parameters are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.createNewCourse(nullLecturesPerWeekCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewCourseExceptionTest4() {
    // Given
    Course nullClassExercisesPerWeekCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            "DSA",
            3,
            null,
            2,
            false,
            true,
            mockModule,
            null
        );
    String expectedMessage = "all parameters are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.createNewCourse(nullClassExercisesPerWeekCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewCourseExceptionTest5() {
    // Given
    Course nullLabExercisesPerWeekCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            "DSA",
            3,
            2,
            null,
            false,
            true,
            mockModule,
            null
        );
    String expectedMessage = "all parameters are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.createNewCourse(nullLabExercisesPerWeekCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewCourseExceptionTest6() {
    // Given
    Course nullModuleCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            "DSA",
            3,
            2,
            2,
            false,
            true,
            null,
            null
        );
    String expectedMessage = "all parameters are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.createNewCourse(nullModuleCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewCourseCodeNameExceptionTest() {
    // Given
    String codeName = mockCourse.getCodeName();
    Mockito.when(courseRepository.existsByCodeName(Mockito.anyString())).thenReturn(true);
    String expectedMessage = String.format("course with code name %s already exists", codeName);
    // When
    String actualMessage = assertThrows(RecordAlreadyExistException.class, () ->
        courseService.createNewCourse(mockCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAllCoursesTest() {
    // Given
    List<Course> expectedCourses = Arrays.asList(mockCourse, mockCourse, mockCourse);
    Mockito.when(courseRepository.findAllByIsApproved(Mockito.anyBoolean())).thenReturn(expectedCourses);
    // When
    List<Course> actualCourses = courseService.getAllCourses();
    // Then
    assertEquals(expectedCourses, actualCourses);
  }

  @Test
  void getCourseTest() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    // When
    Course actualCourse = courseService.getCourse(courseId);
    // Then
    assertEquals(mockCourse, actualCourse);
  }

  @Test
  void getCourseNotFoundExceptionTest() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = String.format("course with id %s was not found", courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseService.getCourse(courseId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseNotFoundExceptionTest2() {
    // Given
    Long courseId = null;
    String expectedMessage = String.format("course with id %s was not found", courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseService.getCourse(courseId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void updateCourseInfoTest() {
    // Given
    Long courseId = mockCourse.getId();
    Module updatedModule =
        new Module(2L, "Computer Sciences", "CS", 4);
    Semester newSemester =
        new Semester(1L, LocalDate.of(2021, Month.OCTOBER, 01), null, null, null, null, null);
    newSemester.setDatesByStartingDate();
    Course expectedUpdatedCourseInfo =
        new Course(
            courseId,
            "Data Structures and Algorithms 2",
            "DSA 2",
            32,
            22,
            22,
            true,
            true,
            updatedModule,
            newSemester
        );
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    Mockito.when(moduleService.getModule(Mockito.anyLong())).thenReturn(updatedModule);
    Mockito.when(semesterService.getSemester(Mockito.anyLong())).thenReturn(newSemester);
    // When
    Course actualUpdatedCourseInfo =
        courseService.updateCourseInfo(courseId, expectedUpdatedCourseInfo);
    // Then
    assertEquals(expectedUpdatedCourseInfo, actualUpdatedCourseInfo);
  }

  @Test
  void updateCourseInfoCodeNameExistExceptionTest() {
    // Given
    Long courseId = mockCourse.getId();
    Course expectedUpdatedCourseInfo =
        new Course(
            courseId,
            "Data Structures and Algorithms",
            "DSA 2",
            3,
            2,
            2,
            false,
            true,
            mockModule,
            null
        );
    String codeName = expectedUpdatedCourseInfo.getCodeName();
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    Mockito.when(courseRepository.existsByCodeName(Mockito.anyString())).thenReturn(true);
    String expectedMessage =
        String.format("course with code name %s already exists", codeName);
    // When
    String actualMessage = assertThrows(RecordAlreadyExistException.class, () ->
        courseService.updateCourseInfo(courseId, expectedUpdatedCourseInfo)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void updateCourseInfoNotExistExceptionTest() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage =
        String.format("course with id %s was not found", courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseService.updateCourseInfo(courseId, mockCourse)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void deleteCourseTest() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    // When
    courseService.deleteCourse(courseId);
    // Then
    assertTrue(true);
  }

  @Test
  void deleteCourseNotFoundExceptionTest() {
    // Given
    Long courseId = mockCourse.getId();
    String expectedMessage = String.format("course with id %s was not found", courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseService.deleteCourse(courseId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void deleteCourseNotFoundExceptionTest2() {
    // Given
    Long courseId = null;
    String expectedMessage = String.format("course with id %s was not found", courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseService.deleteCourse(courseId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void enrollStudentTest() {
    // Given
    Long courseId = mockCourse.getId();
    Long studentId = 1L;
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(new Student());
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    Mockito.when(courseRepository.save(Mockito.any(Course.class))).thenReturn(mockCourse);
    fixedClock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant(),
        ZoneId.systemDefault());
    Mockito.doReturn(fixedClock.instant()).when(clock).instant();
    Mockito.doReturn(fixedClock.getZone()).when(clock).getZone();
    // When
    courseService.enrollStudent(courseId, studentId);
    // Then
    Mockito.verify(courseRepository, Mockito.times(1)).save(Mockito.any(Course.class));
  }

  @Test
  void enrollStudentToOptionalCourse() {
    // Given
    Course optionalCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            "DSA",
            3,
            2,
            2,
            true,
            true,
            mockModule,
            mockSemester
        );
    Long courseId = optionalCourse.getId();
    Long studentId = 1L;
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(new Student());
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(optionalCourse));
    Mockito.doNothing().when(courseSubmissionService)
        .addNewCourseSubmission(Mockito.any(CourseSubmission.class));
    fixedClock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant(),
        ZoneId.systemDefault());
    Mockito.doReturn(fixedClock.instant()).when(clock).instant();
    Mockito.doReturn(fixedClock.getZone()).when(clock).getZone();
    // When
    courseService.enrollStudent(courseId, studentId);
    // Then
    Mockito.verify(courseSubmissionService, Mockito.times(1))
        .addNewCourseSubmission(Mockito.any(CourseSubmission.class));
  }

  @Test
  void enrollStudentNotInSubmissionPeriod() {
    // Given
    String expectedMessage =
        String.format("course submission can be created only in submission period (%1$s - %2$s)",
            mockCourse.getSemester().getSubmissionPeriodFrom(),
            mockCourse.getSemester().getSubmissionPeriodTo());
    Long courseId = mockCourse.getId();
    Long studentId = 1L;
    LocalDate dateBeforeSubmissionPeriod = mockSemester.getSubmissionPeriodFrom().minusDays(3);
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(new Student());
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    fixedClock = Clock.fixed(
        dateBeforeSubmissionPeriod.atStartOfDay(ZoneId.systemDefault()).toInstant(),
        ZoneId.systemDefault());
    Mockito.doReturn(fixedClock.instant()).when(clock).instant();
    Mockito.doReturn(fixedClock.getZone()).when(clock).getZone();
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.enrollStudent(courseId, studentId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void enrollStudentNotInSubmissionPeriod2() {
    // Given
    String expectedMessage =
        String.format("course submission can be created only in submission period (%1$s - %2$s)",
            mockCourse.getSemester().getSubmissionPeriodFrom(),
            mockCourse.getSemester().getSubmissionPeriodTo());
    LocalDate dateAfterSubmissionPeriod = mockSemester.getSubmissionPeriodTo().plusDays(3);
    Long courseId = mockCourse.getId();
    Long studentId = 1L;
    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(new Student());
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    fixedClock = Clock.fixed(
        dateAfterSubmissionPeriod.atStartOfDay(ZoneId.systemDefault()).toInstant(),
        ZoneId.systemDefault());
    Mockito.doReturn(fixedClock.instant()).when(clock).instant();
    Mockito.doReturn(fixedClock.getZone()).when(clock).getZone();
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseService.enrollStudent(courseId, studentId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getEnrolledStudentsTest() {
    // Given
    Long courseId = mockCourse.getId();
    Student student = mockObjects.getStudent();
    Set<Student> expectedEnrolledStudents = new HashSet<>(
        Arrays.asList(student));
    mockCourse.enrollStudent(student);
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    // When
    Set<Student> actualEnrolledStudents = courseService.getEnrolledStudents(courseId);
    // Then
    assertEquals(expectedEnrolledStudents, actualEnrolledStudents);
  }

  @Test
  void assignLecturerTest() {
    // Given
    Long courseId = mockCourse.getId();
    Long lecturerId = 1L;
    Mockito.when(lecturerService.getLecturer(Mockito.anyLong())).thenReturn(new Lecturer());
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    Mockito.when(courseRepository.save(Mockito.any(Course.class))).thenReturn(mockCourse);
    // When
    courseService.assignLecturer(courseId, lecturerId);
    // Then
    Mockito.verify(courseRepository, Mockito.times(1)).save(Mockito.any(Course.class));
  }

  @Test
  void getCourseLecturersTest() {
    // Given
    Long courseId = mockCourse.getId();
    Lecturer lecturer = mockObjects.getLecturer();
    Set<Lecturer> expectedLecturers = new HashSet<>(Arrays.asList(lecturer));
    mockCourse.assignLecturer(lecturer);
    Mockito.when(courseRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockCourse));
    // When
    Set<Lecturer> actualLecturers = courseService.getCourseLecturers(courseId);
    // Then
    assertEquals(expectedLecturers, actualLecturers);
  }

  @Test
  void getActiveSubmissions() {
    // Given
    Long courseId = mockCourse.getId();
    List<CourseSubmission> expectedCourseSubmissions =
        Arrays.asList(mockCourseSubmission, mockCourseSubmission, mockCourseSubmission);
    Mockito.when(courseRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourse));
    Mockito.when(courseSubmissionService.getCourseSubmissions(Mockito.any(Course.class)))
        .thenReturn(expectedCourseSubmissions);
    // When
    List<CourseSubmission> actualCourseSubmissions = courseService.getActiveSubmissions(courseId);
    // Then
    assertEquals(expectedCourseSubmissions, actualCourseSubmissions);
  }

  @Test
  void approveCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    Mockito.when(courseSubmissionService.resolveCourseSubmission(Mockito.anyLong(),
        Mockito.any(SubmissionStatus.class))).thenReturn(mockCourseSubmission);
    // When
    courseService.approveCourseSubmission(courseSubmissionId);
    // Then
    Mockito.verify(courseSubmissionService, Mockito.times(1))
        .resolveCourseSubmission(Mockito.anyLong(), Mockito.any(SubmissionStatus.class));
  }

  @Test
  void rejectCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    Mockito.when(courseSubmissionService.resolveCourseSubmission(Mockito.anyLong(),
        Mockito.any(SubmissionStatus.class))).thenReturn(mockCourseSubmission);
    // When
    courseService.rejectCourseSubmission(courseSubmissionId);
    // Then
    Mockito.verify(courseSubmissionService, Mockito.times(1))
        .resolveCourseSubmission(Mockito.anyLong(), Mockito.any(SubmissionStatus.class));
  }

  @Test
  void revokeCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    Mockito.when(courseSubmissionService.resolveCourseSubmission(Mockito.anyLong(),
        Mockito.any(SubmissionStatus.class))).thenReturn(mockCourseSubmission);
    // When
    courseService.revokeCourseSubmission(courseSubmissionId);
    // Then
    Mockito.verify(courseSubmissionService, Mockito.times(1))
        .resolveCourseSubmission(Mockito.anyLong(), Mockito.any(SubmissionStatus.class));
  }

  @Test
  void getCoursesToBeApproved() {
    // Given
    List<Course> expectedCourses = Arrays.asList(mockCourse, mockCourse, mockCourse);
    Mockito.when(courseRepository.findAllByIsApproved(Mockito.anyBoolean())).thenReturn(expectedCourses);
    // When
    List<Course> actualCourses = courseService.getCoursesToBeApproved();
    // Then
    assertEquals(expectedCourses, actualCourses);
  }

  @Test
  void approveCourseCreation() {
    // Given
    Long courseId = mockCourse.getId();
    Mockito.when(courseRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourse));
    // When
    courseService.approveCourseCreation(courseId);
    // Then
    Mockito.verify(courseRepository, Mockito.times(1))
        .findById(Mockito.anyLong());
  }
}