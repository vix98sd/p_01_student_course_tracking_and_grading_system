package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.CourseSubmissionRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

@ExtendWith(MockitoExtension.class)
class CourseSubmissionServiceImplTest {

  @Mock
  private CourseSubmissionRepository courseSubmissionRepository;
  @Mock
  private Clock clock;
  private final Clock defaultClock = Clock.systemDefaultZone();
  @InjectMocks
  private CourseSubmissionServiceImpl courseSubmissionService;

  private final Course mockCourse = mockObjects.getCourse();
  private Student mockStudent = mockObjects.getStudent();
  private final CourseSubmission mockCourseSubmission = mockObjects.getCourseSubmission();

  @Test
  void addNewCourseSubmission() {
    // Given
    Mockito.when(courseSubmissionRepository.save(Mockito.any(CourseSubmission.class)))
        .thenAnswer(i -> i.getArgument(0));
    // When
    courseSubmissionService.addNewCourseSubmission(mockCourseSubmission);
    // Then
    Mockito.verify(courseSubmissionRepository, Mockito.times(1))
        .save(Mockito.any(CourseSubmission.class));
  }

  @Test
  void addNewCourseInvalidArgumentException() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseSubmission noCourseCourseSubmission =
        new CourseSubmission(
            1L,
            mockCourse.getSemester().getStartingDate().minusDays(1),
            mockCourse.getSemester().getStartingDate().minusDays(1),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            mockStudent,
            null
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseSubmissionService.addNewCourseSubmission(noCourseCourseSubmission)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewCourseInvalidArgumentException2() {
    // Given
    String expectedMessage = "all arguments are required";
    CourseSubmission noStudentCourseSubmission =
        new CourseSubmission(
            1L,
            mockCourse.getSemester().getStartingDate().minusDays(1),
            mockCourse.getSemester().getStartingDate().minusDays(1),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            null,
            mockCourse
        );
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        courseSubmissionService.addNewCourseSubmission(noStudentCourseSubmission)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseSubmission() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    CourseSubmission expectedCourseSubmission = mockCourseSubmission;
    Mockito.when(courseSubmissionRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockCourseSubmission));
    // When
    CourseSubmission actualCourseSubmission = courseSubmissionService
        .getCourseSubmission(courseSubmissionId);
    // Then
    assertEquals(expectedCourseSubmission, actualCourseSubmission);
  }

  @Test
  void getCourseSubmissionNoIdException() {
    // Given
    Long courseSubmissionId = null;
    String expectedMessage =
        String.format("course submission with id %s was not found", courseSubmissionId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseSubmissionService.getCourseSubmission(courseSubmissionId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseSubmissionNotFoundException() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    String expectedMessage =
        String.format("course submission with id %s was not found", courseSubmissionId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        courseSubmissionService.getCourseSubmission(courseSubmissionId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getCourseSubmissions() {
    // Given
    Boolean isResolved = false;
    List<CourseSubmission> expectedCourseSubmissions =
        Arrays.asList(mockCourseSubmission, mockCourseSubmission, mockCourseSubmission);
    Mockito.when(courseSubmissionRepository
            .findAllByCourseAndIsResolved(Mockito.any(Course.class), Mockito.anyBoolean()))
        .thenReturn(expectedCourseSubmissions);
    // When
    List<CourseSubmission> actualCourseSubmissions = courseSubmissionService
        .getCourseSubmissions(mockCourse);
    // Then
    assertEquals(expectedCourseSubmissions, actualCourseSubmissions);
  }

  @Nested
  @DisplayName("Tests that needs clock")
  class TestNest {

    @BeforeEach
    void setUpClock() {
      Mockito.doReturn(defaultClock.instant()).when(clock).instant();
      Mockito.doReturn(defaultClock.getZone()).when(clock).getZone();
    }

    @Test
    void resolveCourseSubmissionApprove() {
      // Given
      Long courseSubmissionId = mockCourseSubmission.getId();
      CourseSubmission expectedCourseSubmission = mockCourseSubmission;
      Mockito.when(courseSubmissionRepository.findById(Mockito.anyLong()))
          .thenReturn(java.util.Optional.of(mockCourseSubmission));
      Mockito.when(courseSubmissionRepository.save(Mockito.any(CourseSubmission.class)))
          .thenAnswer(i -> i.getArgument(0));
      // When
      CourseSubmission actualCourseSubmission = courseSubmissionService
          .resolveCourseSubmission(courseSubmissionId, SubmissionStatus.APPROVED);
      // Then
      assertEquals(expectedCourseSubmission, actualCourseSubmission);
    }

    @Test
    void resolveCourseSubmissionReject() {
      // Given
      Long courseSubmissionId = mockCourseSubmission.getId();
      CourseSubmission expectedCourseSubmission = mockCourseSubmission;
      Mockito.when(courseSubmissionRepository.findById(Mockito.anyLong()))
          .thenReturn(java.util.Optional.of(mockCourseSubmission));
      Mockito.when(courseSubmissionRepository.save(Mockito.any(CourseSubmission.class)))
          .thenAnswer(i -> i.getArgument(0));
      // When
      CourseSubmission actualCourseSubmission = courseSubmissionService
          .resolveCourseSubmission(courseSubmissionId, SubmissionStatus.REJECTED);
      // Then
      assertEquals(expectedCourseSubmission, actualCourseSubmission);
    }

    @Test
    void resolveCourseSubmissionRevoke() {
      // Given
      Long courseSubmissionId = mockCourseSubmission.getId();
      CourseSubmission expectedCourseSubmission = mockCourseSubmission;
      Mockito.when(courseSubmissionRepository.findById(Mockito.anyLong()))
          .thenReturn(java.util.Optional.of(mockCourseSubmission));
      Mockito.when(courseSubmissionRepository.save(Mockito.any(CourseSubmission.class)))
          .thenAnswer(i -> i.getArgument(0));
      // When
      CourseSubmission actualCourseSubmission = courseSubmissionService
          .resolveCourseSubmission(courseSubmissionId, SubmissionStatus.REVOKED);
      // Then
      assertEquals(expectedCourseSubmission, actualCourseSubmission);
    }
  }

  @Test
  void resolveCourseSubmissionAlreadyResolvedException() {
    // Given
    Long courseSubmissionId = mockCourseSubmission.getId();
    String expectedMessage =
        String.format("submission with id %s is already resolved", courseSubmissionId);
    CourseSubmission resolvedCourseSubmission =
        new CourseSubmission(
            1L,
            mockCourse.getSemester().getStartingDate().minusDays(1),
            mockCourse.getSemester().getStartingDate().minusDays(1),
            true,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            mockStudent,
            mockCourse
        );
    Mockito.when(courseSubmissionRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(resolvedCourseSubmission));
    // When
    String actualMessage = assertThrows(RecordAlreadyExistException.class, () ->
        courseSubmissionService.resolveCourseSubmission(courseSubmissionId,
            SubmissionStatus.REVOKED)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }
}