package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.repository.LecturerRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class LecturerServiceImplTest {

  @Mock
  private LecturerRepository lecturerRepository;
  @Mock
  private PasswordEncoder passwordEncoder;
  @Mock
  private Clock clock;
  private final Clock defaultClock = Clock.systemDefaultZone();
  @InjectMocks
  private LecturerServiceImpl lecturerService;

  private Lecturer mockLecturer = mockObjects.getLecturer();


  @Test
  void addNewLecturerExceptionTest() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noFirstNameLecturer = mockObjects.getLecturer();
    noFirstNameLecturer.setFirstName(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noFirstNameLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest2() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noLastNameLecturer = mockObjects.getLecturer();
    noLastNameLecturer.setLastName(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noLastNameLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest3() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noPhoneLecturer = mockObjects.getLecturer();
    noPhoneLecturer.setPhoneNumber(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noPhoneLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest4() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noDoBLecturer = mockObjects.getLecturer();
    noDoBLecturer.setDateOfBirth(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noDoBLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest5() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noDegreeLecturer = mockObjects.getLecturer();
    noDegreeLecturer.setDegree(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noDegreeLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest6() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noVocationLecturer = mockObjects.getLecturer();
    noVocationLecturer.setVocation(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noVocationLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest7() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noUsernameLecturer = mockObjects.getLecturer();
    noUsernameLecturer.setUsername(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noUsernameLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addNewLecturerExceptionTest8() {
    // Given
    String expectedMessage = "all parameters are required";
    Lecturer noPasswordLecturer = mockObjects.getLecturer();
    noPasswordLecturer.setPassword(null);
    Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        lecturerService.createNewLecturer(noPasswordLecturer)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAllLecturersTest() {
    // Given
    List<Lecturer> expectedLecturers = Arrays.asList(mockLecturer, mockLecturer, mockLecturer);
    Mockito.when(lecturerRepository.findAll()).thenReturn(expectedLecturers);
    // When
    List<Lecturer> actualLecturers = lecturerService.getAllLecturers();
    // Then
    assertEquals(expectedLecturers, actualLecturers);
  }

  @Test
  void getLecturerByIdTest() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Lecturer expectedLecturer = mockLecturer;
    Mockito.when(lecturerRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockLecturer));
    // When
    Lecturer actualLecturer = lecturerService.getLecturer(lecturerId);
    // Then
    assertEquals(expectedLecturer, actualLecturer);
  }

  @Test
  void getLecturerByIdExceptionTest() {
    // Given
    Long lecturerId = mockLecturer.getId();
    String expectedMessage = String.format("lecturer with id %s was not found", lecturerId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        lecturerService.getLecturer(lecturerId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getLecturerByIdExceptionTest2() {
    // Given
    Long lecturerId = null;
    String expectedMessage = String.format("lecturer with id %s was not found", lecturerId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        lecturerService.getLecturer(lecturerId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getLecturerByUsername() {
    // Given
    String lecturerUsername = mockLecturer.getUsername();
    Lecturer expectedLecturer = mockLecturer;
    Mockito.when(lecturerRepository.findByUsername(Mockito.anyString()))
        .thenReturn(java.util.Optional.of(mockLecturer));
    // When
    Lecturer actualLecturer = lecturerService.getLecturer(lecturerUsername);
    // Then
    assertEquals(expectedLecturer, actualLecturer);
  }

  @Test
  void getLecturerByUsernameException() {
    // Given
    String lecturerUsername = null;
    String expectedMessage =
        String.format("lecturer with username %s was not found", lecturerUsername);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        lecturerService.getLecturer(lecturerUsername)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getLecturerByUsernameException2() {
    // Given
    String lecturerUsername = mockLecturer.getUsername();
    String expectedMessage =
        String.format("lecturer with username %s was not found", lecturerUsername);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        lecturerService.getLecturer(lecturerUsername)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Nested
  @DisplayName("Tests that needs clock")
  class NestedTests {

    @BeforeEach
    void setUpClock() {
      Mockito.doReturn(defaultClock.instant()).when(clock).instant();
      Mockito.doReturn(defaultClock.getZone()).when(clock).getZone();
    }

    @Test
    void addNewLecturerTest() {
      // Given
      Lecturer expectedLecturer = mockLecturer;
      Mockito.when(lecturerRepository.save(Mockito.any(Lecturer.class))).thenReturn(mockLecturer);
      Mockito.when(passwordEncoder.encode(Mockito.anyString()))
          .thenAnswer(i -> i.getArgument(0));
      // When
      Lecturer actualLecturer = lecturerService.createNewLecturer(mockLecturer);
      // Then
      assertEquals(expectedLecturer, actualLecturer);
    }

    @Test
    void updateLecturerInfoTest() {
      // Given
      Long lecturerId = mockLecturer.getId();
      Lecturer expectedLecturer = mockObjects.getLecturer();
      expectedLecturer.setFirstName("Spasoje");
      expectedLecturer.setLastName("Vostic");
      expectedLecturer.setPhoneNumber("+381697654321");
      expectedLecturer.setDateOfBirth(LocalDate.of(2021, Month.AUGUST, 24));
      expectedLecturer.setDegree("MSc");
      expectedLecturer.setVocation("Professor assistant");
      expectedLecturer.setDateOfPromotion(LocalDate.now());
      expectedLecturer.setUsername("username_nes");
      expectedLecturer.setPassword("password_new");
      Mockito.when(lecturerRepository.findById(Mockito.anyLong())).thenReturn(
          java.util.Optional.of(mockLecturer));
      // When
      Lecturer actualLecturer = lecturerService.updateLecturerInfo(lecturerId, expectedLecturer);
      // Then
      assertEquals(expectedLecturer, actualLecturer);
    }
  }

  @Test
  void deleteLecturerTest() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Mockito.when(lecturerRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.ofNullable(mockLecturer));
    Mockito.doNothing().when(lecturerRepository).delete(Mockito.any(Lecturer.class));
    // When
    lecturerService.deleteLecturer(lecturerId);
    // Then
    Mockito.verify(lecturerRepository, Mockito.times(1))
        .delete(Mockito.any(Lecturer.class));
  }

  @Test
  void getLecturerCoursesTest() {
    // Given
    Long lecturerId = mockLecturer.getId();
    Course course = mockObjects.getCourse();
    Set<Course> expectedCourses = new HashSet<>(Arrays.asList(course, course, course));
    Lecturer lecturerWithCourses = mockObjects.getLecturerWithCourse();
    Mockito.when(lecturerRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(lecturerWithCourses));
    // When
    Set<Course> actualCourses = lecturerService.getLecturerCourses(lecturerId);
    // Then
    assertEquals(expectedCourses, actualCourses);
  }
}