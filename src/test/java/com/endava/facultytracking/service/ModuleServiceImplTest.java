package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.repository.ModuleRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class ModuleServiceImplTest {

  @MockBean
  private ModuleRepository moduleRepository;

  private final Module mockModule = new Module(1L, "Information Technologies", "IT", 3);
  private ModuleService moduleService;

  @BeforeEach
  void setup() {
    moduleService = new ModuleServiceImpl(moduleRepository);
  }

  @Test
  void createModuleTest() {
    // Given
    Mockito.when(moduleRepository.save(Mockito.any(Module.class))).thenReturn(mockModule);
    // When
    Module actualModule = moduleService.createModule(mockModule);
    // Then
    assertEquals(mockModule, actualModule);
  }

  @Test
  void createModuleExceptionTest() {
    // Given
    String expectedMessage = "all parameters are required";
    Module nullNameModule = new Module(null, mockModule.getCodeName(), mockModule.getGrade());
    // When
    Exception exception = assertThrows(IllegalStateException.class, () ->
        moduleService
            .createModule(nullNameModule)
    );
    String actualMessage = exception.getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createModuleExceptionTest2() {
    // Given
    String expectedMessage = "all parameters are required";
    Module nullCodeNameModule = new Module(mockModule.getName(), null, mockModule.getGrade());
    // When
    Exception exception = assertThrows(IllegalStateException.class, () ->
        moduleService
            .createModule(nullCodeNameModule)
    );
    String actualMessage = exception.getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createModuleExceptionTest3() {
    // Given
    String expectedMessage = "all parameters are required";
    Module nullGradeModule = new Module(mockModule.getName(), mockModule.getCodeName(), null);
    // When
    Exception exception = assertThrows(IllegalStateException.class, () ->
        moduleService
            .createModule(nullGradeModule)
    );
    String actualMessage = exception.getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewModuleCodeNameExceptionTest() {
    // Given
    String codeName = mockModule.getCodeName();
    Mockito.when(moduleRepository.existsByCodeName(Mockito.anyString())).thenReturn(true);
    String expectedMessage = String.format("module with code name %s already exists", codeName);
    // When
    String actualMessage = assertThrows(RecordAlreadyExistException.class, () ->
        moduleService.createModule(mockModule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAllModulesTest() {
    // Given
    List<Module> expectedModules = Arrays.asList(mockModule, mockModule, mockModule);
    Mockito.when(moduleRepository.findAll()).thenReturn(expectedModules);
    // When
    List<Module> actualModules = moduleService.getAllModules();
    // Then
    assertEquals(expectedModules, actualModules);
  }

  @Test
  void getModuleTest() {
    // Given
    Long moduleId = mockModule.getId();
    Mockito.when(moduleRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockModule));
    // When
    Module actualModule = moduleService.getModule(moduleId);
    // Then
    assertEquals(mockModule, actualModule);
  }

  @Test
  void getModuleExceptionTest() {
    // Given
    Long moduleId = mockModule.getId();
    String expectedMessage = String.format("module with id %s does not exist", moduleId);
    // When
    Exception exception = assertThrows(RecordNotFoundException.class, () ->
        moduleService.getModule(moduleId));
    // Then
    assertEquals(expectedMessage, exception.getMessage());
  }

  @Test
  void getModuleExceptionTest2() {
    // Given
    Long moduleId = null;
    String expectedMessage = String.format("module with id %s does not exist", moduleId);
    // When
    Exception exception = assertThrows(RecordNotFoundException.class, () ->
        moduleService.getModule(moduleId));
    // Then
    assertEquals(expectedMessage, exception.getMessage());
  }

  @Test
  void updateModuleInfoTest() {
    // Given
    Long moduleId = mockModule.getId();
    Mockito.when(moduleRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(mockModule));
    Module expectedUpdatedModuleInfo = new Module(1L, "Computer Sciences", "CS", 4);
    // When
    Module actualUpdatedModuleInfo = moduleService.updateModuleInfo(moduleId,
        expectedUpdatedModuleInfo);
    // Then
    assertEquals(expectedUpdatedModuleInfo, actualUpdatedModuleInfo);
  }

  @Test
  void updateModuleInfoExceptionTest() {
    // Given
    Long moduleId = mockModule.getId();
    String expectedMessage = String.format("module with id %s does not exist", moduleId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        moduleService.updateModuleInfo(moduleId, mockModule)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void deleteModuleTest() {
    // Given
    Long moduleId = mockModule.getId();
    Mockito.when(moduleRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(mockModule));
    Mockito.doNothing().when(moduleRepository).deleteById(Mockito.anyLong());
    String expectedResult = "Module deleted";
    // When
    String actualResult = moduleService.deleteModule(moduleId);
    // Then
    assertEquals(expectedResult, actualResult);
  }

  @Test
  void deleteModuleExceptionTest() {
    // Given
    Long moduleId = mockModule.getId();
    Mockito.when(moduleRepository.existsById(Mockito.anyLong())).thenReturn(false);
    String expectedMessage = String.format("module with id %s does not exist", moduleId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        moduleService.deleteModule(moduleId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }
}