package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.InvalidArgumentException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.repository.RoleRepository;
import com.endava.facultytracking.utility.mockObjects;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTest {

  @Mock
  private RoleRepository roleRepository;
  @InjectMocks
  private RoleServiceImpl roleService;

  private final Role mockRole = mockObjects.getRoleAdmin();

  @Test
  void saveRole() {
    // Given
    Mockito.when(roleRepository.save(Mockito.any(Role.class)))
        .thenReturn(mockRole);
    // When
    roleService.saveRole(mockRole);
    // Then
    Mockito.verify(roleRepository, Mockito.times(1))
        .save(Mockito.any(Role.class));
  }

  @Test
  void saveRoleException() {
    // Given
    Role noNameRole = mockObjects.getRoleAdmin();
    noNameRole.setName(null);
    String expectedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(InvalidArgumentException.class, () ->
        roleService.saveRole(noNameRole)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getRole() {
    // Given
    String roleName = mockRole.getName();
    Role expectedRole = mockObjects.getRoleAdmin();
    Mockito.when(roleRepository.findRoleByName(Mockito.anyString()))
        .thenReturn(java.util.Optional.of(mockRole));
    // When
    Role actualRole = roleService.getRole(roleName);
    // Then
    assertEquals(expectedRole, actualRole);
  }

  @Test
  void getRoleNoNameException() {
    // Given
    String roleName = null;
    String expectedMessage = String.format("role with name %s was not found", roleName);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        roleService.getRole(roleName)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getRoleNotFoundException() {
    // Given
    String roleName = mockRole.getName();
    String expectedMessage = String.format("role with name %s was not found", roleName);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        roleService.getRole(roleName)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }
}