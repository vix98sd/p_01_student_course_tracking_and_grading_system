package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.repository.SemesterRepository;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SemesterServiceImplTest {

  @MockBean
  private SemesterRepository semesterRepository;

  private final Semester mockSemester = new Semester(LocalDate.of(2021, Month.MARCH, 1));
  private SemesterService semesterService;

  @BeforeEach
  void setup() {
    semesterService = new SemesterServiceImpl(semesterRepository);
  }

  @Test
  void createSemesterTest() {
    // Given
    Semester semester = new Semester();
    Mockito.when(semesterRepository.save(Mockito.any(Semester.class))).thenReturn(mockSemester);
    // When
    Semester actualSemester = semesterService.createSemester(mockSemester);
    // Then
    assertEquals(mockSemester, actualSemester);
  }

  @Test
  void createSemesterExceptionTest() {
    // Given
    String exceptedMessage = "starting date is required";
    Mockito.when(semesterRepository.save(Mockito.any(Semester.class))).thenReturn(mockSemester);
    Semester noStartingDateSemester = new Semester();
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        semesterService.createSemester(noStartingDateSemester)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void getAllSemestersTest() {
    // Given
    List<Semester> expectedSemesters = Arrays.asList(mockSemester, mockSemester, mockSemester);
    Mockito.when(semesterRepository.findAll()).thenReturn(expectedSemesters);
    // When
    List<Semester> actualSemesters = semesterService.getAllSemesters();
    // Then
    assertEquals(expectedSemesters, actualSemesters);
  }

  @Test
  void getSemesterTest() {
    // Given
    Long semesterId = 1L;
    Mockito.when(semesterRepository.findById(Mockito.anyLong()))
        .thenReturn(java.util.Optional.of(mockSemester));
    // When
    Semester actualSemester = semesterService.getSemester(semesterId);
    // Then
    assertEquals(mockSemester, actualSemester);
  }

  @Test
  void getSemesterExceptionTest() {
    // Given
    Long semesterId = 1L;
    String expectedMessage = String.format("semester with id %s does not exist", semesterId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        semesterService.getSemester(semesterId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void updateSemesterInfoTest() {
    // Given
    Long semesterId = 1L;
    Semester expectedUpdatedSemesterInfo = new Semester(LocalDate.of(2022, Month.OCTOBER, 05));
    Mockito.when(semesterRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockSemester));
    // When
    Semester actualUpdatedSemesterInfo = semesterService
        .updateSemesterInfo(semesterId, expectedUpdatedSemesterInfo);
    // Then
    assertEquals(expectedUpdatedSemesterInfo, actualUpdatedSemesterInfo);
  }

  @Test
  void updateSemesterInfoExceptionTest() {
    // Given
    Long semesterId = 1L;
    String expectedMessage = String.format("semester with id %s does not exist", semesterId);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        semesterService.updateSemesterInfo(semesterId, mockSemester)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void deleteSemesterTest() {
    // Given
    Long semesterId = 1L;
    Mockito.when(semesterRepository.existsById(Mockito.anyLong())).thenReturn(true);
    Mockito.doNothing().when(semesterRepository).deleteById(Mockito.anyLong());
    String expectedMessage = "Semester deleted";
    // When
    String actualMessage = semesterService.deleteSemester(semesterId);
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void deleteSemesterExceptionTest() {
    // Given
    Long semesterId = 1L;
    Mockito.when(semesterRepository.existsById(Mockito.anyLong())).thenReturn(false);
    String expectedMessage = String.format("semester with id %s does not exist", semesterId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        semesterService.deleteSemester(semesterId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }
}