package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordAlreadyExistException;
import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.repository.StudentRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class StudentServiceImplTest {

  @Mock
  private StudentRepository studentRepository;
  @Mock
  private ModuleService moduleService;
  @Mock
  private PasswordEncoder passwordEncoder;
  @Mock
  private Clock clock;
  private final Clock defaultClock = Clock.systemDefaultZone();
  @InjectMocks
  private StudentServiceImpl studentService;

  private final Student mockStudent = mockObjects.getStudent();

  @Nested
  @DisplayName("Tests that needs clock")
  class NestedTests {

    @BeforeEach
    void setUpClock() {
      Mockito.doReturn(defaultClock.instant()).when(clock).instant();
      Mockito.doReturn(defaultClock.getZone()).when(clock).getZone();
    }

    @Test
    void createNewStudentTest() {
      // Given
      Mockito.when(studentRepository.save(Mockito.any(Student.class))).thenReturn(mockStudent);
      Mockito.when(passwordEncoder.encode(Mockito.anyString()))
          .thenAnswer(i -> i.getArgument(0));
      // When
      Student actualStudent = studentService.createNewStudent(mockStudent);
      // Then
      assertEquals(mockStudent, actualStudent);
    }
  }

  @Test
  void createNewStudentExceptionTest() {
    // Given
    String expectedMessage = "all parameters are required";
    Student noFirstNameStudent = mockObjects.getStudent();
    noFirstNameStudent.setFirstName(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentService.createNewStudent(noFirstNameStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, expectedMessage);
  }

  @Test
  void createNewStudentExceptionTest2() {
    // Given
    String expectedMessage = "all parameters are required";
    Student noLastNameStudent = mockObjects.getStudent();
    noLastNameStudent.setLastName(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentService.createNewStudent(noLastNameStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, expectedMessage);
  }

  @Test
  void createNewStudentExceptionTest3() {
    // Given
    String expectedMessage = "all parameters are required";
    Student noIndexNumberStudent = mockObjects.getStudent();
    noIndexNumberStudent.setIndexNumber(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentService.createNewStudent(noIndexNumberStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, expectedMessage);
  }

  @Test
  void createNewStudentExceptionTest4() {
    // Given
    String expectedMessage = "all parameters are required";
    Student noDoBStudent = mockObjects.getStudent();
    noDoBStudent.setDateOfBirth(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentService.createNewStudent(noDoBStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, expectedMessage);
  }

  @Test
  void createNewStudentExceptionTest5() {
    // Given
    String expectedMessage = "all parameters are required";
    Student noUsernameStudent = mockObjects.getStudent();
    noUsernameStudent.setUsername(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentService.createNewStudent(noUsernameStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewStudentExceptionTest6() {
    // Given
    String expectedMessage = "all parameters are required";
    Student noPasswordStudent = mockObjects.getStudent();
    noPasswordStudent.setPassword(null);
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentService.createNewStudent(noPasswordStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void createNewStudentUsernameExceptionTest() {
    // Given
    String username = mockStudent.getUsername();
    Mockito.when(studentRepository.existsByUsername(Mockito.anyString())).thenReturn(true);
    String expectedMessage = String.format("username %s is already taken", username);
    // When
    String actualMessage = assertThrows(RecordAlreadyExistException.class, () ->
        studentService.createNewStudent(mockStudent)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getAllStudentsTest() {
    // Given
    List<Student> expectedStudents = Arrays.asList(mockStudent, mockStudent, mockStudent);
    Mockito.when(studentRepository.findAll()).thenReturn(expectedStudents);
    // When
    List<Student> actualStudents = studentService.getAllStudents();
    // Then
    assertEquals(expectedStudents, actualStudents);
  }

  @Test
  void getStudentByIdTest() {
    // Given
    Student expectedStudent = mockObjects.getStudent();
    Long studentId = mockStudent.getId();
    Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(mockStudent));
    // When
    Student actualStudent = studentService.getStudent(studentId);
    // Then
    assertEquals(expectedStudent, actualStudent);
  }

  @Test
  void getStudentByIdExceptionTest() {
    // Given
    Long studentId = mockStudent.getId();
    String expectedMessage = String.format("student with id %s was not found", studentId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentService.getStudent(studentId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getStudentByIdExceptionTest2() {
    // Given
    Long studentId = null;
    String expectedMessage = String.format("student with id %s was not found", studentId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentService.getStudent(studentId)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getStudentByUsername() {
    // Given
    String studentUsername = mockStudent.getUsername();
    Student expectedStudent = mockObjects.getStudent();
    Mockito.when(studentRepository.findByUsername(Mockito.anyString()))
        .thenReturn(java.util.Optional.of(mockStudent));
    // When
    Student actualStudent = studentService.getStudent(studentUsername);
    // Then
    assertEquals(expectedStudent, actualStudent);
  }

  @Test
  void getStudentByUsernameException(){
    // Given
    String studentUsername = null;
    String expectedMessage = String.format("student with username %s was not found", studentUsername);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentService.getStudent(studentUsername)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void getStudentByUsernameException2(){
    // Given
    String studentUsername = mockStudent.getUsername();
    String expectedMessage = String.format("student with username %s was not found", studentUsername);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentService.getStudent(studentUsername)).getMessage();
    // Then
    assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void updateStudentTest() {
    // Given
    Long studentId = mockStudent.getId();
    Student expectedStudent = mockObjects.getStudent();
    expectedStudent.setIndexNumber("5/2017");
    expectedStudent.setDateOfBirth(LocalDate.of(1999, Month.JUNE, 6));
    expectedStudent.setDateOfEntry(LocalDate.of(2015, Month.JUNE, 11));
    expectedStudent.setGrade(3);
    expectedStudent.setUsername("username_new");
    expectedStudent.setPassword("password_new");
    Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.ofNullable(mockStudent));
    // When
    Student actualStudent = studentService.updateStudent(studentId, expectedStudent);
    // Then
    assertEquals(expectedStudent, actualStudent);
  }

  @Test
  void deleteStudentTest() {
    // Given
    Long studentId = mockStudent.getId();
    Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.ofNullable(mockStudent));
    // When
    studentService.deleteStudent(studentId);
    // Then
    assertTrue(true);
  }

  @Test
  void getEnrolledCoursesTest() {
    // Given
    Long studentId = mockStudent.getId();
    Course course = mockObjects.getCourse();
    Set<Course> expectedCourses = new HashSet<Course>(Arrays.asList(course, course, course));
    Student studentWithCourses = mockObjects.getStudentWithCourses();
    Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.of(studentWithCourses));
    // When
    Set<Course> actualCourses = studentService.getEnrolledCourses(studentId);
    // Then
    assertEquals(expectedCourses, actualCourses);
  }

  @Test
  void assignModuleTest() {
    // Given
    Long studentId = mockStudent.getId();
    Long moduleId = 1L;
    Module mockModule = new Module(moduleId, "Information Technologies", "IT", 3);
    Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.ofNullable(mockStudent));
    Mockito.when(moduleService.getModule(Mockito.anyLong())).thenReturn(mockModule);
    // When
    studentService.assignModule(studentId, moduleId);
    // Then
    Mockito.verify(studentRepository, Mockito.times(1)).findById(Mockito.anyLong());
    Mockito.verify(moduleService, Mockito.times(1)).getModule(Mockito.anyLong());
  }

  @Test
  void getAssignedModulesTest() {
    // Given
    Long studentId = mockStudent.getId();
    Long moduleId = 1L;
    Module mockModule = new Module(moduleId, "Information Technologies", "IT", 3);
    Set<Module> expectedModules = new HashSet<>();
    expectedModules.add(mockModule);
    expectedModules.add(mockModule);
    expectedModules.add(mockModule);

    mockStudent.assignNewModule(mockModule);
    mockStudent.assignNewModule(mockModule);
    mockStudent.assignNewModule(mockModule);
    Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(
        java.util.Optional.ofNullable(mockStudent));
    // When
    Set<Module> actualModules = studentService.getAssignedModules(studentId);
    // Then
    assertEquals(expectedModules, actualModules);
  }
}