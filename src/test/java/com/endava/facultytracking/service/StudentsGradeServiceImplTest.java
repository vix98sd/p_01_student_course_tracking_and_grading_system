package com.endava.facultytracking.service;

import static org.junit.jupiter.api.Assertions.*;

import com.endava.facultytracking.domain.exception.RecordNotFoundException;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.domain.model.StudentsGrade;
import com.endava.facultytracking.repository.StudentsGradeRepository;
import com.endava.facultytracking.utility.mockObjects;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@RequiredArgsConstructor
class StudentsGradeServiceImplTest {

  @Mock
  private StudentsGradeRepository studentsGradeRepository;
  @Mock
  private StudentService studentService;
  @Mock
  private CourseService courseService;
  @InjectMocks
  private StudentsGradeServiceImpl studentsGradeService;

  private final Student mockStudent = mockObjects.getStudent();
  private final Course mockCourse = mockObjects.getCourse();
  private final StudentsGrade mockStudentsGrade = mockObjects.getStudentsGrade();
  private final Map<String, Number> expectedMap = new HashMap<>();

  @BeforeEach
  private void setUp() {
    expectedMap.put("studentsGradeId", mockStudentsGrade.getId());
    expectedMap.put("studentId", mockStudentsGrade.getStudent().getId());
    expectedMap.put("courseId", mockStudentsGrade.getCourse().getId());
    expectedMap.put("points", mockStudentsGrade.getPoints());
    expectedMap.put("grade", mockStudentsGrade.getGrade());
  }

  @Test
  void addNewStudentsGradeTest() {
//    // Given
//    Mockito.when(studentsGradeRepository.save(Mockito.any(StudentsGrade.class)))
//        .thenReturn(mockStudentsGrade);
//    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(mockStudent);
//    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
//    expectedMap.put("studentsGradeId", null);
//    // When
////    Map<String, Number> actualMap = studentsGradeService.addNewStudentsGrade(mockStudentsGrade);
//    StudentsGrade actualMap = studentsGradeService.addNewStudentsGrade(mockStudentsGrade);
//    assertTrue(false);
//    // Then
//    assertEquals(expectedMap, actualMap);
  }

  @Test
  void addNewStudentsGradeTest2() {
//    // Given
//    Long studentId = mockStudentsGrade.getStudent().getId();
//    Long courseId = mockStudentsGrade.getCourse().getId();
//    Mockito.when(studentsGradeRepository.save(Mockito.any(StudentsGrade.class)))
//        .thenReturn(mockStudentsGrade);
//    Mockito.when(studentsGradeRepository
//            .findAllByStudentIdAndCourseIdAndIsAnnulled(studentId, courseId, false))
//        .thenReturn(java.util.Optional.of(mockStudentsGrade));
//    Mockito.when(studentService.getStudent(Mockito.anyLong())).thenReturn(mockStudent);
//    Mockito.when(courseService.getCourse(Mockito.anyLong())).thenReturn(mockCourse);
//    expectedMap.put("studentsGradeId", null);
//    // When
////    Map<String, Number> actualMap = studentsGradeService.addNewStudentsGrade(mockStudentsGrade);
//    StudentsGrade actualMap = studentsGradeService.addNewStudentsGrade(mockStudentsGrade);
//    assertTrue(false);
//    // Then
//    assertEquals(expectedMap, actualMap);
  }

  @Test
  void addNewStudentsGradeExceptionTest() {
    // Given
    Student student = mockObjects.getStudent();
    student.setId(null);
    StudentsGrade noStudentStudentsGrade =
        new StudentsGrade(
            1L,
            student,
            mockCourse,
            10,
            100,
            false
        );
    String exceptedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentsGradeService.addNewStudentsGrade(noStudentStudentsGrade)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void addNewStudentsGradeExceptionTest2() {
    // Given
    Course course = mockObjects.getCourse();
    course.setId(null);
    StudentsGrade noCourseStudentsGrade =
        new StudentsGrade(
            1L,
            mockStudent,
            course,
            10,
            100,
            false
        );
    String exceptedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentsGradeService.addNewStudentsGrade(noCourseStudentsGrade)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void addNewStudentsGradeExceptionTest3() {
    // Given
    StudentsGrade noPointsStudentsGrade =
        new StudentsGrade(
            1L,
            mockStudent,
            mockCourse,
            null,
            100,
            false
        );
    String exceptedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentsGradeService.addNewStudentsGrade(noPointsStudentsGrade)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void addNewStudentsGradeExceptionTest4() {
    // Given
    StudentsGrade noGradeStudentsGrade =
        new StudentsGrade(
            1L,
            mockStudent,
            mockCourse,
            10,
            null,
            false
        );
    String exceptedMessage = "all arguments are required";
    // When
    String actualMessage = assertThrows(IllegalStateException.class, () ->
        studentsGradeService.addNewStudentsGrade(noGradeStudentsGrade)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void getAllStudentsGradesTest() {
    // Given
    Long studentId = mockStudentsGrade.getStudent().getId();
    List<StudentsGrade> studentsGradesList = Arrays.asList(mockStudentsGrade, mockStudentsGrade,
        mockStudentsGrade);
    List<Map<String, Number>> expectedStudentsGradesMap = Arrays.asList(expectedMap, expectedMap,
        expectedMap);
    Mockito.when(studentsGradeRepository.findAllByStudentIdAndIsAnnulled(Mockito.anyLong(),
        Mockito.anyBoolean())).thenReturn(studentsGradesList);
    // When
    List<Map<String, Number>> actualStudentsGradesMap = studentsGradeService.getAllStudentsGrades(
        studentId);
    // Then
    assertEquals(expectedStudentsGradesMap, actualStudentsGradesMap);
  }

  @Test
  void getAllStudentsGradesExceptionTest() {
    // Given
    Long courseId = mockStudentsGrade.getCourse().getId();
    Mockito.when(studentsGradeRepository.findAllByStudentIdAndIsAnnulled(Mockito.anyLong(),
        Mockito.anyBoolean())).thenReturn(new ArrayList<>());

    String exceptedMessage = String.format("course with id %s does not have any grades", courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentsGradeService.getAllGradesByCourse(courseId)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void getAllGradesByCourseTest() {
    // Given
    Long courseId = mockStudentsGrade.getCourse().getId();
    List<StudentsGrade> studentsGradesList = Arrays.asList(mockStudentsGrade, mockStudentsGrade,
        mockStudentsGrade);
    List<Map<String, Number>> expectedStudentsGradesMap = Arrays.asList(expectedMap, expectedMap,
        expectedMap);
    Mockito.when(studentsGradeRepository.findAllByCourseIdAndIsAnnulled(Mockito.anyLong(),
        Mockito.anyBoolean())).thenReturn(studentsGradesList);
    // When
    List<Map<String, Number>> actualStudentsGradesMap = studentsGradeService.getAllGradesByCourse(
        courseId);
    // Then
    assertEquals(expectedStudentsGradesMap, actualStudentsGradesMap);
  }

  @Test
  void getAllGradesByCourseExceptionTest() {
    // Given
    Long studentId = mockStudentsGrade.getStudent().getId();
    Mockito.when(studentsGradeRepository.findAllByStudentIdAndIsAnnulled(Mockito.anyLong(),
        Mockito.anyBoolean())).thenReturn(new ArrayList<>());

    String exceptedMessage = String.format("student with id %s does not have any grades",
        studentId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentsGradeService.getAllStudentsGrades(studentId)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void getStudentsGradeByCourseTest() {
    // Given
    Long studentId = mockStudentsGrade.getStudent().getId();
    Long courseId = mockStudentsGrade.getCourse().getId();
    Mockito.when(
        studentsGradeRepository.findAllByStudentIdAndCourseIdAndIsAnnulled(Mockito.anyLong(),
            Mockito.anyLong(),
            Mockito.anyBoolean())).thenReturn(java.util.Optional.of(mockStudentsGrade));
    // When
    StudentsGrade actualStudentsGrade = studentsGradeService.getStudentsGradeByCourse(studentId,
        courseId);
    // Then
    assertEquals(mockStudentsGrade, actualStudentsGrade);
  }

  @Test
  void getStudentsGradeByCourseExceptionTest() {
    // Given
    Long studentId = mockStudentsGrade.getStudent().getId();
    Long courseId = null;
    String exceptedMessage = String.format(
        "student with id %1$s does not have any grades on course with id %2$s", studentId,
        courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentsGradeService.getStudentsGradeByCourse(studentId, courseId)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void getStudentsGradeByCourseExceptionTest2() {
    // Given
    Long studentId = null;
    Long courseId = mockStudentsGrade.getCourse().getId();
    String exceptedMessage = String.format(
        "student with id %1$s does not have any grades on course with id %2$s", studentId,
        courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentsGradeService.getStudentsGradeByCourse(studentId, courseId)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void getStudentsGradeByCourseExceptionTest3() {
    // Given
    Long studentId = mockStudentsGrade.getStudent().getId();
    Long courseId = mockStudentsGrade.getCourse().getId();

    String exceptedMessage = String.format(
        "student with id %1$s does not have any grades on course with id %2$s", studentId,
        courseId);
    // When
    String actualMessage = assertThrows(RecordNotFoundException.class, () ->
        studentsGradeService.getStudentsGradeByCourse(studentId, courseId)).getMessage();
    // Then
    assertEquals(exceptedMessage, actualMessage);
  }

  @Test
  void annulGradeTest() {
    // Given
    Mockito.when(
        studentsGradeRepository.findAllByStudentIdAndCourseIdAndIsAnnulled(Mockito.anyLong(),
            Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(
        java.util.Optional.of(mockStudentsGrade));
    Mockito.when(studentsGradeRepository.save(Mockito.any(StudentsGrade.class)))
        .thenReturn(mockStudentsGrade);
    // When
    studentsGradeService.annulGrade(mockStudentsGrade);
    // Then
    Mockito.verify(studentsGradeRepository, Mockito.times(1))
        .save(Mockito.any(StudentsGrade.class));
  }
}