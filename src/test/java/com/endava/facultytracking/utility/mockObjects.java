package com.endava.facultytracking.utility;

import com.endava.facultytracking.domain.enums.CourseExamResultStatus;
import com.endava.facultytracking.domain.enums.SubmissionStatus;
import com.endava.facultytracking.domain.model.Administrator;
import com.endava.facultytracking.domain.model.Course;
import com.endava.facultytracking.domain.model.CourseAssignment;
import com.endava.facultytracking.domain.model.CourseAssignmentResult;
import com.endava.facultytracking.domain.model.CourseExam;
import com.endava.facultytracking.domain.model.CourseExamResult;
import com.endava.facultytracking.domain.model.CourseExamSubmission;
import com.endava.facultytracking.domain.model.CourseSchedule;
import com.endava.facultytracking.domain.model.CourseSubmission;
import com.endava.facultytracking.domain.model.Lecturer;
import com.endava.facultytracking.domain.model.Module;
import com.endava.facultytracking.domain.model.Role;
import com.endava.facultytracking.domain.model.Semester;
import com.endava.facultytracking.domain.model.Student;
import com.endava.facultytracking.domain.model.StudentsGrade;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class mockObjects {

  private static Module mockModule = new Module();
  private static Semester mockSemester = new Semester();
  private static Course mockCourse = new Course();
  private static Role mockRole = new Role();
  private static Administrator mockAdministrator = new Administrator();
  private static Lecturer mockLecturer = new Lecturer();
  private static Student mockStudent = new Student();
  private static StudentsGrade mockStudentsGrade = new StudentsGrade();
  private static CourseSubmission mockCourseSubmission = new CourseSubmission();
  private static CourseExam mockCourseExam = new CourseExam();
  private static CourseExamResult mockCourseExamResult = new CourseExamResult();
  private static CourseExamSubmission mockCourseExamSubmission = new CourseExamSubmission();
  private static CourseSchedule mockCourseSchedule = new CourseSchedule();
  private static CourseAssignment mockCourseAssignment = new CourseAssignment();
  private static CourseAssignmentResult mockCourseAssignmentResult = new CourseAssignmentResult();

  private static void prepareObjects() {
    mockModule =
        new Module(
            1L,
            "Information Technologies",
            "IT",
            3
        );
    mockSemester =
        new Semester(LocalDate.of(2021, Month.MARCH, 1));
    mockCourse =
        new Course(
            1L,
            "Data Structures and Algorithms",
            "DSA",
            3,
            2,
            2,
            false,
            true,
            mockModule,
            mockSemester
        );
    mockAdministrator =
        new Administrator(
            1L,
            "FirstName",
            "LastName",
            "username_admin",
            "notStrongPassword",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        );
    mockRole =
        new Role(1L, "ROLE_STUDENT");
    mockLecturer =
        new Lecturer(
            1L,
            "Stefan",
            "Marjanovic",
            "+381691234567",
            LocalDate.of(1998, Month.JUNE, 05),
            LocalDate.of(2021, Month.AUGUST, 24),
            "Dr",
            "Professor",
            LocalDate.of(2021, Month.AUGUST, 24),
            "username_lecturer",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>()
        );
    mockStudent =
        new Student(
            1L,
            "Stefan",
            "Marjanovic",
            "4/2017",
            LocalDate.of(1998, Month.JUNE, 5),
            LocalDate.of(2017, Month.JULY, 10),
            1,
            "username_student",
            "password",
            true,
            true,
            true,
            true,
            new ArrayList<>(),
            new HashSet<>(List.of(mockCourse))
        );
    mockStudentsGrade =
        new StudentsGrade(
            1L,
            mockStudent,
            mockCourse,
            100,
            10,
            false
        );
    mockCourseSubmission =
        new CourseSubmission(
            1L,
            mockCourse.getSemester().getStartingDate().minusDays(1),
            mockCourse.getSemester().getStartingDate().minusDays(1),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            mockStudent,
            mockCourse
        );
    mockCourseExam =
        new CourseExam(
            1L,
            mockCourse.getSemester().getEndingDate().plusDays(1),
            LocalTime.now(),
            true,
            "Lorem ipsum dolor",
            mockCourse,
            null
        );
    mockCourseExamResult =
        new CourseExamResult(
            1L,
            95L,
            mockStudent,
            false,
            false,
            CourseExamResultStatus.WAITING_FOR_APPROVAL,
            mockCourseExam
        );
    mockCourseExamSubmission =
        new CourseExamSubmission(
            1L,
            LocalDate.now(),
            LocalDate.now(),
            false,
            SubmissionStatus.WAITING_FOR_APPROVAL,
            mockStudent,
            mockCourseExam
        );
    mockCourseSchedule =
        new CourseSchedule(
            1L,
            mockCourse.getSemester().getStartingDate().plusDays(2),
            LocalTime.of(13, 30),
            2,
            "Lectures",
            false,
            mockCourse
        );
    mockCourseAssignment =
        new CourseAssignment(
            1L,
            LocalDate.of(2021, Month.APRIL, 1),
            LocalDate.of(2021, Month.APRIL, 15),
            LocalTime.of(13, 30),
            LocalTime.of(13, 30),
            "Lorem ipsum dolor",
            mockCourse
        );
    mockCourseAssignmentResult =
        new CourseAssignmentResult(
            1L,
            95L,
            mockStudent,
            mockCourseAssignment
        );
  }

  public static Module getModule() {
    prepareObjects();
    return mockModule;
  }

  public static Semester getSemester() {
    prepareObjects();
    return mockSemester;
  }

  public static Course getCourse() {
    prepareObjects();
    return mockCourse;
  }

  public static Role getRoleAdmin() {
    prepareObjects();
    mockRole.setName("ROLE_ADMIN");
    return mockRole;
  }

  public static Role getRoleLecturer() {
    prepareObjects();
    mockRole.setName("ROLE_LECTURER");
    return mockRole;
  }

  public static Role getRoleStudent() {
    prepareObjects();
    mockRole.setName("ROLE_STUDENT");
    return mockRole;
  }

  public static Administrator getAdministrator() {
    prepareObjects();
    return mockAdministrator;
  }

  public static Administrator getNewAdministrator() {
    prepareObjects();
    mockAdministrator.setFirstName("NewFirstName");
    mockAdministrator.setLastName("NewLastName");
    mockAdministrator.setUsername("NewUsername");
    mockAdministrator.setPassword("NewNotStrongPassword");
    return mockAdministrator;
  }

  public static Lecturer getLecturer() {
    prepareObjects();
    return mockLecturer;
  }

  public static Lecturer getLecturerWithCourse() {
    prepareObjects();
    mockLecturer.addCourse(mockCourse);
    return mockLecturer;
  }

  public static Student getStudent() {
    prepareObjects();
    return mockStudent;
  }

  public static Student getStudentWithCourses() {
    prepareObjects();
    mockStudent.setStudentsCourses(new HashSet<>(List.of(mockCourse)));
    return mockStudent;
  }

  public static StudentsGrade getStudentsGrade() {
    prepareObjects();
    return mockStudentsGrade;
  }

  public static CourseSubmission getCourseSubmission() {
    prepareObjects();
    return mockCourseSubmission;
  }

  public static CourseExam getCourseExam() {
    prepareObjects();
    mockCourseExam.setResults(new HashSet<>(List.of(mockCourseExamResult)));
    return mockCourseExam;
  }

  public static CourseExamResult getCourseExamResult() {
    prepareObjects();
    return mockCourseExamResult;
  }

  public static CourseExamSubmission getCourseExamSubmission() {
    prepareObjects();
    return mockCourseExamSubmission;
  }

  public static CourseSchedule getCourseSchedule() {
    prepareObjects();
    return mockCourseSchedule;
  }

  public static CourseAssignment getCourseAssignment() {
    prepareObjects();
    return mockCourseAssignment;
  }

  public static CourseAssignment getCourseAssignmentWithResults(){
    prepareObjects();
    mockCourseAssignment.getResults().add(mockCourseAssignmentResult);
    return mockCourseAssignment;
  }

  public static CourseAssignmentResult getCourseAssignmentResult() {
    prepareObjects();
    return mockCourseAssignmentResult;
  }
}
